<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Auth::routes();
  Route::group(['middleware' => 'auth'], function () {

  Route::get('/cadastros/clientes', 'Cadastros\ClientesController@index')->name('cadastros.clientes');
  Route::get('/cadastros/clientes/editar/{id}', 'Cadastros\ClientesController@edit')->name('cadastros.clientes.editar');
  Route::post('/cadastros/clientes/atualizar/{id}', 'Cadastros\ClientesController@update')->name('cadastros.clientes.atualizar');
  Route::get('/cadastros/clientes/cadastrar', 'Cadastros\ClientesController@create')->name('cadastros.clientes.cadastrar');
  Route::post('/cadastros/clientes/salvar', 'Cadastros\ClientesController@store')->name('cadastros.clientes.salvar');
  Route::get('/cadastros/clientes/procurar', 'Cadastros\ClientesController@search')->name('cadastros.clientes.procurar');
  Route::get('/cadastros/clientes/listar', 'Cadastros\ClientesController@show')->name('cadastros.clientes.listar');
  Route::get('/cadastros/clientes/deletar/{id}', 'Cadastros\ClientesController@destroy')->name('cadastros.clientes.deletar');

});
