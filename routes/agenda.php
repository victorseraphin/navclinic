<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Auth::routes();
  Route::group(['middleware' => 'auth'], function () {

  Route::get('/agenda', 'Agenda\AgendaController@index')->name('agenda');
  Route::post('/agenda/salvar', 'Agenda\AgendaController@store')->name('agenda.salvar');
  Route::post('/agenda/novo_cliente', 'Agenda\AgendaController@novo_cliente')->name('agenda.novo_cliente');
  Route::get('/agenda/confirmado/{id}', 'Agenda\AgendaController@confirmado')->name('agenda.confirmado');
  Route::get('/agenda/getby/{id}', 'Agenda\AgendaController@getby')->name('agenda.getby');
  Route::get('/agenda/cancelado/{id}', 'Agenda\AgendaController@cancelado')->name('agenda.cancelado');
  Route::get('/agenda/aguardando/{id}', 'Agenda\AgendaController@aguardando')->name('agenda.aguardando');
  Route::get('/agenda/atender/{id}', 'Agenda\AgendaController@atender')->name('agenda.atender');
  Route::post('/agenda/atender/{id}', 'Agenda\AgendaController@atender_store')->name('agenda.atender');

});
