<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  /*Route::get('/', function () {
      return view('welcome');
  });*/
  Route::get('/', 'Auth\LoginController@index')->name('login');
  /*Route::get('facebook', function () {
      return view('facebook');
  });
  Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
  Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

  Route::get('google', function () {
      return view('google');
  });
  Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
  Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');
  */
  Auth::routes();
  Route::group(['middleware' => 'auth'], function () {
  Route::get('/dashboard', 'System\DashboardController@index')->name('system.dashboard');

  Route::get('/system/programas', 'System\ProgramasController@index')->name('system.programas');
  Route::get('/system/programas/editar/{id}', 'System\ProgramasController@edit')->name('system.programas.editar');
  Route::post('/system/programas/atualizar/{id}', 'System\ProgramasController@update')->name('system.programas.atualizar');
  Route::get('/system/programas/cadastrar', 'System\ProgramasController@create')->name('system.programas.cadastrar');
  Route::post('/system/programas/salvar', 'System\ProgramasController@store')->name('system.programas.salvar');
  Route::get('/system/programas/procurar', 'System\ProgramasController@search')->name('system.programas.procurar');
  Route::get('/system/programas/listar', 'System\ProgramasController@show')->name('system.programas.listar');
  Route::get('/system/programas/deletar/{id}', 'System\ProgramasController@destroy')->name('system.programas.deletar');

  Route::get('/system/grupos', 'System\GruposController@index')->name('system.grupos');
  Route::get('/system/grupos/editar/{id}', 'System\GruposController@edit')->name('system.grupos.editar');
  Route::post('/system/grupos/atualizar/{id}', 'System\GruposController@update')->name('system.grupos.atualizar');
  Route::get('/system/grupos/cadastrar', 'System\GruposController@create')->name('system.grupos.cadastrar');
  Route::post('/system/grupos/salvar', 'System\GruposController@store')->name('system.grupos.salvar');
  Route::get('/system/grupos/checkeds/{id}', 'System\GruposController@checkeds')->name('system.grupos.checkeds');
  Route::get('/system/grupos/procurar', 'System\GruposController@search')->name('system.grupos.procurar');
  Route::get('/system/grupos/listar', 'System\GruposController@show')->name('system.grupos.listar');
  Route::get('/system/grupos/deletar/{id}', 'System\GruposController@destroy')->name('system.grupos.deletar');

  Route::get('/system/empresas', 'System\EmpresasController@index')->name('system.empresas');
  Route::get('/system/empresas/editar/{id}', 'System\EmpresasController@edit')->name('system.empresas.editar');
  Route::post('/system/empresas/atualizar/{id}', 'System\EmpresasController@update')->name('system.empresas.atualizar');
  Route::get('/system/empresas/cadastrar', 'System\EmpresasController@create')->name('system.empresas.cadastrar');
  Route::post('/system/empresas/salvar', 'System\EmpresasController@store')->name('system.empresas.salvar');
  Route::get('/system/empresas/procurar', 'System\EmpresasController@search')->name('system.empresas.procurar');
  Route::get('/system/empresas/listar', 'System\EmpresasController@show')->name('system.empresas.listar');
  Route::get('/system/empresas/deletar/{id}', 'System\EmpresasController@destroy')->name('system.empresas.deletar');

  Route::get('/system/usuarios', 'System\UsuariosController@index')->name('system.usuarios');
  Route::get('/system/usuarios/editar/{id}', 'System\UsuariosController@edit')->name('system.usuarios.editar');
  Route::post('/system/usuarios/atualizar/{id}', 'System\UsuariosController@update')->name('system.usuarios.atualizar');
  Route::get('/system/usuarios/cadastrar', 'System\UsuariosController@create')->name('system.usuarios.cadastrar');
  Route::post('/system/usuarios/salvar', 'System\UsuariosController@store')->name('system.usuarios.salvar');
  Route::get('/system/usuarios/checkeds/{id}', 'System\UsuariosController@checkeds')->name('system.usuarios.checkeds');
  Route::get('/system/usuarios/procurar', 'System\UsuariosController@search')->name('system.usuarios.procurar');
  Route::get('/system/usuarios/listar', 'System\UsuariosController@show')->name('system.usuarios.listar');
  Route::get('/system/usuarios/deletar/{id}', 'System\UsuariosController@destroy')->name('system.usuarios.deletar');
  Route::get('/system/usuarios/ativar/{id}', 'System\UsuariosController@ativar')->name('system.usuarios.ativar');

  Route::get('/system/estados', 'System\EstadosController@index')->name('system.estados');
  Route::get('/system/estados/editar/{id}', 'System\EstadosController@edit')->name('system.estados.editar');
  Route::post('/system/estados/atualizar/{id}', 'System\EstadosController@update')->name('system.estados.atualizar');
  Route::get('/system/estados/cadastrar', 'System\EstadosController@create')->name('system.estados.cadastrar');
  Route::post('/system/estados/salvar', 'System\EstadosController@store')->name('system.estados.salvar');
  Route::get('/system/estados/checkeds/{id}', 'System\EstadosController@checkeds')->name('system.estados.checkeds');
  Route::get('/system/estados/procurar', 'System\EstadosController@search')->name('system.estados.procurar');
  Route::get('/system/estados/listar', 'System\EstadosController@show')->name('system.estados.listar');
  Route::get('/system/estados/deletar/{id}', 'System\EstadosController@destroy')->name('system.estados.deletar');
  Route::get('/system/estados/ativar/{id}', 'System\EstadosController@ativar')->name('system.estados.ativar');

  Route::get('/system/cidades', 'System\CidadesController@index')->name('system.cidades');
  Route::get('/system/cidades/editar/{id}', 'System\CidadesController@edit')->name('system.cidades.editar');
  Route::post('/system/cidades/atualizar/{id}', 'System\CidadesController@update')->name('system.cidades.atualizar');
  Route::get('/system/cidades/cadastrar', 'System\CidadesController@create')->name('system.cidades.cadastrar');
  Route::post('/system/cidades/salvar', 'System\CidadesController@store')->name('system.cidades.salvar');
  Route::get('/system/cidades/checkeds/{id}', 'System\CidadesController@checkeds')->name('system.cidades.checkeds');
  Route::get('/system/cidades/procurar', 'System\CidadesController@search')->name('system.cidades.procurar');
  Route::get('/system/cidades/listar', 'System\CidadesController@show')->name('system.cidades.listar');
  Route::get('/system/cidades/deletar/{id}', 'System\CidadesController@destroy')->name('system.cidades.deletar');
  Route::get('/system/cidades/ativar/{id}', 'System\CidadesController@ativar')->name('system.cidades.ativar');


  Route::get('/system/enderecos', 'System\EnderecosController@index')->name('system.enderecos');
  Route::get('/system/enderecos/editar/{id}', 'System\EnderecosController@edit')->name('system.enderecos.editar');
  Route::post('/system/enderecos/atualizar/{id}', 'System\EnderecosController@update')->name('system.enderecos.atualizar');
  Route::get('/system/enderecos/cadastrar', 'System\EnderecosController@create')->name('system.enderecos.cadastrar');
  Route::post('/system/enderecos/salvar', 'System\EnderecosController@store')->name('system.enderecos.salvar');
  Route::get('/system/enderecos/procurar', 'System\EnderecosController@search')->name('system.enderecos.procurar');
  Route::get('/system/enderecos/listar', 'System\EnderecosController@show')->name('system.enderecos.listar');
  Route::get('/system/enderecos/deletar/{id}', 'System\EnderecosController@destroy')->name('system.enderecos.deletar');
  Route::get('/system/enderecos/padrao/{id}', 'System\EnderecosController@padrao')->name('system.enderecos.padrao');


  Route::get('/system/preferencias', 'System\PreferenciasController@index')->name('system.preferencias');

  Route::get('/meus_dados/{id}', 'MeusDadosController@index')->name('meus_dados');
  Route::post('/meus_dados/atualizar/{id}', 'MeusDadosController@update')->name('meus_dados.atualizar');

});

Route::view('/404-tenant', 'errors.404-tenant')->name('404.tenant');

Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/register/salvar', 'Auth\RegisterController@store')->name('register.salvar');

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login/login', 'Auth\LoginController@login')->name('login.login');


Route::any('/logout', 'Auth\LoginController@logout')->name('logout');


include_once('cadastros.php');
include_once('agenda.php');
include_once('prontuario.php');
