<?php

namespace App\Models\Cadastros;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clientes extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','nome','cpf','email','fone','nascimento','sexo','system_unit_id','loc_ceps_id','end_descricao','end_numero'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='clientes';


}
