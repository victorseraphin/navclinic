<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemUser extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','name', 'email', 'doc','password','frontpage_id','system_unit_id','active','facebook_id','google_id'];
    protected $hidden = ['password', 'remember_token','created_at','updated_at','deleted_at'];
    protected $table ='system_user';

    //metodo usado para fazer login pelo facebook
    public function addNewFacebook($input){

        $check = static::where('facebook_id',$input['facebook_id'])->first();
        if(is_null($check)){
            return static::create($input);
        }
        return $check;

    }
    //metodo usado para fazer login pelo facebook
    public function addNewGoogle($input){

        $check = static::where('google_id',$input['google_id'])->first();
        if(is_null($check)){
            return static::create($input);
        }
        return $check;

    }

    public function programas()
    {
      return $this->belongsToMany(SystemProgram::class,'system_program_user');

    }

    public function eAdmin()
    {
      return $this->existeGrupo('SuperAdmin');
    }
    public function grupos()
    {
      return $this->belongsToMany(SystemGroup::class,'system_group_user');
    }

    public function existeGrupo($grupo)
    {
      if (is_string($grupo)) {
        $grupo = SystemGroup::where('name','=',$grupo)->firstOrFail();
      }
      return (boolean) $this->grupos()->find($grupo->id);
    }

    /*public function listaProgramas()
    {
       return SystemProgram::with('grupos')->get();
    }*/


    public function temUmGrupoDestes($grupos)
    {
        $userGrupos = $this->grupos;
        //$teste = $this->listaProgramas();
        //$array_programas = array();
        //foreach ($teste as $programas) {
          //array_push($array_programas, $programas->grupos);
        //}
        return $grupos->intersect($userGrupos)->count();
    }

    public function temUmProgramaDestes($programas)
    {
      $userProgramas = $this->programas;
      $array_programas = array();
      foreach ($userProgramas as $value) {
        array_push($array_programas, $value->id);
      }
      if (in_array($programas, $array_programas)) {
        return 1;
      }
      else{
        return 0;
      }
    }
}
