<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cep extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','cid_id','rua_nome','bairro_nome'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='loc_ceps';


}
