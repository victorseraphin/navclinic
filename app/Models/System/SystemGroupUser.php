<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SystemGroupUser extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['system_group_id','system_user_id'];
    protected $hidden = ['id','created_at','updated_at'];
    protected $table ='system_group_user';

    public static function classGrupo999($id)
    {
      $gruposUser = SystemGroupUser::select('system_group_id')->where('system_user_id',$id)->get();
      $groupuser = array();
      foreach($gruposUser as $value){
        array_push($groupuser,$value->system_group_id);
      }
      if(in_array(999,$groupuser)){
        return true;
      }else{
        return false;
      }
    }

}
