<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemGroup extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','name'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='system_group';


    /*public function usuarios()
    {
      return $this->belongsToMany(SystemUser::class, 'system_group_user');
    }*/

    public function programas()
    {
      return $this->belongsToMany(SystemProgram::class,'system_group_program');
    }


    /*public function existePrograma($programas)
    {
      if (is_string($programas)) {
        $programas = SystemProgram::where('controller','=',$programas)->firstOrFail();
      }

      return (boolean) $this->programas()->find($programas->id);

    }*/



}
