<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemUnit extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['name'];
    protected $hidden = ['id','created_at','updated_at','deleted_at'];
    protected $table ='system_unit';



}
