<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SystemGroupProgram extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['system_group_id','system_program_id'];
    protected $hidden = ['id','created_at','updated_at'];
    protected $table ='system_group_program';

}
