<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemProgram extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','name', 'controller'];
    //protected $hidden = ['created_at','updated_at'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='system_program';

    public function grupos()
    {
         return $this->belongsToMany(SystemGroup::class,'system_group_program');
    }


}
