<?php

namespace App\Models\System;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemDatabase extends Model
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['name', 'cnpj_cpf','domain', 'bd_database', 'bd_hostname', 'bd_username', 'bd_password'];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }
}
