<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cidades extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','cod_ibge','cid_nome','est_id'];
    protected $hidden = ['id','created_at','updated_at','deleted_at'];
    protected $table ='loc_cidades';


}
