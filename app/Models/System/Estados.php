<?php

namespace App\Models\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estados extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','est_nome','est_uf'];
    protected $hidden = ['id','created_at','updated_at','deleted_at'];
    protected $table ='loc_estados';


}
