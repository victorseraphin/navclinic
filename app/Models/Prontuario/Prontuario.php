<?php

namespace App\Models\Prontuario;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prontuario extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','profissional_id','cliente_id','agenda_id','queixa','historico','observacao','palpacao',
                           'evolucao','data','status','valor','system_unit_id'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='prontuario';


}
