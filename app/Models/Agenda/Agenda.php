<?php

namespace App\Models\Agenda;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agenda extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id','profissional_id','cliente_id','data','hora_ini','hora_fin','status','valor','system_unit_id'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    protected $table ='agenda';


}
