<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Models\System\SystemProgram;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
     public function boot()
     {
         $this->registerPolicies();

         foreach ($this->listaProgramas() as $programas) {
           Gate::define($programas->controller,function($user) use($programas){
             $array = array();
             foreach($programas->grupos as $value){
               array_push($array, $value->id);
             }
             if($programas->controller == "system.databases"  and $user->eAdmin() == false){
               return false;
             }
             else{
               return $user->temUmGrupoDestes($programas->grupos) || $user->temUmProgramaDestes($programas->id) || $user->eAdmin();
             }
           });
         }


     }
     public function listaProgramas()
     {
        //return SystemProgram::with('grupos');
        return SystemProgram::with('grupos')->get();
     }
}
