<?php
namespace App\Http\Controllers\Prontuario;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Prontuario\Prontuario;

class ProntuarioController extends Controller
{
  public function index($id)
  {
    if(Gate::denies('agenda')){
      abort(403,"Não autorizado!");
    }
    $query = Prontuario::select('prontuario.data','prontuario.queixa','prontuario.historico','prontuario.observacao',
                                'prontuario.palpacao','prontuario.evolucao','prontuario.valor','agenda.hora_ini');

    $query->where('prontuario.clientes_id','=',$id);
    $query->where('prontuario.profissional_id','=',Auth::user()['id']);
    $query->where('prontuario.status','=','5');
    $query->leftjoin('agenda', 'prontuario.agenda_id', '=', 'agenda.id');


    $tabela = $query->orderBy('data', 'desc')->get();
    return view('prontuario.prontuario',['tabela'=>$tabela]);
  }
}
