<?php

namespace App\Http\Controllers\Cadastros;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Cadastros\Clientes;
use App\Models\System\Cidades;
use App\Models\System\Estados;
use App\Models\System\Cep;

class ClientesController extends Controller
{

    public function index()
    {
      if(Gate::denies('cadastros.clientes')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $tabela = Clientes::where('system_unit_id','=',Auth::user()['system_unit_id'])->get();
      return view('cadastros.clientes',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('cadastros.clientes.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_nome = (isset(session('search')['nome']) ? session('search')['nome'] : null);

      $query = Clientes::select('*')->where('system_unit_id','=',Auth::user()['system_unit_id']);
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_nome)){
        $query->where('nome', 'ILIKE', "%$session_nome%");
      }
      $tabela = $query->orderBy('nome', 'asc')->get();

      return view('cadastros.clientes',['tabela' => $tabela]);
    }
    public function edit($id)
    {
      if(Gate::denies('cadastros.clientes.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = Clientes::where('id',$id)->where('system_unit_id','=',Auth::user()['system_unit_id'])->firstOrFail();
        $estados = Estados::get();
        if($campos){
          return view('cadastros.clientesform',['campos' => $campos,'estados'=>$estados]);
        }else{
          $tabela = Clientes::get();
          return view('cadastros.clientes',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id)
    {
      if(Gate::denies('cadastros.clientes.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'nome'  => 'required',
          'sexo'  => 'required',
      ],[],[
          'nome'  => '"nome"',
          'sexo'  => '"sexo"',

        ]);
        if($request->loc_ceps_id != null){
          $cep = preg_replace("/[^0-9]/", "", $request->loc_ceps_id);

          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

          //verifica se o cep esta cadastrado
          if($id_cep == null){
            //verifica se a cidade existe
            $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

            if($cid_id == null){
              $est_id = DB::table('loc_estados')->select('id')->where('est_uf',$request->end_uf)->first();
              $cidade = new Cidades;
              $cidade->cod_ibge = $request->get('cod_ibge');
              $cidade->cid_nome = $request->get('cidade_nome');
              $cidade->est_id   = $est_id->id;
              $cidade->save();

              $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();

            }else{
              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();
            }
          }
          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();
        }


        $dados = Clientes::findOrFail($id);
        $dados->nome              = $request->nome;
        $dados->system_unit_id    = Auth::user()['system_unit_id'];
        if($request->loc_ceps_id != null){
          $dados->loc_ceps_id     = $id_cep->id;
        }else{
          $dados->loc_ceps_id = null;
        }
        $dados->email             = $request->email;
        $dados->cpf               = $request->cpf;
        $dados->fone              = $request->fone;
        $dados->sexo              = $request->sexo;
        if($request->nascimento != null){
          $dataarray                = explode('/', $request->nascimento);
          $dados->nascimento        = $dataarray[2] . '-' . $dataarray[1] . '-' . $dataarray[0];
        }else{
          $dados->nascimento = null;
        }
        $dados->end_descricao     = $request->end_descricao;
        $dados->end_numero        = $request->end_numero;
        $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('cadastros.clientes.listar');

    }
    public function create()
    {
      if(Gate::denies('cadastros.clientes.cadastrar')){
        abort(403,"Não autorizado!");
      }
      $estados = Estados::get();
      return view('cadastros.clientesform',['estados'=>$estados]);
    }
    public function store(Request $request)
    {
      if(Gate::denies('cadastros.clientes.salvar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'nome'  => 'required',
          'sexo'  => 'required',
      ],[],[
          'nome'  => '"nome"',
          'sexo'  => '"sexo"',

      ]);
      $cpf_exite = Clientes::where('cpf',$request->cpf)->first();
      //dd($cpf_exite);
      if($cpf_exite != null and $request->cpf != null){
        alert()->error('CPF já cadastrado no sistema!')->persistent("Fechar");
        return redirect()->back()->withInput();
      }else{

        if($request->loc_ceps_id != null){
          $cep = preg_replace("/[^0-9]/", "", $request->loc_ceps_id);
          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

          //verifica se o cep esta cadastrado
          if($id_cep == null){
            //verifica se a cidade existe
            $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

            if($cid_id == null){
              $est_id = DB::table('loc_estados')->select('id')->where('est_uf',$request->end_uf)->first();
              $cidade = new Cidades;
              $cidade->cod_ibge = $request->get('cod_ibge');
              $cidade->cid_nome = $request->get('cidade_nome');
              $cidade->est_id   = $est_id->id;
              $cidade->save();

              $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();

            }else{
              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();
            }
          }
          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

        }



        $dados = new Clientes;
        $dados->nome              = $request->nome;
        $dados->system_unit_id    = Auth::user()['system_unit_id'];
        if($request->loc_ceps_id != null){
          $dados->loc_ceps_id     = $id_cep->id;
        }else{
          $dados->loc_ceps_id = null;
        }
        $dados->email             = $request->email;
        $dados->cpf               = $request->cpf;
        $dados->fone              = $request->fone;
        $dados->sexo              = $request->sexo;
        $dados->nascimento        = $request->nascimento;
        $dados->end_descricao     = $request->end_descricao;
        $dados->end_numero        = $request->end_numero;
        $dados->save();

        if($dados){
          alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
          alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('cadastros.clientes.listar');
      }

    }
    public function destroy($id)
    {
      if(Gate::denies('cadastros.clientes.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = Clientes::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('cadastros.clientes.listar');
    }
    public function search(Request $request)
    {
      if(Gate::denies('cadastros.clientes.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['nome'] = $request->nome;
      session(['search' => $search]);
      return redirect()->route('cadastros.clientes.listar');

    }


}
