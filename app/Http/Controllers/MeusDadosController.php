<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\System\SystemUser;
use Illuminate\Support\Facades\Auth;


class MeusDadosController extends Controller
{

  public function index($id)
  {
    if(Gate::denies('meus_dados')){
      abort(403,"Não autorizado!");
    }
    $campos = SystemUser::where('id',$id)->firstOrFail();
    return view('meus_dados',['campos' => $campos]);
  }
  public function update(Request $request,$id)
  {
    if(Gate::denies('meus_dados.atualizar')){
      abort(403,"Não autorizado!");
    }
    if($request->password != null){
      $numero = filter_var($request->password, FILTER_SANITIZE_NUMBER_INT) !== '';

      if(preg_match('/\p{Lu}/u', $request->password)){
        $maiusculo= true;
      }else{
        $maiusculo= false;
      }
      if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $request->password)){
        $especial= true;
      }else{
        $especial= false;
      }
      if(strlen($request->password) >= 8){
        $caracteres= true;
      }else{
        $caracteres= false;
      }
      if(preg_match('/[a-z]/', $request->password)){
        $minusculo= true;
      }else{
        $minusculo= false;
      }

      if($numero != true or $maiusculo != true or $especial != true or $caracteres != true or $minusculo != true){
        alert()->warning("8 caracteres,
                            1 letra maiúscula,
                            1 letra minúscula,
                            1 caracter especial,
                            1 número",'A SENHA deve ter:')->persistent("Fechar");
      }
      $this->validate($request, [
            'name'  => 'required',
            'email'  => 'required|unique:system_user,email,'.$id,
            'password'  => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'confirma_senha'  => 'required|same:password',
      ],[],[
            'name'  => '"nome"',
            'email'  => '"email"',
            'password'  => '"senha"',
            'confirma_senha'  => '"confirma senha"',
      ]);
    }else{

    }
    $this->validate($request, [
      'name'  => 'required',
      'fone'  => 'required',
      'doc'  => 'required',
      'email'  => 'required|unique:system_user,email,'.$id,

    ],[],[
      'name'  => '"nome"',
      'fone'  => '"fone"',
      'doc'  => '"cpf/cnpj"',
      'email'  => '"email"',
    ]);
    $sytem_user = SystemUser::where('id',$id)->get();

    $dados = SystemUser::findOrFail($id);
    $dados->name  = $request->name;
    $dados->fone  = $request->fone;
    $dados->doc  = $request->doc;
    $dados->email  = $request->email;
    if($request->password != $sytem_user[0]->password){
      $dados->password  = Hash::make($request->password);
    }
    $dados->save();

    if($dados){
      alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
      alert()->error('Problema ao alterado registro!')->persistent("Fechar");
    }
    if(isset(session('plano')['id']) and isset(session('plano')['url'])){
      return redirect()->route(session('plano')['url'],session('plano')['id']);
    }elseif(isset(session('plano')['url'])){
      return redirect()->route(session('plano')['url']);
    }
    return redirect()->route('meus_dados',$id);

  }




}
