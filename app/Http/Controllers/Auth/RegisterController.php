<?php

namespace App\Http\Controllers\Auth;

use App\Models\System\SystemUser;
use App\Models\System\SystemGroupUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    //use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(){
          return view('auth.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'doc' => 'required|string|max:255',
            'fone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:system_user',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function store(Request $request)
    {
      $cliente               = new SystemUser();
      $cliente->name         = $request->get('name');
      $cliente->doc          = $request->get('doc');
      $cliente->fone         = $request->get('fone');
      $cliente->email        = $request->get('email');
      $cliente->password     = bcrypt($request->get('password'));
      $cliente->active       =  'Y';

      $cliente->save();

      $grupo                    = new SystemGroupUser();
      $grupo->system_user_id    = $cliente->id;
      $grupo->system_group_id   = 3;
      $grupo->save();

      if($cliente and $grupo){
        alert()->success('Cadastro efetuado com sucesso.','Sucesso!')->autoclose(3000);
        if(isset(session('plano')['id']) and isset(session('plano')['url'])){
          return redirect()->route(session('plano')['url'],session('plano')['id']);
        }elseif(isset(session('plano')['url'])){
          return redirect()->route(session('plano')['url']);
        }
        return redirect()->route('login');

      }else{
        alert()->error('Erro ao cadastrar!');
        return redirect()->route('register');
      }

    }
}
