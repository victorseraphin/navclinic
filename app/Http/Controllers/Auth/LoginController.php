<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\System\SystemDatabase;
use App\Models\System\SystemUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
  use AuthenticatesUsers;
  //private $url_top = "https://admin.navdata.com.br/";
  //private $url_top = "http://adminlte.laravel:8000/";

  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }
  public function index(){
        return view('auth.login');
  }

    public function login(Request $request){
      $this->validate($request, [
              'email' => 'required',
              'password' => 'required',
          ],[],[
              'email'     => 'E-mail',
              'password'     => 'Senha',
          ]);

          $credentials = [
              'email'=> $request->email,
              'password' => $request->password
          ];

          $dados_usuario = DB::table('system_user')->select('id')->where('email', $request->get('email'))->first();

            if($dados_usuario != null){
              $ativo = DB::table('system_user')->select('active')->where('id', $dados_usuario->id)->first();
              if($ativo->active == 'Y'){
                $authOK = Auth::guard()->attempt($credentials, $request->remember);
                if($authOK){
                  if(isset(session('plano')['id']) and isset(session('plano')['url'])){
                    return redirect()->route(session('plano')['url'],session('plano')['id']);
                  }elseif(isset(session('plano')['url'])){
                    return redirect()->route(session('plano')['url']);
                  }
                  return redirect()->route('agenda');
                }
                return redirect()->back()->withInput($request->only(['email','remember_admin']))->withErrors(['password'=>'A senha está incorreta.']);
              }else{
                  alert()->error('Usuário bloqueado! Entre em contato com o ADMINISTRADOR')->autoclose(3000);
                  return redirect()->route('login');
              }

            }
            else{
                return redirect()->back()->withInput($request->only(['email','remember_admin']))->withErrors(['email'=>'Essas credenciais não correspondem aos nossos registros.']);
            }


    }

    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->invalidate();
        //session()->forget(['dados_empresa']);
        //$url = $this->url_top."logout_total";
        return redirect('login');
    }

}
