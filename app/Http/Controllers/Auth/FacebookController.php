<?php

namespace App\Http\Controllers\Auth;

use App\Models\System\SystemUser;
use App\Models\System\SystemGroupUser;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth;
use Illuminate\Support\Facades\DB;

class FacebookController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();


    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {

            $user = Socialite::driver('facebook')->user();

            $cliente_id = DB::table('system_user')->select('id')->where('facebook_id', $user->getId())->first();

            if ($cliente_id) {
                $id = $cliente_id->id;
                $authOK = Auth::guard()->loginUsingId($id);

                if($authOK){
                    return redirect()->route('home');
                }
            } else {
                // check if they're an existing user
                $existingUser = SystemUser::where('email', $user->email)->first();

                if($existingUser){
                    // log them in
                    alert()->error('Este e-mail já está sendo utilizado!')->persistent("Fechar");
                    return redirect()->route('login');
                } else {
                  $create['name'] = $user->getName();
                  $create['email'] = $user->getEmail();
                  $create['facebook_id'] = $user->getId();
                  $create['password'] = bcrypt(rand(1,10000));

                  $userModel = new SystemUser;
                  $createdUser = $userModel->addNewFacebook($create);

                  $cliente_id = DB::table('system_user')->select('id')->where('facebook_id', $user->getId())->first();

                  $grupo                    = new SystemGroupUser();
                  $grupo->system_user_id    = $cliente_id->id;
                  $grupo->system_group_id   = 3;
                  $grupo->save();

                  $id = $cliente_id->id;
                  $authOK = Auth::guard()->loginUsingId($id);

                  if($authOK){
                      return redirect()->route('home');
                  }
                }

            }

    }
}
