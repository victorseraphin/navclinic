<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\System\Estados;


class EstadosController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.estados')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $tabela = Estados::get();
      return view('system.estados',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('system.estados.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = Estados::select('id','est_nome');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('est_nome', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.estados',['tabela' => $tabela]);
    }
    public function edit($id)
    {
      if(Gate::denies('system.estados.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = Estados::where('id',$id)->firstOrFail();
        if($campos){
          return view('system.estadosform',['campos' => $campos]);
        }else{
          $tabela = Estados::get();
          return view('system.estados',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id)
    {
      if(Gate::denies('system.estados.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
            'est_nome'  => 'required',
            'est_uf'  => 'required',
        ],[],[
            'est_nome'  => 'nome',
            'est_uf'    => 'uf',

        ]);

        $dados = Estados::findOrFail($id);
        $dados->est_nome  = $request->est_nome;
        $dados->est_uf  = $request->est_uf;
        $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.estados.listar');

    }
    public function create()
    {
      if(Gate::denies('system.estados.cadastrar')){
        abort(403,"Não autorizado!");
      }
          return view('system.estadosform');
    }
    public function store(Request $request)
    {
      if(Gate::denies('system.estados.salvar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'est_nome'  => 'required',
          'est_uf'  => 'required',
      ],[],[
          'est_nome'  => 'nome',
          'est_uf'    => 'uf',

        ]);

        $dados = new Estados;
        $dados->est_nome  = $request->est_nome;
        $dados->est_uf  = $request->est_uf;
        $dados->save();

        if($dados){
            alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('system.estados.listar');

    }
    public function destroy($id)
    {
      if(Gate::denies('system.estados.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = Estados::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.estados.listar');
    }
    public function search(Request $request)
    {
      if(Gate::denies('system.estados.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['name'] = $request->name;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = Estados::select('id','est_nome');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('est_nome', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.estados',['tabela' => $tabela]);
    }


}
