<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\System\SystemGroup;
use App\Models\System\SystemProgram;
use App\Models\System\SystemGroupUser;
use App\Models\System\SystemGroupProgram;
use Illuminate\Support\Facades\Gate;


class GruposController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.grupos')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);

      $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
      if($grupoUser){
        $tabela     = SystemGroup::get();
      }else{
        $tabela     = SystemGroup::where('id','!=', 999)->get();
      }


      return view('system.grupos',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('system.grupos.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
      if($grupoUser){
        $query     = SystemGroup::select('id','name');
      }else{
        $query     = SystemGroup::where('id','!=', 999);
      }

      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.grupos',['tabela' => $tabela]);
    }
    public function checkeds($id){

        $query  = SystemProgram::select('system_program.id','system_program.controller', 'system_program.name','system_group_program.system_program_id as programa')
                                            ->leftjoin('system_group_program', function($join) use($id){
                                              $join->on('system_program.id', '=', 'system_group_program.system_program_id')
                                              ->where('system_group_program.system_group_id', '=', $id);
                                            });


      $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);

      if(!$grupoUser){
        $query->where('system_program.id','>', 16);
      }

      $tabela_programas = $query->orderBy('controller', 'asc')->get();

      $response = array(
              'status' => 'success',
              'data'=>$tabela_programas,
          );
          return \Response::json($response);

    }
    public function edit($id)
    {
      if(Gate::denies('system.grupos.editar')){
        abort(403,"Não autorizado!");
      }
      $campos = SystemGroup::where('id',$id)->firstOrFail();

      if($campos){
        return view('system.gruposform',['campos' => $campos]);
      }else{
        $tabela = SystemGroup::get();
        return view('system.grupos',['tabela' => $tabela]);
      }
    }
    public function update(Request $request,$id){
      if(Gate::denies('system.grupos.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
            'name'  => 'required',
        ],[],[
            'name'  => 'nome',

        ]);
        $dados = SystemGroup::findOrFail($id);
        $dados->name  = $request->name;
        $dados->save();

        $id = $dados->id;
        $dados = SystemGroupProgram::where('system_group_id',$id);
        $dados->delete();

        if($request->programas){
          foreach($request->programas as $linha){
              $programas = new SystemGroupProgram;
              $programas->system_group_id = $id;
              $programas->system_program_id =$linha;
              $programas->save();
          }
        }

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.grupos');

    }
    public function create(){
      if(Gate::denies('system.grupos.cadastrar')){
        abort(403,"Não autorizado!");
      }
        $tabela = SystemProgram::get();
        return view('system.gruposform',['tabela'=>$tabela]);
    }
    public function store(Request $request){
      if(Gate::denies('system.grupos.salvar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'name'  => 'required',
      ],[],[
          'name'  => 'nome',

        ]);
        $dados = new SystemGroup;
        $dados->name  = $request->name;
        $dados->save();

        $id = $dados->id;
        if($request->programas){
          foreach($request->programas as $linha){
            $programas = new SystemGroupProgram;
            $programas->system_group_id = $id;
            $programas->system_program_id =$linha;
            $programas->save();
          }
        }

        if($dados){
            alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('system.grupos');

    }
    public function destroy($id){
      if(Gate::denies('system.grupos.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = SystemGroup::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.grupos.listar');
    }
    public function search(Request $request){
      if(Gate::denies('system.grupos.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['name'] = $request->name;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
      if($grupoUser){
        $query     = SystemGroup::select('id','name');
      }else{
        $query     = SystemGroup::where('id','!=', 999);
      }
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.grupos',['tabela' => $tabela]);
  }


}
