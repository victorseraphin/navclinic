<?php

namespace App\Http\Controllers\System;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use Illuminate\Http\Request;

class PreferenciasController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.preferencias')){
        abort(403,"Não autorizado!");
      }
      return view('system.preferencias');
    }

}
