<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\System\SystemUser;
use App\Models\System\SystemUnit;
use App\Models\System\SystemGroup;
use App\Models\System\SystemProgram;
use App\Models\System\SystemProgramUser;
use App\Models\System\SystemGroupUser;
use Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class UsuariosController extends Controller
{

  public function index()
  {
    if(Gate::denies('system.usuarios')){
      abort(403,"Não autorizado!");
    }
    session()->forget(['search']);
    $tabela = SystemUser::get();
    return view('system.usuarios',['tabela' => $tabela]);
  }
  public function show()
  {
    if(Gate::denies('system.usuarios.listar')){
      abort(403,"Não autorizado!");
    }
    $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
    $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

    $query = SystemUser::select('id','name', 'email', 'password','frontpage_id','system_unit_id','active');
    if(!empty($session_id)){
      $query->where('id','=',$session_id);
    }
    if(!empty($session_name)){
      $query->where('name', 'ILIKE', "%$session_name%");
    }
    $tabela = $query->orderBy('id', 'asc')->get();

    return view('system.usuarios',['tabela' => $tabela]);
  }
  public function checkeds($id){
    $query  = SystemProgram::select('system_program.id','system_program.controller', 'system_program.name','system_program_user.system_program_id as programa')
                                                                        ->leftjoin('system_program_user', function($join) use($id){
                                                                          $join->on('system_program.id', '=', 'system_program_user.system_program_id')
                                                                          ->where('system_program_user.system_user_id', '=', $id);
                                                                        });
    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);

    if(!$grupoUser){
      $query->where('system_program.id','>', 16);
    }

    $tabela_programas = $query->orderBy('controller', 'asc')->get();

    $response = array(
            'status' => 'success',
            'data'=>$tabela_programas,
        );
        return \Response::json($response);

  }
  public function edit($id)
  {
    if(Gate::denies('system.usuarios.editar')){
      abort(403,"Não autorizado!");
    }
    $campos = SystemUser::where('id',$id)->firstOrFail();
    $tabela_programas  = SystemProgram::get();

    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
    if($grupoUser){
      $tabela_grupos     = SystemGroup::get();
      $tabela_empresas  = SystemUnit::get();
    }else{
      $tabela_grupos     = SystemGroup::where('id','!=', 999)->get();
      $tabela_empresas  = SystemUnit::where('id','=',$campos->system_unit_id)->get();;
    }

    $grupo = SystemGroupUser::select('system_group_id')->where('system_user_id',$id)->get();
    $programas = SystemProgramUser::select('system_program_id')->where('system_user_id',$id)->get();

    $program_user = array();
    foreach($programas as $linha){
      array_push($program_user,$linha->system_program_id);
    }
    $group_user = array();
    foreach($grupo as $linha){
      array_push($group_user,$linha->system_group_id);

    }
    if($campos){
      return view('system.usuariosform',['campos' => $campos,'tabela_programas'=>$tabela_programas, 'tabela_grupos'=>$tabela_grupos,'group_user'=>$group_user,'program_user'=>$program_user,'tabela_empresas'=>$tabela_empresas]);
    }else{
      $tabela = SystemUser::get();
      return view('system.usuarios',['tabela' => $tabela]);
    }
  }
  public function update(Request $request,$id)
  {
    if(Gate::denies('system.usuarios.atualizar')){
      abort(403,"Não autorizado!");
    }
    if($request->password != null){
      $numero = filter_var($request->password, FILTER_SANITIZE_NUMBER_INT) !== '';

      if(preg_match('/\p{Lu}/u', $request->password)){
        $maiusculo= true;
      }else{
        $maiusculo= false;
      }
      if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $request->password)){
        $especial= true;
      }else{
        $especial= false;
      }
      if(strlen($request->password) >= 8){
        $caracteres= true;
      }else{
        $caracteres= false;
      }
      if(preg_match('/[a-z]/', $request->password)){
        $minusculo= true;
      }else{
        $minusculo= false;
      }

      if($numero != true or $maiusculo != true or $especial != true or $caracteres != true or $minusculo != true){
        alert()->warning("8 caracteres,
                            1 letra maiúscula,
                            1 letra minúscula,
                            1 caracter especial,
                            1 número",'A SENHA deve ter:')->persistent("Fechar");
      }
      $this->validate($request, [
            'name'  => 'required',
            'email'  => 'required|unique:system_user,email,'.$id,
            'password'  => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'confirma_senha'  => 'required|same:password',
      ],[],[
            'name'  => '"nome"',
            'email'  => '"email"',
            'password'  => '"senha"',
            'confirma_senha'  => '"confirma senha"',
      ]);
    }else{

    }
    $this->validate($request, [
      'name'  => 'required',
      'email'  => 'required|unique:system_user,email,'.$id,

    ],[],[
      'name'  => '"nome"',
      'email'  => '"email"',
    ]);
    //dd($request->programas);
    $dados = SystemUser::findOrFail($id);
    $dados->name  = $request->name;
    $dados->login  = $request->login;
    $dados->email  = $request->email;
    $dados->system_unit_id  = $request->system_unit_id;
    if($request->password){
      $dados->password  = Hash::make($request->password);
    }
    $dados->save();

    $id = $dados->id;
    $dados = SystemGroupUser::where('system_user_id',$id);
    $dados->delete();

    $dados = SystemProgramUser::where('system_user_id',$id);
    $dados->delete();

    if($request->programas){
      foreach($request->programas as $linha){
        $programas = new SystemProgramUser;
        $programas->system_user_id = $id;
        $programas->system_program_id =$linha;
        $programas->save();
      }
    }
    if($request->grupos){
      foreach($request->grupos as $linha){
        $grupos = new SystemGroupUser;
        $grupos->system_user_id = $id;
        $grupos->system_group_id =$linha;
        $grupos->save();
      }
    }

    if($dados){
      alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
      alert()->error('Problema ao alterado registro!')->persistent("Fechar");
    }
    return redirect()->route('system.usuarios');

  }
  public function create()
  {
    if(Gate::denies('system.usuarios.cadastrar')){
      abort(403,"Não autorizado!");
    }
    $tabela_programas  = SystemProgram::get();
    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);

    if($grupoUser){
      $tabela_grupos     = SystemGroup::get();
      $tabela_empresas  = SystemUnit::get();
    }else{
      $tabela_grupos     = SystemGroup::where('id','!=', 999)->get();
      $tabela_empresas  = SystemUnit::where('id','=',$campos->system_unit_id)->get();;
    }

    return view('system.usuariosform',['tabela_programas'=>$tabela_programas, 'tabela_grupos'=>$tabela_grupos,'tabela_empresas'=>$tabela_empresas]);
  }
  public function store(Request $request)
  {
    if(Gate::denies('system.usuarios.salvar')){
      abort(403,"Não autorizado!");
    }
    $numero = filter_var($request->password, FILTER_SANITIZE_NUMBER_INT) !== '';

    if(preg_match('/\p{Lu}/u', $request->password)){
      $maiusculo= true;
    }else{
      $maiusculo= false;
    }
    if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $request->password)){
      $especial= true;
    }else{
      $especial= false;
    }
    if(strlen($request->password) >= 8){
      $caracteres= true;
    }else{
      $caracteres= false;
    }
    if(preg_match('/[a-z]/', $request->password)){
      $minusculo= true;
    }else{
      $minusculo= false;
    }

    if($numero != true or $maiusculo != true or $especial != true or $caracteres != true or $minusculo != true){
      alert()->warning("8 caracteres,
                          1 letra maiúscula,
                          1 letra minúscula,
                          1 caracter especial,
                          1 número",'A SENHA deve ter:')->persistent("Fechar");
    }

    $this->validate($request, [
          'name'  => 'required',
          'email'  => 'required|unique:system_user',
          'password'  => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
          'confirma_senha'  => 'required|same:password',
    ],[],[
          'name'  => '"nome"',
          'email'  => '"email"',
          'password'  => '"senha"',
          'confirma_senha'  => '"confirma senha"',
    ]);

    $dados = new SystemUser;
    $dados->name  = $request->name;
    $dados->login  = $request->login;
    $dados->email  = $request->email;
    $dados->system_unit_id  = $request->system_unit_id;
    $dados->password  = Hash::make($request->password);
    $dados->active  = 'Y';
    $dados->save();

    $id = $dados->id;

    if($request->programas){
      foreach($request->programas as $linha){
        $programas = new SystemProgramUser;
        $programas->system_user_id = $id;
        $programas->system_program_id =$linha;
        $programas->save();
      }
    }
    if($request->grupos){
      foreach($request->grupos as $linha){
        $grupos = new SystemGroupUser;
        $grupos->system_user_id = $id;
        $grupos->system_group_id =$linha;
        $grupos->save();
      }
    }

    if($dados){
      alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
      alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
    }
    return redirect()->route('system.usuarios');

  }
  public function destroy($id)
  {
    if(Gate::denies('system.usuarios.deletar')){
      abort(403,"Não autorizado!");
    }

    $dados = SystemUser::where('id',$id)->firstOrFail();
    $dados->delete();

    if($dados){
      alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
      alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
    }
      return redirect()->route('system.usuarios.listar');
  }
  public function search(Request $request){
    if(Gate::denies('system.usuarios.procurar')){
      abort(403,"Não autorizado!");
    }
    $search = array();
    $search['id'] = $request->id;
    $search['name'] = $request->name;
    session(['search' => $search]);


    $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

    $query = SystemUser::select('id','name', 'email', 'password','frontpage_id','system_unit_id','active');
    if(!empty($session_id)){
      $query->where('id','=',$session_id);
    }
    if(!empty($session_name)){
      $query->where('name', 'ILIKE', "%$session_name%");
    }
    $tabela = $query->orderBy('id', 'asc')->get();
    return view('system.usuarios',['tabela' => $tabela]);
  }
  public function ativar($id)
  {
    if(Gate::denies('system.usuarios.ativar')){
      abort(403,"Não autorizado!");
    }

    $dados = SystemUser::findOrFail($id);

    if($dados){
      if($dados->active != 'Y' ){
        $dados->active  = 'Y';
        $dados->save();
      }
      else{
        $dados->active  = 'N';
        $dados->save();
      }
    }
    if($dados){
      alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
      alert()->error('Registro não pode ser alterado!')->persistent("Fechar");
    }
    return redirect()->route('system.usuarios.listar')->with('success', 'Sucesso!!!');
  }


}
