<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\System\SystemProgram;
use App\Models\System\SystemGroupUser;
use Illuminate\Support\Facades\Gate;

class ProgramasController extends Controller
{

  public function index()
  {
    if(Gate::denies('system.programas')){
      abort(403,"Não autorizado!");
    }
    session()->forget(['search']);
    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
    if($grupoUser){
      $tabela     = SystemProgram::get();
    }else{
      $tabela     = SystemProgram::where('id','>', 16)->get();
    }

    return view('system.programas',['tabela' => $tabela]);
  }
  public function show()
  {
    if(Gate::denies('system.programas.listar')){
      abort(403,"Não autorizado!");
    }
    $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
    $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
    if($grupoUser){
      $query     = SystemProgram::select('id','name','controller');
    }else{
      $query     = SystemProgram::select('id','name', 'controller')->where('id','>', 16);
    }
    if(!empty($session_id)){
      $query->where('id','=',$session_id);
    }
    if(!empty($session_name)){
      $query->where('name', 'ILIKE', "%$session_name%");
    }
    $tabela = $query->orderBy('id', 'asc')->get();

    return view('system.programas',['tabela' => $tabela]);
  }
  public function edit($id)
  {
    if(Gate::denies('system.programas.editar')){
      abort(403,"Não autorizado!");
    }
      $campos = SystemProgram::where('id',$id)->firstOrFail();
      if($campos){
        return view('system.programasform',['campos' => $campos]);
      }else{
        $tabela = SystemProgram::get();
        return view('system.programas',['tabela' => $tabela]);
      }
  }
  public function update(Request $request,$id)
  {
    if(Gate::denies('system.programas.atualizar')){
      abort(403,"Não autorizado!");
    }
      $this->validate($request, [
        'name'  => 'required',
        'controller'  => 'required',
    ],[],[
        'name'  => '"descrição"',
        'controller'  => '"nome"',

      ]);

      $dados = SystemProgram::findOrFail($id);
      $dados->name  = $request->name;
      $dados->controller  = $request->controller;
      $dados->save();

      if($dados){
          alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Problema ao alterado registro!')->persistent("Fechar");
      }
      return redirect()->route('system.programas');

  }
  public function create()
  {
    if(Gate::denies('system.programas.cadastrar')){
      abort(403,"Não autorizado!");
    }
        return view('system.programasform');
  }
  public function store(Request $request)
  {
    if(Gate::denies('system.programas.salvar')){
      abort(403,"Não autorizado!");
    }
      $this->validate($request, [
          'name'  => 'required',
          'controller'  => 'required|unique:system_program',
      ],[],[
          'name'  => '"descrição"',
          'controller'  => '"nome"',

      ]);

      $dados = new SystemProgram;
      $dados->name  = $request->name;
      $dados->controller  = $request->controller;
      $dados->save();

      if($dados){
          alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
      }
      return redirect()->route('system.programas');

  }
  public function destroy($id){
    if(Gate::denies('system.programas.deletar')){
      abort(403,"Não autorizado!");
    }
    $dados = SystemProgram::where('id',$id)->firstOrFail();
    $dados->delete();

    if($dados){
        alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
    }else{
        alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
    }
    return redirect()->route('system.programas.listar');
  }
  public function search(Request $request){
    if(Gate::denies('system.programas.procurar')){
      abort(403,"Não autorizado!");
    }
    $search = array();
    $search['id'] = $request->id;
    $search['name'] = $request->name;
    session(['search' => $search]);


    $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

    $grupoUser = SystemGroupUser::classGrupo999(Auth::user()['id']);
    if($grupoUser){
      $query     = SystemProgram::select('id','name','controller');
    }else{
      $query     = SystemProgram::select('id','name', 'controller')->where('id','>', 16);
    }

    if(!empty($session_id)){
      $query->where('id','=',$session_id);
    }
    if(!empty($session_name)){
      $query->where('name', 'ILIKE', "%$session_name%");
    }
    $tabela = $query->orderBy('id', 'asc')->get();

    return view('system.programas',['tabela' => $tabela]);
  }


}
