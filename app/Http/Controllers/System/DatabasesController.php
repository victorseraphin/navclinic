<?php

namespace App\Http\Controllers\System;


use App\Http\Requests\Tenant\StoreUpdateCompanyFormRequest;
use App\Events\Tenant\DatabaseCreated;
use App\Models\System\SystemDatabase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\Tenant\CompanyCreated;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class DatabasesController extends Controller
{
  private $company;

  public function __construct(SystemDatabase $company)
  {
    $this->company = $company;

  }
    public function index()
    {
      if(Gate::denies('system.databases')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $tabela = SystemDatabase::get();
      return view('system.databases',['tabela' => $tabela]);
    }

    public function create(){
      if(Gate::denies('system.databases.cadastrar')){
        abort(403,"Não autorizado!");
      }
      return view('system.databasesform');
    }
    public function store(StoreUpdateCompanyFormRequest $request){
      if(Gate::denies('system.databases.salvar')){
        abort(403,"Não autorizado!");
      }
      $company = $this->company->create($request->all());

       if ($request->has('create_database'))

           event(new CompanyCreated($company));
       else
           event(new DatabaseCreated($company));

           if($company){
               alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
           }else{
               alert()->error('Problema ao alterado registro!')->persistent("Fechar");
           }
           return redirect()->route('system.databases');
    }
    public function show()
    {
      if(Gate::denies('system.databases.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = SystemDatabase::select('id','name');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.databases',['tabela' => $tabela]);
    }
    public function edit($id){
      if(Gate::denies('system.databases.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = SystemDatabase::where('id',$id)->firstOrFail();
        if($campos){
          return view('system.databasesform',['campos' => $campos]);
        }else{
          $tabela = SystemDatabase::get();
          return view('system.databases',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id){
      if(Gate::denies('system.databases.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
            'name'  => 'required',
        ],[],[
            'name'  => 'nome',

        ]);

        $dados = SystemDatabase::findOrFail($id);
        $dados->name  = $request->name;
        $dados->cnpj_cpf  = $request->cnpj_cpf;
        $dados->domain  = $request->domain;
        $dados->bd_database  = $request->bd_database;
        $dados->bd_hostname  = $request->bd_hostname;
        $dados->bd_username  = $request->bd_username;
        $dados->bd_password  = $request->bd_password;
        $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.databases');

    }

    public function destroy($id){
      if(Gate::denies('system.databases.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = SystemDatabase::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.databases.listar');
    }
    public function search(Request $request){
      if(Gate::denies('system.databases.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['name'] = $request->name;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = SystemDatabase::select('id','name');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.databases',['tabela' => $tabela]);
    }


}
