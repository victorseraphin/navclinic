<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\System\SystemUnit;


class EmpresasController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.empresas')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $tabela = SystemUnit::get();
      return view('system.empresas',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('system.empresas.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = SystemUnit::select('id','name');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.empresas',['tabela' => $tabela]);
    }
    public function edit($id)
    {
      if(Gate::denies('system.empresas.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = SystemUnit::where('id',$id)->firstOrFail();
        if($campos){
          return view('system.empresasform',['campos' => $campos]);
        }else{
          $tabela = SystemUnit::get();
          return view('system.empresas',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id)
    {
      if(Gate::denies('system.empresas.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
            'name'  => 'required',

        ],[],[
            'name'  => '"nome"',


        ]);

        $dados = SystemUnit::findOrFail($id);
        $dados->name  = $request->name;
        $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.empresas.listar');

    }
    public function create()
    {
      if(Gate::denies('system.empresas.cadastrar')){
        abort(403,"Não autorizado!");
      }
          return view('system.empresasform');
    }
    public function store(Request $request)
    {
      if(Gate::denies('system.empresas.salvar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'name'  => 'required',
      ],[],[
          'name'  => '"nome"',

        ]);

        $dados = new SystemUnit;
        $dados->name  = $request->name;
        $dados->save();

        if($dados){
            alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('system.empresas.listar');

    }
    public function destroy($id)
    {
      if(Gate::denies('system.empresas.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = SystemUnit::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.empresas.listar');
    }
    public function search(Request $request)
    {
      if(Gate::denies('system.empresas.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['name'] = $request->name;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = SystemUnit::select('id','name');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('name', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.empresas',['tabela' => $tabela]);
    }


}
