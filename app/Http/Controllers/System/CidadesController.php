<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\System\Cidades;
use App\Models\System\Estados;


class CidadesController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.cidades')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $tabela = Cidades::get();
      return view('system.cidades',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('system.cidades.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = Cidades::select('id','cid_nome');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('cid_nome', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.cidades',['tabela' => $tabela]);
    }
    public function edit($id)
    {
      if(Gate::denies('system.cidades.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = Cidades::where('id',$id)->firstOrFail();
        $estados = Estados::get();
        if($campos){
          return view('system.cidadesform',['campos' => $campos,'estados'=>$estados]);
        }else{
          $tabela = Cidades::get();
          return view('system.cidades',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id)
    {
      if(Gate::denies('system.cidades.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
            'cid_nome'  => 'required',
            'cod_ibge'  => 'required',
            'est_id'  => 'required',
        ],[],[
            'cid_nome'  => '"nome"',
            'cod_ibge'  => '"ibge"',
            'est_id'  => '"uf"',

        ]);

        $dados = Cidades::findOrFail($id);
        $dados->cid_nome  = $request->cid_nome;
        $dados->cod_ibge  = $request->cod_ibge;
        $dados->est_id  = $request->est_id;
        $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.cidades.listar');

    }
    public function create()
    {
      if(Gate::denies('system.cidades.cadastrar')){
        abort(403,"Não autorizado!");
      }
      $estados = Estados::get();
      return view('system.cidadesform',['estados'=>$estados]);
    }
    public function store(Request $request)
    {
      if(Gate::denies('system.cidades.salvar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'cid_nome'  => 'required',
          'cod_ibge'  => 'required',
          'est_id'  => 'required',
      ],[],[
          'cid_nome'  => '"nome"',
          'cod_ibge'  => '"ibge"',
          'est_id'  => '"uf"',

        ]);

        $dados = new Cidades;
        $dados->name  = $request->name;
        $dados->save();

        if($dados){
            alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('system.cidades.listar');

    }
    public function destroy($id)
    {
      if(Gate::denies('system.cidades.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = Cidades::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.cidades.listar');
    }
    public function search(Request $request)
    {
      if(Gate::denies('system.cidades.procurar')){
        abort(403,"Não autorizado!");
      }
      $search = array();
      $search['id'] = $request->id;
      $search['name'] = $request->name;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_name = (isset(session('search')['name']) ? session('search')['name'] : null);

      $query = Cidades::select('id','cid_nome');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_name)){
        $query->where('cid_nome', 'ILIKE', "%$session_name%");
      }
      $tabela = $query->orderBy('id', 'asc')->get();

      return view('system.cidades',['tabela' => $tabela]);
    }


}
