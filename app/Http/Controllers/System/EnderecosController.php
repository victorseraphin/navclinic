<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\System\Endereco;
use App\Models\System\Cidades;
use App\Models\System\Cep;
use Illuminate\Support\Facades\Auth;


class EnderecosController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.enderecos')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      session()->forget(['plano']);
      $id =  Auth::user()['id'];
      $tabela = Endereco::select('enderecos.id','enderecos.end_descricao','enderecos.end_numero','enderecos.loc_ceps_id','enderecos.end_padrao')
      ->join('system_user', function($join) use($id){
        $join->on('enderecos.system_user_id', '=', 'system_user.id')
        ->where('system_user.id', '=', $id);
      })->get();

      return view('system.enderecos',['tabela' => $tabela]);
    }
    public function show()
    {
      if(Gate::denies('system.enderecos.listar')){
        abort(403,"Não autorizado!");
      }
      $id =  Auth::user()['id'];
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_end_descricao = (isset(session('search')['end_descricao']) ? session('search')['end_descricao'] : null);

      $query = Endereco::select('enderecos.id','enderecos.end_descricao','enderecos.end_numero','enderecos.loc_ceps_id','enderecos.end_padrao')
      ->join('system_user', function($join) use($id){
        $join->on('enderecos.system_user_id', '=', 'system_user.id')
        ->where('system_user.id', '=', $id);
      });

      if(!empty($session_id)){
        $query->where('enderecos.id','=',$session_id);
      }
      if(!empty($session_end_descricao)){
        $query->where('enderecos.end_descricao', 'ILIKE', "%$session_end_descricao%");
      }

      $tabela = $query->orderBy('enderecos.id', 'asc')->get();
      return view('system.enderecos',['tabela' => $tabela]);
    }
    public function edit($id)
    {
      if(Gate::denies('system.enderecos.editar')){
        abort(403,"Não autorizado!");
      }
        $campos = Endereco::where('id',$id)->firstOrFail();
        //$especies = Especie::get();
        if($campos){
          return view('system.enderecosform',['campos' => $campos]);
        }else{
          $tabela = Endereco::get();
          return view('system.enderecos',['tabela' => $tabela]);
        }
    }
    public function update(Request $request,$id)
    {
      if(Gate::denies('system.enderecos.atualizar')){
        abort(403,"Não autorizado!");
      }
        $this->validate($request, [
          'end_descricao'  => 'required',
          'end_numero'  => 'required',
      ],[],[
          'end_descricao'  => '"endereço"',
          'end_numero'  => '"numero"',

        ]);

        $cep = preg_replace("/[^0-9]/", "", $request->loc_ceps_id);

       $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

       //verifica se o cep esta cadastrado
       if($id_cep == null){
         //verifica se a cidade existe
         $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

         if($cid_id == null){
           $est_id = DB::table('loc_estados')->select('id')->where('est_uf',$request->end_uf)->first();
           $cidade = new Cidades;
           $cidade->cod_ibge = $request->get('cod_ibge');
           $cidade->cid_nome = $request->get('end_descricao');
           $cidade->est_id   = $est_id->id;
           $cidade->save();

           $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

           $loc_cep = new Cep;
           $loc_cep->id = $cep;
           $loc_cep->cid_id = $cid_id->id;
           $loc_cep->rua_nome = $request->get('rua_nome');
           $loc_cep->bairro_nome = $request->get('bairro_nome');
           $loc_cep->cod_ibge = $request->get('cod_ibge');
           $loc_cep->unidade = $request->get('unidade');
           $loc_cep->gia = $request->get('gia');
           $loc_cep->save();

         }else{
           $loc_cep = new Cep;
           $loc_cep->id = $cep;
           $loc_cep->cid_id = $cid_id->id;
           $loc_cep->rua_nome = $request->get('rua_nome');
           $loc_cep->bairro_nome = $request->get('bairro_nome');
           $loc_cep->cod_ibge = $request->get('cod_ibge');
           $loc_cep->unidade = $request->get('unidade');
           $loc_cep->gia = $request->get('gia');
           $loc_cep->save();
         }
       }
       $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

       $dados = Endereco::findOrFail($id);
       $dados->system_user_id   = Auth::user()['id'];
       $dados->loc_ceps_id      = $id_cep->id;
       $dados->end_descricao    = $request->end_descricao;
       $dados->end_numero       = $request->end_numero;
       $dados->save();

        if($dados){
            alert()->success('Registro alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Problema ao alterado registro!')->persistent("Fechar");
        }
        return redirect()->route('system.enderecos');

    }
    public function create()
    {
      if(Gate::denies('system.enderecos.cadastrar')){
        abort(403,"Não autorizado!");
      }
      return view('system.enderecosform');
    }
    public function store(Request $request)
    {
      if(Gate::denies('system.enderecos.salvar')){
        abort(403,"Não autorizado!");
      }
      $this->validate($request, [
        'end_descricao'  => 'required',
        'end_numero'  => 'required',
      ],[],[
        'end_descricao'  => '"endereço"',
        'end_numero'  => '"numero"',
      ]);
      //dd($request->loc_ceps_id);
      $cep = preg_replace("/[^0-9]/", "", $request->loc_ceps_id);

      $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

      //verifica se o cep esta cadastrado
      if($id_cep == null){
        //verifica se a cidade existe
        $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

        if($cid_id == null){
          $est_id = DB::table('loc_estados')->select('id')->where('est_uf',$request->end_uf)->first();
          $cidade = new Cidades;
          $cidade->cod_ibge = $request->get('cod_ibge');
          $cidade->cid_nome = $request->get('cidade_nome');
          $cidade->est_id   = $est_id->id;
          $cidade->save();

          $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

          $loc_cep = new Cep;
          $loc_cep->id = $cep;
          $loc_cep->cid_id = $cid_id->id;
          $loc_cep->rua_nome = $request->get('rua_nome');
          $loc_cep->bairro_nome = $request->get('bairro_nome');
          $loc_cep->cod_ibge = $request->get('cod_ibge');
          $loc_cep->unidade = $request->get('unidade');
          $loc_cep->gia = $request->get('gia');
          $loc_cep->save();

        }else{
          $loc_cep = new Cep;
          $loc_cep->id = $cep;
          $loc_cep->cid_id = $cid_id->id;
          $loc_cep->rua_nome = $request->get('rua_nome');
          $loc_cep->bairro_nome = $request->get('bairro_nome');
          $loc_cep->cod_ibge = $request->get('cod_ibge');
          $loc_cep->unidade = $request->get('unidade');
          $loc_cep->gia = $request->get('gia');
          $loc_cep->save();
        }
      }
      $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();
      $dados = new Endereco;
      $dados->system_user_id   = Auth::user()['id'];
      $dados->loc_ceps_id      = $id_cep->id;
      $dados->end_descricao    = $request->end_descricao;
      $dados->end_numero       = $request->end_numero;
      $dados->end_padrao       = 'N';
      $dados->save();


      if($dados){
        alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
        alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
      }
      $search['url'] = 'site.finaliza_pedido';
      session(['voltar_rota' => $search]);

      if(isset(session('plano')['id']) and isset(session('plano')['url'])){
        return redirect()->route(session('plano')['url'],session('plano')['id']);
      }elseif(isset(session('plano')['url'])){
        return redirect()->route(session('plano')['url']);
      }
      return redirect()->route('system.enderecos');

    }
    public function destroy($id)
    {
      if(Gate::denies('system.enderecos.deletar')){
        abort(403,"Não autorizado!");
      }
      $dados = Endereco::where('id',$id)->firstOrFail();
      $dados->delete();

      if($dados){
          alert()->success('Registro excluido com sucesso.','Sucesso!')->persistent("Fechar");
      }else{
          alert()->error('Registro não pode ser excluído!')->persistent("Fechar");
      }
      return redirect()->route('system.enderecos.listar');
    }
    public function search(Request $request)
    {
      if(Gate::denies('system.enderecos.procurar')){
        abort(403,"Não autorizado!");
      }
      $id =  Auth::user()['id'];
      $search = array();
      $search['id'] = $request->id;
      $search['end_descricao'] = $request->end_descricao;
      session(['search' => $search]);


      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_end_descricao = (isset(session('search')['end_descricao']) ? session('search')['end_descricao'] : null);

      $query = Endereco::select('enderecos.id','enderecos.end_descricao','enderecos.end_numero','enderecos.loc_ceps_id','enderecos.end_padrao')
      ->join('system_user', function($join) use($id){
        $join->on('enderecos.system_user_id', '=', 'system_user.id')
        ->where('system_user.id', '=', $id);
      });
      if(!empty($session_id)){
        $query->where('enderecos.id','=',$session_id);
      }
      if(!empty($session_end_descricao)){
        $query->where('enderecos.end_descricao', 'ILIKE', "%$session_end_descricao%");
      }
      $tabela = $query->orderBy('enderecos.id', 'asc')->get();
      return view('system.enderecos',['tabela' => $tabela]);
    }
    public function padrao($id){

        /*if(Auth::user()['admin'] == 0){
            return redirect()->guest(route('admin.dashboard'));
        }*/

        $user =  Auth::user()['id'];

        $todosdados = Endereco::where('system_user_id',$user)->get();
        foreach($todosdados as $value){
          $dados = Endereco::findOrFail($value->id);
          $dados->end_padrao       = 'N';
          $dados->save();

        }

        $dados = Endereco::findOrFail($id);

        if($dados){
            if($dados->end_padrao != 'Y' ){
                DB::table('enderecos')
                ->where('id', $id)
                ->update(['end_padrao' => 'Y']);
            }
            else{
                DB::table('enderecos')
                ->where('id', $id)
                ->update(['end_padrao' => 'N']);
            }
        }

        if($dados){
            alert()->success('Registro Alterado com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
            alert()->error('Registro não pode ser alterado!')->persistent("Fechar");
        }
        return redirect()->route('system.enderecos.listar');
    }


}
