<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class DashboardController extends Controller
{

    public function index()
    {
      if(Gate::denies('system.dashboard')){
        abort(403,"Não autorizado!");
      }
      $user = Auth::user();
      return view('system.dashboard');
    }

}
