<?php

namespace App\Http\Controllers\Agenda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Cadastros\Clientes;
use App\Models\Agenda\Agenda;
use App\Models\Prontuario\Prontuario;
use App\Models\System\Cidades;
use App\Models\System\Estados;
use App\Models\System\Cep;
use App\Models\System\SystemUser;

class AgendaController extends Controller
{

    public function index()
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }
      session()->forget(['search']);
      $cliente = Clientes::where('system_unit_id','=',Auth::user()['system_unit_id'])->get();
      $profissional = SystemUser::where('system_unit_id','=',Auth::user()['system_unit_id'])
                                ->where('login','=','P')->where('active','=','Y')->get();

      $query = Agenda::select('agenda.id','clientes.nome','clientes.email','clientes.fone','agenda.data','agenda.hora_ini','agenda.hora_fin','agenda.status')
                      ->where('agenda.system_unit_id','=',Auth::user()['system_unit_id']);

      if(Auth::user()['login'] == 'P'){
        $query->where('profissional_id','=',Auth::user()['id']);
      }
      $query->join('system_user', 'agenda.profissional_id', '=', 'system_user.id');
      $query->join('clientes', 'agenda.clientes_id', '=', 'clientes.id');
      $agenda = $query->get();
      return view('agenda.agenda',['cliente' => $cliente,'profissional'=>$profissional,'agenda'=>$agenda]);
    }
    public function show()
    {
      if(Gate::denies('agenda.listar')){
        abort(403,"Não autorizado!");
      }
      $session_id = (isset(session('search')['id']) ? session('search')['id'] : null);
      $session_nome = (isset(session('search')['nome']) ? session('search')['nome'] : null);

      $query = Clientes::select('*');
      if(!empty($session_id)){
        $query->where('id','=',$session_id);
      }
      if(!empty($session_nome)){
        $query->where('nome', 'ILIKE', "%$session_nome%");
      }
      $tabela = $query->orderBy('nome', 'asc')->get();

      return view('agenda.agenda',['tabela' => $tabela]);
    }

    public function store(Request $request)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = new Agenda;
      $dados->profissional_id   = $request->profissional_id;
      $dados->clientes_id       = $request->clientes_id;
      $dados->data              = $request->data;
      $dados->hora_ini          = $request->hora_ini;
      $dados->hora_fin          = $request->hora_fin;
      $dados->system_unit_id    = Auth::user()['system_unit_id'];
      $dados->save();

      $dados2 = new Prontuario;
      $dados2->profissional_id   = $request->profissional_id;
      $dados2->clientes_id       = $request->clientes_id;
      $dados2->agenda_id         = $dados->id;
      $dados2->data              = $request->data;
      $dados2->queixa            = $request->queixa;
      $dados2->system_unit_id    = Auth::user()['system_unit_id'];
      $dados2->save();

      if($dados){
        alert()->success('Agendamento incluído.','Sucesso!')->persistent("Fechar");
      }else{
        alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
      }
      return redirect()->route('agenda');

    }
    public function confirmado($id)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = Agenda::findOrFail($id);
      $dados->status  = 1;
      $dados->save();
      return redirect()->route('agenda');
    }
    public function cancelado($id)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = Agenda::findOrFail($id);
      $dados->status  = 2;
      $dados->save();
      return redirect()->route('agenda');
    }
    public function aguardando($id)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = Agenda::findOrFail($id);
      $dados->status  = 3;
      $dados->save();
      return redirect()->route('agenda');
    }
    public function atender($id)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = Agenda::findOrFail($id);
      $dados->status  = 4;
      $dados->save();

      $query = Prontuario::select('prontuario.id','prontuario.data','prontuario.queixa','prontuario.historico','prontuario.observacao',
                                  'prontuario.palpacao','prontuario.evolucao','agenda.hora_ini');

      $query->where('prontuario.clientes_id','=',$dados->clientes_id);
      $query->where('prontuario.agenda_id','=',$id);
      $query->join('agenda', 'prontuario.agenda_id', '=', 'agenda.id');

      $campos = $query->orderBy('data', 'desc')->firstOrFail();

      return view('agenda.atender',['campos' => $campos]);
    }
    public function atender_store(Request $request, $id)
    {
      if(Gate::denies('agenda')){
        abort(403,"Não autorizado!");
      }

      $dados = Prontuario::findOrFail($id);
      $dados->queixa        = $request->queixa;
      $dados->historico     = $request->historico;
      $dados->observacao    = $request->observacao;
      $dados->palpacao      = $request->palpacao;
      $dados->evolucao      = $request->evolucao;
      $dados->valor         = $request->valor;
      $dados->status        ='5';
      $dados->save();


      $dados2 = Agenda::findOrFail($dados->agenda_id);
      $dados2->status  = 5;
      $dados2->save();


      return redirect()->route('agenda');
    }
    public function getby($id)
    {
      $json_data = array();
      $query = Agenda::select('clientes.nome','clientes.email','clientes.fone','clientes.nascimento','system_user.name',
                              'agenda.data','agenda.hora_ini','agenda.hora_fin','agenda.status','prontuario.queixa')
                      ->where('agenda.system_unit_id','=',Auth::user()['system_unit_id'])->where('agenda.id','=',$id);

      if(Auth::user()['login'] == 'P'){
        $query->where('agenda.profissional_id','=',Auth::user()['id']);
      }
      $query->join('system_user', 'agenda.profissional_id', '=', 'system_user.id');
      $query->join('clientes', 'agenda.clientes_id', '=', 'clientes.id');
      $query->leftjoin('prontuario', 'agenda.id', '=', 'prontuario.agenda_id');
      $agenda = $query->get();

  	  foreach ($agenda as $linha):
        $e = array();
        $c = explode('-', $linha->data);
        $diasemana = date("w", mktime(0,0,0,$c[1],$c[2],$c[0]) );
        switch($diasemana) {
              case"0": $diasemana = "domingo";       break;
              case"1": $diasemana = "segunda-feira"; break;
              case"2": $diasemana = "terça-feira";   break;
              case"3": $diasemana = "quarta-feira";  break;
              case"4": $diasemana = "quinta-feira";  break;
              case"5": $diasemana = "sexta-feira";   break;
              case"6": $diasemana = "sábado";        break;
        }

        $e['nome_profissional'] = $linha->name;
        $e['nome']              = $linha->nome;
        $e['email']             = $linha->email;
        $e['tel']               = $linha->fone;
        $e['nascimento']        = date('d/m/Y',strtotime($linha->nascimento));
        $e['data']              = date('d/m/Y',strtotime($linha->data));
        $e['hora_inicio']       = $linha->hora_ini;
        $e['hora_final']        = $linha->hora_fin;
        $e['dia_semana']        = $diasemana;
        $e['procedimento']      = $linha->queixa;
        $e['status']            = $linha->status;

        if($linha->status == 0){
          $e['flag'] = "Aguardando a confirmação  do cliente";
          $e['cor_flag'] = "#A4A4A4";
        }
        elseif($linha->status == 1){
          $e['flag'] = "O cliente já esta confirmado";
          $e['cor_flag'] = "#00cc00";
        }
        elseif($linha->status == 2){
          $e['flag'] = "O agendamento foi cancelado";
          $e['cor_flag'] = "#ff6666";
        }
        elseif($linha->status == 3){
          $e['flag'] = "O cliente já está aguardando";
          $e['cor_flag'] = "#FFA500";
        }
        elseif($linha->status == 4){
          $e['flag'] = "O cliente está sendo atendido";
          $e['cor_flag'] = "#ff3399";
        }
        elseif($linha->status == 5){
          $e['flag'] = "O cliente já foi atendido";
          $e['cor_flag'] = "#0099ff";
        }
        else{
          $e['flag'] = "Aguardando a confirmação  do cliente";
          $e['cor_flag'] = "#A4A4A4";
        }

        array_push($json_data, $e);

      endforeach;

      //dd($json_data);
      return $json_data;
    }
    public function novo_cliente(Request $request)
    {      
        $this->validate($request, [
          'nome'  => 'required',
          'sexo'  => 'required',
      ],[],[
          'nome'  => '"nome"',
          'sexo'  => '"sexo"',

      ]);
      $cpf_exite = Clientes::where('cpf',$request->cpf)->first();
      //dd($cpf_exite);
      if($cpf_exite != null and $request->cpf != null){
        alert()->error('CPF já cadastrado no sistema!')->persistent("Fechar");
        return redirect()->back()->withInput();
      }else{

        if($request->loc_ceps_id != null){
          $cep = preg_replace("/[^0-9]/", "", $request->loc_ceps_id);
          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

          //verifica se o cep esta cadastrado
          if($id_cep == null){
            //verifica se a cidade existe
            $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

            if($cid_id == null){
              $est_id = DB::table('loc_estados')->select('id')->where('est_uf',$request->end_uf)->first();
              $cidade = new Cidades;
              $cidade->cod_ibge = $request->get('cod_ibge');
              $cidade->cid_nome = $request->get('cidade_nome');
              $cidade->est_id   = $est_id->id;
              $cidade->save();

              $cid_id = DB::table('loc_cidades')->select('id')->where('cod_ibge',$request->cod_ibge)->first();

              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();

            }else{
              $loc_cep = new Cep;
              $loc_cep->id = $cep;
              $loc_cep->cid_id = $cid_id->id;
              $loc_cep->rua_nome = $request->get('rua_nome');
              $loc_cep->bairro_nome = $request->get('bairro_nome');
              $loc_cep->cod_ibge = $request->get('cod_ibge');
              $loc_cep->unidade = $request->get('unidade');
              $loc_cep->gia = $request->get('gia');
              $loc_cep->save();
            }
          }
          $id_cep = DB::table('loc_ceps')->select('id')->where('id',$cep)->first();

        }



        $dados = new Clientes;
        $dados->nome              = $request->nome;
        $dados->system_unit_id    = Auth::user()['system_unit_id'];
        if($request->loc_ceps_id != null){
          $dados->loc_ceps_id     = $id_cep->id;
        }else{
          $dados->loc_ceps_id = null;
        }
        $dados->email             = $request->email;
        $dados->cpf               = $request->cpf;
        $dados->fone              = $request->fone;
        $dados->sexo              = $request->sexo;
        $dados->nascimento        = $request->nascimento;
        $dados->end_descricao     = $request->end_descricao;
        $dados->end_numero        = $request->end_numero;
        $dados->save();

        if($dados){
          alert()->success('Registro incluído com sucesso.','Sucesso!')->persistent("Fechar");
        }else{
          alert()->error('Problema ao cadastrar novo registro!')->persistent("Fechar");
        }
        return redirect()->route('agenda');
      }

    }


}
