<?php
use App\Models\System\Endereco;

if (!function_exists('classActivePath')) {
    function classActivePath($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? ' menu-open' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return ' menu-open';
        }
        return '';
    }
}

if (!function_exists('classActivePathActive')) {
    function classActivePathActive($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? 'active' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return 'active';
        }
        return '';
    }
}
if (!function_exists('classTitulo')) {
    function classTitulo($segment, $value)
    {
        return Request::segment($segment) == $value;
    }
}
if (!function_exists('classActiveSegment')) {
    function classActiveSegment($segment, $value)
    {
      if(!is_array($value)) {
        $array = explode('.', $value);
        return Request::segment($segment) == $array[1] && Request::segment(1) == $array[0] ? 'active' : '';
      }
      else{
        foreach ($value as $v) {
          $array = explode('.', $v);
          if(Request::segment($segment) == $array[1] && Request::segment(1) == $array[0]):
            return 'active';
          endif;

        }
      }
    }
}
if (!function_exists('class_user_incomplete')) {
    function class_user_incomplete()
    {
      if(Auth::user()['doc'] == null){
        return true;
      }
      else{
        return false;
      }
    }
}
if (!function_exists('class_ender_incomplete')) {
    function class_ender_incomplete()
    {
      $ender = Endereco::where('system_user_id',Auth::user()['id'])->first();
      if($ender == null){
        return true;
      }
      else{
        return false;
      }
    }
}
