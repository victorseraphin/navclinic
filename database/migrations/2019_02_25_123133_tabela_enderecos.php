<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('enderecos', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('system_user_id')->nullable();
          $table->foreign('system_user_id')->references('id')->on('system_user')->onDelete('cascade');

          $table->unsignedInteger('loc_ceps_id')->nullable();
          $table->foreign('loc_ceps_id')->references('id')->on('loc_ceps')->onDelete('cascade');

          $table->string('end_descricao',200);
          $table->string('end_numero',8);
          $table->string('end_padrao',1);

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
