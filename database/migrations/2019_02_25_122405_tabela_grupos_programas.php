<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaGruposProgramas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('system_group_program', function (Blueprint $table) {
          $table->increments('id');

          $table->unsignedInteger('system_group_id')->nullable();
          $table->foreign('system_group_id')->references('id')->on('system_group')->onDelete('cascade');

          $table->unsignedInteger('system_program_id')->nullable();
          $table->foreign('system_program_id')->references('id')->on('system_program')->onDelete('cascade');

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_group_program');
    }
}
