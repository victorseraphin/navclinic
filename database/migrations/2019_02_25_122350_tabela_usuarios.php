<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('system_user', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name',200);
          $table->string('razao',200)->nullable();
          $table->string('doc',20)->unique()->nullable();
          $table->string('fone',20)->nullable();
          $table->string('login',20)->nullable();
          $table->string('email',255)->unique();
          $table->string('password',255);

          $table->unsignedInteger('system_unit_id')->nullable();
          $table->foreign('system_unit_id')->references('id')->on('system_unit')->onDelete('cascade');

          $table->unsignedInteger('frontpage_id')->nullable();
          $table->foreign('frontpage_id')->references('id')->on('system_program')->onDelete('cascade');

          $table->string('facebook_id',255)->nullable();
          $table->string('google_id',255)->nullable();          
          $table->string('active',1)->default('Y');
          $table->rememberToken()->nullable();
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_user');
    }
}
