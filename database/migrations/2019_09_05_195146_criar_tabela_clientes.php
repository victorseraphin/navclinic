<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('clientes', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nome',200);
          $table->string('cpf',200)->nullable();
          $table->string('email',200)->nullable();
          $table->string('fone',200)->nullable();
          $table->date('nascimento')->nullable();
          $table->string('sexo',1)->nullable();
          
          $table->unsignedInteger('system_unit_id')->nullable();
          $table->foreign('system_unit_id')->references('id')->on('system_unit')->onDelete('cascade');

          $table->unsignedInteger('loc_ceps_id')->nullable();
          $table->foreign('loc_ceps_id')->references('id')->on('loc_ceps')->onDelete('cascade');

          $table->string('end_descricao',200)->nullable();
          $table->string('end_numero',8)->nullable();

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('clientes');
    }
}
