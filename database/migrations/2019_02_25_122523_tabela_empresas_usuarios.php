<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaEmpresasUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('system_unit_user', function (Blueprint $table) {
          $table->increments('id');

          $table->unsignedInteger('system_user_id')->nullable();
          $table->foreign('system_user_id')->references('id')->on('system_user')->onDelete('cascade');

          $table->unsignedInteger('system_unit_id')->nullable();
          $table->foreign('system_unit_id')->references('id')->on('system_unit')->onDelete('cascade');

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_unit_user');
    }
}
