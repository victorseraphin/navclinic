<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaLocalidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // ------ localidades  ---------------------------------------
      Schema::create('loc_estados', function (Blueprint $table) {
          $table->integer('id');
          $table->primary('id');
          $table->string('est_nome');
          $table->string('est_uf');
          $table->timestamps();
          $table->softDeletes();
      });

      Schema::create('loc_cidades', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('cod_ibge');
          $table->string('cid_nome');
          $table->unsignedInteger('est_id');
          $table->foreign('est_id')->references('id')->on('loc_estados');
          $table->timestamps();
          $table->softDeletes();
      });

      Schema::create('loc_ceps', function (Blueprint $table) {
          $table->integer('id');
          $table->primary('id');
          $table->unsignedInteger('cid_id');
          $table->string('rua_nome');
          $table->string('bairro_nome');
          $table->string('unidade')->nullable();
          $table->string('cod_ibge')->nullable();
          $table->string('gia')->nullable();
          $table->foreign('cid_id')->references('id')->on('loc_cidades');
          $table->timestamps();
          $table->softDeletes();
      });
      // ------ localidades  ---------------------------------------
    }

    public function down()
    {
      Schema::dropIfExists('loc_ceps');
      Schema::dropIfExists('loc_cidades');
      Schema::dropIfExists('loc_estados');
    }
}
