<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaGruposUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('system_group_user', function (Blueprint $table) {
          $table->increments('id');

          $table->unsignedInteger('system_user_id')->nullable();
          $table->foreign('system_user_id')->references('id')->on('system_user')->onDelete('cascade');

          $table->unsignedInteger('system_group_id')->nullable();
          $table->foreign('system_group_id')->references('id')->on('system_group')->onDelete('cascade');

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_group_user');
    }
}
