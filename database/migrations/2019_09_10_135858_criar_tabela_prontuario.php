<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaProntuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('prontuario', function (Blueprint $table) {
          $table->increments('id');

          $table->unsignedInteger('profissional_id')->nullable();
          $table->foreign('profissional_id')->references('id')->on('system_user')->onDelete('cascade');

          $table->unsignedInteger('clientes_id')->nullable();
          $table->foreign('clientes_id')->references('id')->on('clientes')->onDelete('cascade');

          $table->unsignedInteger('agenda_id')->nullable();
          $table->foreign('agenda_id')->references('id')->on('agenda')->onDelete('cascade');

          $table->text('queixa')->nullable();
          $table->text('historico')->nullable();
          $table->text('observacao')->nullable();
          $table->text('palpacao')->nullable();
          $table->text('evolucao')->nullable();

          $table->date('data');
          $table->string('status',1)->nullable();
          $table->decimal('valor',10,2)->nullable();

          $table->unsignedInteger('system_unit_id')->nullable();
          $table->foreign('system_unit_id')->references('id')->on('system_unit')->onDelete('cascade');

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('prontuario');
    }
}
