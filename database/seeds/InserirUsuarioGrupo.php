<?php

use Illuminate\Database\Seeder;

class InserirUsuarioGrupo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('system_group_user')->delete();
      DB::table('system_group_user')->insert([
          'system_user_id' => 1,
          'system_group_id' => 999,
      ]);
      DB::table('system_group_user')->insert([
          'system_user_id' => 2,
          'system_group_id' => 1,
      ]);
      DB::table('system_group_user')->insert([
          'system_user_id' => 3,
          'system_group_id' => 2,
      ]);      
    }


}
