<?php

use Illuminate\Database\Seeder;

class InserirAdminTableUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('system_user')->delete();
      DB::table('system_user')->insert([
          'name' => 'Super Administrador',
          'email' => 'superadmin@email.com',
          'password' => bcrypt('Franc1sc0*'),
          'active' => 'Y',
      ]);
      DB::table('system_user')->insert([
          'name' => 'Profissional',
          'email' => 'profissional@email.com',
          'password' => bcrypt('Franc1sc0*'),
          'active' => 'Y',
      ]);
      DB::table('system_user')->insert([
          'name' => 'Secretario',
          'email' => 'secretario@email.com',
          'password' => bcrypt('Franc1sc0*'),
          'active' => 'Y',
      ]);
    }
}
