<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
          InserirGrupoAdmin::class,
          InserirAdminTableUsers::class,
          InserirUsuarioGrupo::class,
          inserirProgramasSeeder::class,
          InserirGruposProgramas::class,
          LocEstadosSeeder::class
      ]);
    }
}
