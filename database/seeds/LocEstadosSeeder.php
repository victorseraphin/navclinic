<?php

use Illuminate\Database\Seeder;

class LocEstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('loc_estados')->delete();
      DB::table('loc_estados')->insert(['id' =>	12	, 'est_nome' => 'Acre','est_uf' => 'AC']);
      DB::table('loc_estados')->insert(['id' =>	27	, 'est_nome' => 'Alagoas', 'est_uf' => 'AL']);
      DB::table('loc_estados')->insert(['id' =>	13	, 'est_nome' => 'Amazonas', 'est_uf' => 'AM']);
      DB::table('loc_estados')->insert(['id' =>	16	, 'est_nome' => 'Amapá', 'est_uf' => 'AP']);
      DB::table('loc_estados')->insert(['id' =>	29	, 'est_nome' => 'Bahia', 'est_uf' => 'BA']);
      DB::table('loc_estados')->insert(['id' =>	23	, 'est_nome' => 'Ceara', 'est_uf' => 'CE']);
      DB::table('loc_estados')->insert(['id' =>	53	, 'est_nome' => 'Distrito Federal', 'est_uf' => 'DF']);
      DB::table('loc_estados')->insert(['id' =>	32	, 'est_nome' => 'Espirito Santo', 'est_uf' => 'ES']);
      DB::table('loc_estados')->insert(['id' =>	52	, 'est_nome' => 'Goiás', 'est_uf' => 'GO']);
      DB::table('loc_estados')->insert(['id' =>	21	, 'est_nome' => 'Maranhão', 'est_uf' => 'MA']);
      DB::table('loc_estados')->insert(['id' =>	31	, 'est_nome' => 'Minas Gerais', 'est_uf' => 'MG']);
      DB::table('loc_estados')->insert(['id' =>	50	, 'est_nome' => 'Mato Grosso do Sul', 'est_uf' => 'MS']);
      DB::table('loc_estados')->insert(['id' =>	51	, 'est_nome' => 'Mato Grosso', 'est_uf' => 'MT']);
      DB::table('loc_estados')->insert(['id' =>	15	, 'est_nome' => 'Pará', 'est_uf' => 'PA']);
      DB::table('loc_estados')->insert(['id' =>	25	, 'est_nome' => 'Paraíba', 'est_uf' => 'PB']);
      DB::table('loc_estados')->insert(['id' =>	26	, 'est_nome' => 'Pernambuco', 'est_uf' => 'PE']);
      DB::table('loc_estados')->insert(['id' =>	22	, 'est_nome' => 'Piauí', 'est_uf' => 'PI']);
      DB::table('loc_estados')->insert(['id' =>	41	, 'est_nome' => 'Paraná', 'est_uf' => 'PR']);
      DB::table('loc_estados')->insert(['id' =>	33	, 'est_nome' => 'Rio de Janeiro', 'est_uf' => 'RJ']);
      DB::table('loc_estados')->insert(['id' =>	24	, 'est_nome' => 'Rio Grande do Norte', 'est_uf' => 'RN']);
      DB::table('loc_estados')->insert(['id' =>	11	, 'est_nome' => 'Rondônia', 'est_uf' => 'RO']);
      DB::table('loc_estados')->insert(['id' =>	14	, 'est_nome' => 'Roraima', 'est_uf' => 'RR']);
      DB::table('loc_estados')->insert(['id' =>	43	, 'est_nome' => 'Rio Grande do Sul', 'est_uf' => 'RS']);
      DB::table('loc_estados')->insert(['id' =>	42	, 'est_nome' => 'Santa Catarina', 'est_uf' => 'SC']);
      DB::table('loc_estados')->insert(['id' =>	28	, 'est_nome' => 'Sergipe', 'est_uf' => 'SE']);
      DB::table('loc_estados')->insert(['id' =>	35	, 'est_nome' => 'São Paulo', 'est_uf' => 'SP']);
      DB::table('loc_estados')->insert(['id' =>	17	, 'est_nome' => 'Tocantins', 'est_uf' => 'TO']);
    }
}
