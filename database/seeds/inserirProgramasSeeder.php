<?php

use Illuminate\Database\Seeder;

class inserirProgramasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('system_program')->delete();

      DB::table('system_program')->insert(['name' => 'Listar banco de dados','controller' => 'system.databases']);
      DB::table('system_program')->insert(['name' => 'Editar banco de dados','controller' => 'system.databases.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar banco de dados','controller' => 'system.databases.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar banco de dados','controller' => 'system.databases.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar banco de dados','controller' => 'system.databases.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar banco de dados','controller' => 'system.databases.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar banco de dados','controller' => 'system.databases.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar banco de dados','controller' => 'system.databases.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar programas','controller' => 'system.programas']);
      DB::table('system_program')->insert(['name' => 'Editar programas','controller' => 'system.programas.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar programas','controller' => 'system.programas.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar programas','controller' => 'system.programas.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar programas','controller' => 'system.programas.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar programas','controller' => 'system.programas.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar programas','controller' => 'system.programas.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar programas','controller' => 'system.programas.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar grupos','controller' => 'system.grupos']);
      DB::table('system_program')->insert(['name' => 'Editar grupos','controller' => 'system.grupos.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar grupos','controller' => 'system.grupos.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar grupos','controller' => 'system.grupos.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar grupos','controller' => 'system.grupos.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar grupos','controller' => 'system.grupos.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar grupos','controller' => 'system.grupos.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar grupos','controller' => 'system.grupos.deletar']);

      DB::table('system_program')->insert(['name' => 'Exibe Dashboard SuperAdmin','controller' => 'system.dashboard']);

      DB::table('system_program')->insert(['name' => 'Listar empresas','controller' => 'system.empresas']);
      DB::table('system_program')->insert(['name' => 'Editar empresas','controller' => 'system.empresas.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar empresas','controller' => 'system.empresas.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar empresas','controller' => 'system.empresas.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar empresas','controller' => 'system.empresas.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar empresas','controller' => 'system.empresas.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar empresas','controller' => 'system.empresas.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar empresas','controller' => 'system.empresas.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar usuarios','controller' => 'system.usuarios']);
      DB::table('system_program')->insert(['name' => 'Editar usuarios','controller' => 'system.usuarios.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar usuarios','controller' => 'system.usuarios.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar usuarios','controller' => 'system.usuarios.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar usuarios','controller' => 'system.usuarios.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar usuarios','controller' => 'system.usuarios.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar usuarios','controller' => 'system.usuarios.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar usuarios','controller' => 'system.usuarios.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar preferencias','controller' => 'system.preferencias']);
      DB::table('system_program')->insert(['name' => 'Editar preferencias','controller' => 'system.preferencias.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar preferencias','controller' => 'system.preferencias.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar preferencias','controller' => 'system.preferencias.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar preferencias','controller' => 'system.preferencias.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar preferencias','controller' => 'system.preferencias.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar preferencias','controller' => 'system.preferencias.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar preferencias','controller' => 'system.preferencias.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar estados','controller' => 'system.estados']);
      DB::table('system_program')->insert(['name' => 'Editar estados','controller' => 'system.estados.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar estados','controller' => 'system.estados.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar estados','controller' => 'system.estados.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar estados','controller' => 'system.estados.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar estados','controller' => 'system.estados.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar estados','controller' => 'system.estados.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar estados','controller' => 'system.estados.deletar']);

      DB::table('system_program')->insert(['name' => 'Listar cidades','controller' => 'system.cidades']);
      DB::table('system_program')->insert(['name' => 'Editar cidades','controller' => 'system.cidades.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar cidades','controller' => 'system.cidades.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar cidades','controller' => 'system.cidades.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar cidades','controller' => 'system.cidades.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar cidades','controller' => 'system.cidades.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar cidades','controller' => 'system.cidades.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar cidades','controller' => 'system.cidades.deletar']);


      DB::table('system_program')->insert(['name' => 'Listar enderecos','controller' => 'system.enderecos']);
      DB::table('system_program')->insert(['name' => 'Editar enderecos','controller' => 'system.enderecos.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar enderecos','controller' => 'system.enderecos.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar enderecos','controller' => 'system.enderecos.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar enderecos','controller' => 'system.enderecos.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar enderecos','controller' => 'system.enderecos.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar enderecos','controller' => 'system.enderecos.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar enderecos','controller' => 'system.enderecos.deletar']);
      DB::table('system_program')->insert(['name' => 'Tornar endereco padrão','controller' => 'system.enderecos.padrao']);

      DB::table('system_program')->insert(['name' => 'Listar meus dados','controller' => 'meus_dados']);
      DB::table('system_program')->insert(['name' => 'Atualizar meus dados','controller' => 'meus_dados.atualizar']);




      /*--------------------------------------------- programs clientes  ----------------------------------------------*/

      DB::table('system_program')->insert(['name' => 'Listar clientes','controller' => 'cadastros.clientes']);
      DB::table('system_program')->insert(['name' => 'Editar clientes','controller' => 'cadastros.clientes.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar clientes','controller' => 'cadastros.clientes.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar clientes','controller' => 'cadastros.clientes.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar clientes','controller' => 'cadastros.clientes.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar clientes','controller' => 'cadastros.clientes.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar clientes','controller' => 'cadastros.clientes.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar clientes','controller' => 'cadastros.clientes.deletar']);

      /*DB::table('system_program')->insert(['name' => 'Listar atendimentos','controller' => 'parceiro.atendimentos']);
      DB::table('system_program')->insert(['name' => 'Editar atendimentos','controller' => 'parceiro.atendimentos.editar']);
      DB::table('system_program')->insert(['name' => 'Atualizar atendimentos','controller' => 'parceiro.atendimentos.atualizar']);
      DB::table('system_program')->insert(['name' => 'Cadastrar atendimentos','controller' => 'parceiro.atendimentos.cadastrar']);
      DB::table('system_program')->insert(['name' => 'Salvar atendimentos','controller' => 'parceiro.atendimentos.salvar']);
      DB::table('system_program')->insert(['name' => 'Pesquisar atendimentos','controller' => 'parceiro.atendimentos.procurar']);
      DB::table('system_program')->insert(['name' => 'Listar atendimentos','controller' => 'parceiro.atendimentos.listar']);
      DB::table('system_program')->insert(['name' => 'Deletar atendimentos','controller' => 'parceiro.atendimentos.deletar']);
      DB::table('system_program')->insert(['name' => 'Get Pet atendimentos','controller' => 'parceiro.atendimentos.get_pet_cli']);
      DB::table('system_program')->insert(['name' => 'Encerrar atendimentos','controller' => 'parceiro.atendimentos.encerrar']);
      DB::table('system_program')->insert(['name' => 'Visualizar atendimentos','controller' => 'parceiro.atendimentos.visualizar']);*/

    }
}
