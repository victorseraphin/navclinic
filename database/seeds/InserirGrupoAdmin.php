<?php

use Illuminate\Database\Seeder;

class InserirGrupoAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('system_group')->delete();
      DB::table('system_group')->insert([
        'id' => 999,
        'name' => 'SuperAdmin',
      ]);
      DB::table('system_group')->insert([
        'name' => 'Profissional',
      ]);
      DB::table('system_group')->insert([
        'name' => 'Secretario',
      ]);    
    }
}
