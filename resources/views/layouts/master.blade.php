<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content={{csrf_token()}}>

    <title>NavClinic</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}"></link>
    <link rel="stylesheet" href="{{ asset('dist/plugins/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}} ">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('dist/plugins/fullcalendar/fullcalendar/main.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/fullcalendar/fullcalendar-interaction/main.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/fullcalendar/fullcalendar-daygrid/main.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/fullcalendar/fullcalendar-timegrid/main.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/fullcalendar/fullcalendar-bootstrap/main.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- IonIcons
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />-->
    <link rel="stylesheet" href="{{ asset('css/dataTables.checkboxes.css')}} ">
</head>

<body class="hold-transition sidebar-mini">
    @guest @yield('content') @else
    <div class="wrapper" id="app">
        <!-- Header -->
    @include('layouts.header')
        <!-- Sidebar -->
    @include('layouts.sidebar') @yield('content')
        <!-- Footer -->
    @include('layouts.footer')
    <!--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    @include('sweet::alert')


    </div>
    <!-- ./wrapper -->
    @endguest
    @yield('javascript')

    <script src="{{ asset('js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('dist/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.js') }}"></script>
    <script>
      $(function () {
        //Initialize Select2 Elements
         $('.select2').select2()

         $('#profissional').select2({
             dropdownParent: $('#ModalAgendamento')
         });
         $('#cliente').select2({
             dropdownParent: $('#ModalAgendamento')
         });
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        ClassicEditor
          .create(document.querySelector('#textao'))
          .then(function (editor) {
            // The editor instance
          })
          .catch(function (error) {
            console.error(error)
          })

      })
    </script>
    <script>
      function SomenteNumero(e){
        var tecla=(window.event)?event.keyCode:e.which;
        if((tecla>47 && tecla<58) || tecla==46)
          return true;
        else{
            if (tecla==8 || tecla==0)
              return true;
        else
          return false;
          }
      }
    </script>
    <script type="text/javascript" >
      $(function () {
        $('[data-mask]').inputmask()
      });
      $(document).ready(function() {
        $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
        });
        $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
        });
      });


    function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('end_descricao').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('end_descricao').value=(conteudo.logradouro+" - "+conteudo.bairro+" - "+conteudo.localidade+" - "+conteudo.uf);
            document.getElementById('rua_nome').value=(conteudo.logradouro);
            document.getElementById('cidade_nome').value=(conteudo.localidade);
            document.getElementById('bairro_nome').value=(conteudo.bairro);
            document.getElementById('end_uf').value=(conteudo.uf);
            document.getElementById('cod_ibge').value=(conteudo.ibge);
            document.getElementById('unidade').value=(conteudo.unidade);
            document.getElementById('gia').value=(conteudo.gia);

        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('end_descricao').value="...";
                document.getElementById('end_uf').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };


    </script>

</body>
</html>
