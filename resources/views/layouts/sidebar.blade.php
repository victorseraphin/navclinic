<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('agenda')}}" class="brand-link">
      <img src="/img/logo.png" alt="Laravel Starter" class="fa fa-grav brand-image img-circle elevation-3"
     style="opacity: .8">
     <span class="brand-text font-weight-light">NavClinic</span>
   </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('meus_dados',Auth::user()['id'])}}" class="d-block"> {{auth()->user()->name!=null ? auth()->user()->name : "Administrator"}} </a>
                <a href="{{route('meus_dados',Auth::user()['id'])}}" >
                    <i style="font-size:9px;color:green" class="fa fa-circle"></i> <span style="font-size:12px;">Meu perfil</span>
                </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview {!! classActivePath(1,'agenda') !!}">
                    <a href="{!! route('agenda') !!}" class="nav-link {!! classActiveSegment(1, 'agenda.agenda') !!}">
                      <i class="nav-icon fa fa-calendar"></i>
                      <p>
                        Agenda
                      </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {!! classActivePath(1,['cadastros','prontuario']) !!}">
                  <a href="#" class="nav-link {!! classActivePathActive(1, ['cadastros','prontuario']) !!}">
                    <i class="nav-icon fas fa-archive"></i>
                    <p>
                      Cadastros
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    @can('cadastros.clientes')
                      <li class="nav-item">
                        <a href="{!! route('cadastros.clientes') !!}" class="nav-link {!! classActiveSegment(2, 'cadastros.clientes') !!}{!! classActiveSegment(1, 'prontuario.prontuario') !!}">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Clientes</p>
                        </a>
                      </li>
                    @endcan
                  </ul>
                </li>
                <li class="nav-item has-treeview {!! classActivePath(1,['system','admin','cliente']) !!}">
                    <a href="#" class="nav-link {!! classActivePathActive(1, ['system','admin','cliente']) !!}">
                      <i class="nav-icon fas fa-gear"></i>
                      <p>
                        Configuração
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      @can('system.programas')
                        <li class="nav-item">
                          <a href="{!! route('system.programas') !!}" class="nav-link {!! classActiveSegment(2, 'system.programas') !!}">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Programas</p>
                          </a>
                        </li>
                      @endcan
                      @can('system.grupos')
                        <li class="nav-item">
                          <a href="{!! route('system.grupos') !!}" class="nav-link {!! classActiveSegment(2, 'system.grupos') !!}">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Grupos</p>
                          </a>
                        </li>
                      @endcan
                      @can('system.empresas')
                        <li class="nav-item">
                            <a href="{!! route('system.empresas') !!}" class="nav-link {!! classActiveSegment(2, 'system.empresas') !!}">
                              <i class="fa fa-circle-o nav-icon"></i>
                              <p>Empresas</p>
                            </a>
                        </li>
                      @endcan
                      @can('system.usuarios')
                        <li class="nav-item">
                          <a href="{!! route('system.usuarios') !!}" class="nav-link {!! classActiveSegment(2, 'system.usuarios') !!}">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Usuários</p>
                          </a>
                        </li>
                      @endcan
                      @can('system.preferencias')
                        <li class="nav-item">
                            <a href="{!! route('system.preferencias') !!}" class="nav-link {!! classActiveSegment(2, 'system.preferencias') !!}">
                              <i class="fa fa-circle-o nav-icon"></i>
                              <p>Preferências</p>
                            </a>
                        </li>
                      @endcan
                      @can('system.estados')
                        <li class="nav-item">
                            <a href="{!! route('system.estados') !!}" class="nav-link {!! classActiveSegment(2, 'system.estados') !!}">
                              <i class="fa fa-circle-o nav-icon"></i>
                              <p>Estados</p>
                            </a>
                        </li>
                      @endcan
                      @can('system.cidades')
                        <li class="nav-item">
                            <a href="{!! route('system.cidades') !!}" class="nav-link {!! classActiveSegment(2, 'system.cidades') !!}">
                              <i class="fa fa-circle-o nav-icon"></i>
                              <p>Cidades</p>
                            </a>
                        </li>
                      @endcan
                      @can('system.enderecos')
                        <li class="nav-item">
                            <a href="{!! route('system.enderecos') !!}" class="nav-link {!! classActiveSegment(2, 'system.enderecos') !!}">
                              <i class="fa fa-circle-o nav-icon"></i>
                              <p>Endereco</p>
                            </a>
                        </li>
                      @endcan
                    </ul>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
