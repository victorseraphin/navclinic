@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Agenda</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <div class="input-group-append">
              <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#ModalAgendamento"> Novo Agendamento</a>
            </div>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            {!! csrf_field() !!}
            <div class="row">
              <!-- /.col -->
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-body p-0">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                  </div>
                  <div id="external-events"> </div>

                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
          </div>
          <!-- /.card -->

        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="ModalAgendamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Novo agendamento</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="card-body">
              <form action="{{ route('agenda.salvar') }}" method="post" autocomplete="off">
                {!! csrf_field() !!}
                <div class="row">
                  <div class="col-md-6">
                    <label>Profissional:</label>
                    <div class="form-group">

                      <select id="profissional" name="profissional_id" class="form-control" style="width: 100%;" required>
                        <option value="" readonly="readonly">Selecione...</option>
                        @foreach($profissional as $value)
                        <?php if (isset($campos)):?>
                          <option value="{{ $value->id }}" @if($campos->profissional_id==$value->id) selected="selected" @endif> {{ $value->name }} </option>
                        <?php else:?>
                        <option @if(old('profissional_id')==$value->id) {{'selected="selected"'}} @endif value="{{ $value->id }}"> {{ $value->name }} </option>
                        <?php endif;?>
                        @endforeach
                      </select>
                      @if ($errors->has('profissional_id'))
                      <span class="help-block">
                        <strong>{{ $errors->first('profissional_id') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label>Cliente:</label> <a href="#" class="btn btn-outline-primary btn-xs" data-toggle="modal" data-target="#ModalCadastroCliente"> Novo Cliente</a>
                    <div class="form-group">
                      <select id="cliente" name="clientes_id" class="form-control" style="width: 100%;" required>
                        <option value="" readonly="readonly">Selecione...</option>
                        @foreach($cliente as $value)
                        <?php if (isset($campos)):?>
                          <option value="{{ $value->id }}" @if($campos->clientes_id==$value->id) selected="selected" @endif> {{ $value->nome }} </option>
                        <?php else:?>
                        <option @if(old('clientes_id')==$value->id) {{'selected="selected"'}} @endif value="{{ $value->id }}"> {{ $value->nome }} </option>
                        <?php endif;?>
                        @endforeach
                      </select>
                      @if ($errors->has('clientes_id'))
                      <span class="help-block">
                        <strong>{{ $errors->first('clientes_id') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label>Data:</label>
                    <div class="input-group has-feedback {{ $errors->has('data') ? 'has-error' : '' }}">
                      @if ($errors->has('data'))
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-danger" title="{{ $errors->first('data') }}"><i class="fa fa-exclamation-triangle"></i></button>
                        </div>
                      @endif
                      <input id=data type="date" name="data" class="form-control" placeholder="Quantidade" onkeypress="return(moeda(this,'.',',',event))" required
                      <?php if (old('data')):?>
                        value="{{old('data')}}"
                      <?php elseif (isset($campos)):?>
                        value="{{ $campos->data }}"
                      <?php else:
                        $data = date('Y-m-d');?>
                        value="{{ date('Y-m-d', strtotime($data. ' + 1 days')) }}"
                      <?php endif;?>
                      >
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label>Hora início:</label>
                    <div class="input-group has-feedback {{ $errors->has('hora_ini') ? 'has-error' : '' }}">
                      @if ($errors->has('hora_ini'))
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-danger" title="{{ $errors->first('hora_ini') }}"><i class="fa fa-exclamation-triangle"></i></button>
                        </div>
                      @endif
                      <input id="hora_ini" type="time" name="hora_ini" class="form-control" placeholder="Valor" onkeypress="return(moeda(this,'.',',',event))" required
                      <?php if (old('hora_ini')):?>
                        value="{{old('hora_ini')}}"
                      <?php elseif (isset($campos)):?>
                        value="{{ $campos->hora_ini }}"
                      <?php endif;?>
                      >
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label>Hora fim:</label>
                    <div class="input-group has-feedback {{ $errors->has('hora_fin') ? 'has-error' : '' }}">
                      @if ($errors->has('hora_fin'))
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-danger" title="{{ $errors->first('hora_fin') }}"><i class="fa fa-exclamation-triangle"></i></button>
                        </div>
                      @endif
                      <input id="hora_fin" type="time" name="hora_fin" class="form-control" placeholder="Valor" onkeypress="return(moeda(this,'.',',',event))" required
                      <?php if (old('hora_fin')):?>
                        value="{{old('hora_fin')}}"
                      <?php elseif (isset($campos)):?>
                        value="{{ $campos->hora_fin }}"
                      <?php endif;?>
                      >
                    </div>
                    <br />
                  </div>
                  <div class="col-md-12">
                    <label>Queixa:</label>
                    <div class="input-group has-feedback {{ $errors->has('queixa') ? 'has-error' : '' }}">
                      @if ($errors->has('queixa'))
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-danger" title="{{ $errors->first('queixa') }}"><i class="fa fa-exclamation-triangle"></i></button>
                        </div>
                      @endif
                      <textarea name="queixa" class="form-control" rows="3" placeholder="Enter ..." required
                      <?php if (old('queixa')):?>
                        {{old('queixa')}}
                      <?php elseif (isset($campos)):?>
                        {{ $campos->queixa }}
                      <?php endif;?>
                      ></textarea>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Inserir agendamento</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="modal fade" id="ModalAgenda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="nome_profissional"> </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="card-body">
              <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                  <img class="img-responsive img-circle" src="/img/user.png" alt="User" title="Logo do CSS" width="50" height="50" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                  <span id="nome" style="color:#009BC9;font-weight:bold;"></span> | <span id="email" style="color:#009BC9"></span>
                  <p><span style=" font-size:72%; color:gray">TELEFONE</span> <span id="telefone" style="color:black"></span>  |  <span style=" font-size:72%; color:gray">NASCIMENTO</span>  <span id="nascimentoo" style="color:black"></span>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                  <img class="img-responsive img-circle" src="/img/calendar.png" alt="User" title="Logo do CSS" width="50" height="50" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                  <span id="data2" style="color:#009BC9;font-weight:bold;"></span>, <span id="semana"></span>, das <span id="hora_inicio" style="color:#009BC9;font-weight:bold;"></span> as <span id="hora_final" style="color:#009BC9;font-weight:bold;"></span>
                  <p><span style=" font-size:72%; color:gray">QUEIXA</span> &nbsp; <span id="procedimento"></span>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                  <img class="img-responsive img-circle" src="img/flag.png" alt="User" title="Logo do CSS" width="50" height="50" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                  <p><p><span id="flag"> </span>
                </div>
              </div>
              <div class="modal-footer">
                <a id="confirmado" href="#" class="btn btn-success">Confirmado</a>
                <a id="cancelado" href="#" class="btn btn-danger">Cancelado</a>
                <a id="aguardando" href="#" class="btn btn-warning">Aguardando</a>
                @if(auth()->user()->login == 'P')
                  <a id="atender" href="#" class="btn" style="color:white;background-color:#ff3399;">Atender</a>
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="modal fade" id="ModalCadastroCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Novo Cliente</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="card-body">
              <form action="{{ route('agenda.novo_cliente') }}" method="post" autocomplete="off">
                {!! csrf_field() !!}
                <div class="row">

                  <div class="col-md-6">
                      <label>Nome:</label>
                      <div class="input-group has-feedback {{ $errors->has('nome') ? 'has-error' : '' }}">
                        @if ($errors->has('nome'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('nome') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="nome" class="form-control" placeholder="Nome" required
                        <?php if (old('nome')):?>
                          value="{{old('nome')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->nome }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label>CPF:</label>
                      <div class="input-group has-feedback {{ $errors->has('cpf') ? 'has-error' : '' }}">
                        @if ($errors->has('cpf'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('cpf') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="cpf" class="form-control" placeholder="CPF"
                        <?php if (old('cpf')):?>
                          value="{{old('cpf')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->cpf }}"
                        <?php endif;?>
                        @if (isset($campos)) readonly @endif
                        >
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label>Fone:</label>
                      <div class="input-group has-feedback {{ $errors->has('fone') ? 'has-error' : '' }}">
                        @if ($errors->has('fone'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('fone') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="fone" class="form-control" placeholder="Fone"
                        <?php if (old('fone')):?>
                          value="{{old('fone')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->fone }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>


                  <div class="col-md-3">
                    <label>Sexo:</label>
                    <div class="form-group has-feedback {{ $errors->has('sexo') ? 'has-error' : '' }}">
                      <select name="sexo" class="form-control" required>
                        <option value="" readonly="readonly">Selecione...</option>
                          <?php if (isset($campos)):?>
                            <option value="F" @if($campos->sexo=='F') selected="selected" @endif> Feminino </option>
                            <option value="M" @if($campos->sexo=='M') selected="selected" @endif> Masculino </option>
                          <?php else:?>
                            <option @if(old('sexo')=="F") {{'selected="selected"'}} @endif value="F"> Feminino </option>
                            <option @if(old('sexo')=="M") {{'selected="selected"'}} @endif value="M"> Masculino </option>
                          <?php endif;?>
                      </select>
                      @if ($errors->has('sexo'))
                      <span class="help-block">
                        <strong>{{ $errors->first('sexo') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group has-feedback {{ $errors->has('nascimento') ? 'has-error' : '' }}">
                      <label>Aniversário:</label>
                      <div class="input-group date">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input id="nascimento" type="text" class="form-control pull-right" name="nascimento"
                        <?php if (isset($campos) and !empty($campos->nascimento)):?>
                          value="{{ date('d/m/Y',strtotime($campos->nascimento)) }}"
                        <?php else:?>
                          value="@if(old('nascimento')) {{old('nascimento')}} @endif"
                        <?php endif;?>
                        >
                        @if ($errors->has('nascimento'))
                          <span class="help-block">
                            <strong>{{ $errors->first('nascimento') }}</strong>
                          </span>
                        @endif
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>
                  <div class="col-md-5">
                      <label>E-mail:</label>
                      <div class="input-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        @if ($errors->has('email'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('email') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="email" class="form-control" placeholder="Email"
                        <?php if (old('email')):?>
                          value="{{old('email')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->email }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label>CEP:</label>
                      <div class="input-group has-feedback {{ $errors->has('loc_ceps_id') ? 'has-error' : '' }}">
                        @if ($errors->has('loc_ceps_id'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('loc_ceps_id') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input id="loc_ceps_id" type="text" name="loc_ceps_id" class="form-control" placeholder="CEP" onblur="pesquisacep(this.value);" data-inputmask='"mask": "99.999-999"' data-mask
                          <?php if (old('loc_ceps_id')):?>
                            value="{{old('loc_ceps_id')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->loc_ceps_id }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-7">
                      <label>Endereço:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_descricao') ? 'has-error' : '' }}">
                        @if ($errors->has('end_descricao'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_descricao') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="hidden" id = "rua_nome" name="rua_nome" value="{{old('rua_nome')}}">
                        <input type="hidden" id = "cidade_nome" name="cidade_nome" value="{{old('cidade_nome')}}">
                        <input type="hidden" id = "bairro_nome" name="bairro_nome" value="{{old('bairro_nome')}}">
                        <input type="hidden" id = "end_uf" name="end_uf" value="{{old('end_uf')}}">
                        <input type="hidden" id = "cod_ibge" name="cod_ibge" value="{{old('cod_ibge')}}">
                        <input type="hidden" id = "unidade" name="unidade" value="{{old('unidade')}}">
                        <input type="hidden" id = "gia" name="gia" value="{{old('gia')}}">
                        <input id = "end_descricao" type="text" name="end_descricao" class="form-control" placeholder="Endereço" readonly
                          <?php if (old('end_descricao')):?>
                            value="{{old('end_descricao')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_descricao }}"
                          <?php endif;?>
                      >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Número:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_numero') ? 'has-error' : '' }}">
                        @if ($errors->has('end_numero'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_numero') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="end_numero" class="form-control" placeholder="Número"
                          <?php if (old('end_numero')):?>
                            value="{{old('end_numero')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_numero }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>


                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Salvar novo cliente</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection

@section('javascript')
  <!-- jQuery -->
  <script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.js') }}"></script>
  <script src="{{ asset('dist/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('dist/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/fullcalendar/fullcalendar/main.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/fullcalendar/fullcalendar-daygrid/main.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/fullcalendar/fullcalendar-timegrid/main.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/fullcalendar/fullcalendar-interaction/main.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/fullcalendar/fullcalendar-bootstrap/main.min.js') }}"></script>
  <script src=”/dist/plugins/fullcalendar/locale/pt-br.js”></script>
  <script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    ini_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
      itemSelector: '.external-event',
      eventData: function(eventEl) {
        console.log(eventEl);
        return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
        };
      }
    });

    var calendar = new Calendar(calendarEl, {
      defaultView:'timeGridWeek',
      weekends: false,
      minTime: "08:00:00",
      maxTime: "20:00:00",
      locale: 'pt-br',
      allDaySlot: false,
			selectHelper: true,
      displayEventEnd: false,
      buttonText: {
        today:    'Hoje',
        month:    'Mês',
        week:     'Semana',
        day:      'Dia'
      },
      plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      //Random default events
      events    : [
        <?php foreach($agenda as $value):
          $hora_ini = str_replace(":", ",", $value->hora_ini);
          $hora_fin = str_replace(":", ",", $value->hora_fin);
          if($value->status == 0){
            $color = '#0099ff';
          }
          elseif($value->status == 1){
            $color = '#00cc00';
          }
          elseif($value->status == 2){
            $color = '#ff6666';
          }
          elseif($value->status == 3){
            $color = '#ffff66';
          }
          elseif($value->status == 4){
            $color = '#ff3399';
          }
          elseif($value->status == 5){
            $color = '#A4A4A4';
          }
          else{
            $color = '#0099ff';
          }
          ?>
          {
            id             : {{$value->id}},
            title          : '{{$value->nome}}',
            start          : '{{ $value->data.' '.$value->hora_ini }}',
            end            : '{{ $value->data.' '.$value->hora_fin }}',
            backgroundColor: '{{$color}}', //red
            borderColor    : '{{$color}}', //red
          },
        <?php endforeach ?>

      ],

      editable  : false,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      eventLimit: true,

      drop      : function(info) {
        // is the "remove after drop" checkbox checked?
        if (checkbox.checked) {
          // if so, remove the element from the "Draggable Events" list
          info.draggedEl.parentNode.removeChild(info.draggedEl);
        }
      },
      dateClick: function(info) {
        //alert('Clicked on: ' + info.dateStr);
        variavel = info.dateStr.split("T");
        variavel2 = variavel[1].split("-");
        variavel3 = variavel2[0].split(":");

        converte_hora = parseInt(variavel3[0] * 60);
        converte_min = parseInt(variavel3[1]) + 30;

        total_min = (converte_hora + converte_min)/60;

        var_hora = total_min.toString().split(".");
        hora = var_hora[0];
        if(var_hora[1]){
          min = (var_hora[1]*60)/10;
        }
        else{
          min="00";
        }

        if(hora.toString().length < 2){
          hora = "0"+hora;
        }
        hora_final = hora +':'+ min;

        document.getElementById('data').value = variavel[0];
        document.getElementById('hora_ini').value = variavel3[0]+':'+variavel3[1];
        document.getElementById('hora_fin').value = hora_final;

        $("#ModalAgendamento").modal("show");



      },
      eventClick:  function(event) {
        var id = event.event.id;
        $.get('/agenda/getby/'+id, function( data ) {
          $.each( data, function( index, value ) {
            $('#nome_profissional').html(value.nome_profissional);
            $('#nome').html(value.nome);
            $('#email').html(value.email);
            $('#nascimentoo').html(value.nascimento);
            $('#telefone').html(value.tel);
            $('#data2').html(value.data);
            $('#semana').html(value.dia_semana);
            $('#hora_inicio').html(value.hora_inicio);
            $('#hora_final').html(value.hora_final);
            $('#procedimento').html(value.procedimento);
            $('#flag').html(value.flag);

            const flag = document.getElementById("flag");
            flag.setAttribute("style", "color:"+value.cor_flag);

            const button1 = document.getElementById("confirmado");
            if(value.status != 1 && value.status != 3 && value.status != 4 && value.status != 5){
              button1.setAttribute("class", "btn btn-success");
              button1.setAttribute("href", "agenda/confirmado/"+id);
            }else{
              button1.setAttribute("class", "btn btn-success disabled");
            }

            const button2 = document.getElementById("cancelado");
            if(value.status != 2 && value.status != 3 && value.status != 4 && value.status != 5){
              button2.setAttribute("class", "btn btn-danger");
              button2.setAttribute("href", "agenda/cancelado/"+id);
            }else{
              button2.setAttribute("class", "btn btn-danger disabled");
            }

            const button3 = document.getElementById("aguardando");
            if(value.status != 2 && value.status != 3 && value.status != 4 && value.status != 5){
              button3.setAttribute("class", "btn btn-warning");
              button3.setAttribute("href", "agenda/aguardando/"+id);
            }else{
              button3.setAttribute("class", "btn btn-warning disabled");
            }

            <?php if(auth()->user()->login == 'P'):?>
              const button4 = document.getElementById("atender");
              if(value.status != 5){
                button4.setAttribute("class", "btn btn-warning");
                button4.setAttribute("href", "agenda/atender/"+id);
              }else{
                button4.setAttribute("class", "btn btn-warning disabled");
              }

            <?php endif ?>
          });
        });


        $("#ModalAgenda").modal("show");
      },

    });

    calendar.render();
    // $('#calendar').fullCalendar()

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      ini_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })
</script>
@stop
