@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">Atender</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form action="{{ route('agenda.atender',$campos->id) }}" method="post" autocomplete="off">

                {!! csrf_field() !!}
                <div class="row">
                  <div class="col-md-12">
                      <label>Queixa/Motivo:</label>
                      <div class="form-group has-feedback {{ $errors->has('queixa') ? 'has-error' : '' }}">
                        <textarea name="queixa" class="form-control" rows="3" placeholder="Enter ..."><?php if (old('queixa')):?>{{old('queixa')}}<?php elseif (isset($campos)):?>{{ $campos->queixa }}<?php endif;?></textarea>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <label>Histórico:</label>
                      <div class="form-group has-feedback {{ $errors->has('historico') ? 'has-error' : '' }}">
                        <textarea name="historico" class="form-control" rows="3" placeholder="Enter ..."><?php if (old('historico')):?>{{old('historico')}}<?php elseif (isset($campos)):?>{{ $campos->historico }}<?php endif;?></textarea>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <label>Observação:</label>
                      <div class="form-group has-feedback {{ $errors->has('observacao') ? 'has-error' : '' }}">
                        <textarea name="observacao" class="form-control" rows="3" placeholder="Enter ..."><?php if (old('observacao')):?>{{old('observacao')}}<?php elseif (isset($campos)):?>{{ $campos->observacao }}<?php endif;?></textarea>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <label>Palpação:</label>
                      <div class="form-group has-feedback {{ $errors->has('palpacao') ? 'has-error' : '' }}">
                        <textarea name="palpacao" class="form-control" rows="3" placeholder="Enter ..."><?php if (old('palpacao')):?>{{old('palpacao')}}<?php elseif (isset($campos)):?>{{ $campos->palpacao }}<?php endif;?></textarea>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <label>Evolução:</label>
                      <div class="form-group has-feedback {{ $errors->has('evolucao') ? 'has-error' : '' }}">
                        <textarea name="evolucao" class="form-control" rows="3" placeholder="Enter ..."><?php if (old('evolucao')):?>{{old('evolucao')}}<?php elseif (isset($campos)):?>{{ $campos->evolucao }}<?php endif;?></textarea>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label>Valor:</label>
                      <div class="form-group has-feedback {{ $errors->has('valor') ? 'has-error' : '' }}">
                        <input id="valor" type="text" name="valor" class="form-control" placeholder="Valor consulta" onkeypress='return SomenteNumero(event)'
                        <?php if (old('valor')):?>
                          value="{{old('valor')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->valor }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>
                </div>
                <div class="btn-group">
                  <div class="col-xs-12 col-lg-12">
                    <br />
                    <a href="{{ route('cadastros.clientes.listar') }}">
                      <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                    </a>
                  </div>
                  <div class="col-xs-12 col-lg-12">
                    <br />
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                  </div>
                </div>
              </form>
            <!-- /.table-responsive -->
          </div>


      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script src="{{ asset('dist/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>

  $(function() {
     $( "#nascimento" ).datepicker({
       format: 'dd/mm/yyyy',
       autoclose: true,
     });
   });

</script>

@stop
