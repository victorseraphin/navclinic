@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Meus Dados</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">Meus Dados</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <form id="frm-usuario"  action="{{ route('meus_dados.atualizar',$campos->id) }}" method="post" autocomplete="on">

                {!! csrf_field() !!}
                  <div class="row">
                    <div class="col-md-3">
                      <div class="col-md-6">
                        <label>ID:</label>
                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}">
                          <input type="text" name="id" class="form-control" placeholder="ID" readonly
                          <?php if (old('id')):?>
                            value="{{old('id')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->id }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="col-md-12">
                        <label>CPF/CNPJ:</label>
                        <div class="input-group has-feedback {{ $errors->has('doc') ? 'has-error' : '' }}">
                          @if ($errors->has('doc'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('doc') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="doc" class="form-control" placeholder="CPF/CNPJ"
                          <?php if (old('doc')):?>
                            value="{{old('doc')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->doc }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-10">
                        <label>Nome:</label>
                        <div class="input-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                          @if ($errors->has('name'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('name') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="name" class="form-control" placeholder="Nome"
                          <?php if (old('name')):?>
                            value="{{old('name')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->name }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="col-md-12">
                        <label>Fone:</label>
                        <div class="input-group has-feedback {{ $errors->has('fone') ? 'has-error' : '' }}">
                          @if ($errors->has('fone'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('fone') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="fone" class="form-control" placeholder="(99)99999-9999"
                          <?php if (old('fone')):?>
                            value="{{old('fone')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->fone }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="col-md-10">
                        <label>Login:</label>
                        <div class="form-group has-feedback {{ $errors->has('login') ? 'has-error' : '' }}">
                          <select id="login" name="login" class="form-control" disabled>
                            <option value="" readonly="readonly">Selecione...</option>
                              <?php if (isset($campos)):?>
                                <option value="S" @if($campos->login=='S') selected="selected" @endif> Secretaria(o) </option>
                                <option value="P" @if($campos->login=='P') selected="selected" @endif> Profissional </option>
                              <?php else:?>
                                <option @if(old('login')=="S") {{'selected="selected"'}} @endif value="S"> Secretaria(o) </option>
                                <option @if(old('login')=="P") {{'selected="selected"'}} @endif value="P"> Profissional </option>
                              <?php endif;?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-5">
                      <div class="col-md-10">
                        <label>Email:</label>
                        <div class="input-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                          @if ($errors->has('email'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('email') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="email" class="form-control" placeholder="Email" readonly
                          <?php if (old('email')):?>
                            value="{{old('email')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->email }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="col-md-7">
                        <label>Senha:</label>
                        <div class="input-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                          @if ($errors->has('password'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('password') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="password" name="password" class="form-control" placeholder="Senha" autocomplete="off"
                          <?php if (old('password')):?>
                            value="{{old('password')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->password }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="col-md-7">
                        <label>Confirma senha:</label>
                        <div class="input-group has-feedback {{ $errors->has('confirma_senha') ? 'has-error' : '' }}">
                          @if ($errors->has('confirma_senha'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('confirma_senha') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="password" name="confirma_senha" class="form-control" placeholder="Confirmação da senha" autocomplete="off"
                          <?php if (old('confirma_senha')):?>
                            value="{{old('confirma_senha')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->password }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </br />
                    </div>

                    </div>


                    <div class="btn-group">

                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                      </div>
                    </div>

                  </div>
              </form>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection

  @section('javascript')
  <!-- jQuery -->
  <script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
  <script src="{{ asset('dist/plugins/select2/select2.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.js') }}"></script>

  @stop
