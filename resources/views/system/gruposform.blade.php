@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(3))." ".ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php if (isset($campos)):?>
              <form id="frm-grupo"  action="{{ route('system.grupos.atualizar',$campos->id) }}" method="post" autocomplete="off">
            <?php else: ?>
              <form id="frm-grupo"  action="{{ route('system.grupos.salvar') }}" method="post" autocomplete="off">
            <?php endif;?>
                {!! csrf_field() !!}
                  <div class="row">
                    <div class="col-md-5">
                      <label>Nome:</label>
                      <div class="input-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                        @if ($errors->has('name'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('name') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="name" class="form-control" placeholder="Nome"
                        <?php if (old('name')):?>
                          value="{{old('name')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->name }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>

                <div class="col-md-12">
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table id="tabela2" class="table">
                        <thead>
                          <tr>
                            <th style="width: 10px"><input name="select_all" value="1" type="checkbox"></th>
                            <th>Controller</th>
                            <th>Nome</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="btn-group"  aria-label="Exemplo básico">
                          <div class="col-xs-12 col-lg-12">
                            <br />
                            <a href="{{ route('system.grupos.listar') }}">
                              <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                            </a>
                          </div>

                          <div class="col-xs-12 col-lg-12">
                            <br />
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                            <br />
                          </div>
                        </div>
                      </div>

              </div>
              </div>
              </form>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables/dataTables.checkboxes.min.js"></script>
<script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="/dist/plugins/select2/select2.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<script>

function updateDataTableSelectAllCtrl(table){
 var $table             = table.table().node();
 var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
 var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
 var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

 // If none of the checkboxes are checked
 if($chkbox_checked.length === 0){
    chkbox_select_all.checked = false;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If all of the checkboxes are checked
 } else if ($chkbox_checked.length === $chkbox_all.length){
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If some of the checkboxes are checked
 } else {
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = true;
    }
 }
}
$(document).ready(function (){
  var id = {{ (!empty($campos->id) ? $campos->id : 0)}};
  var rows_selected = [];
  var rows_selected_bd = [];
  var table = $('#tabela2').DataTable({
      'ajax': '/system/grupos/checkeds/'+ id ,
      'columnDefs': [
         {
            'targets': 0,

            render: function (data, type, full, meta){
            console.log(full.programa);
            if(full.programa != null){
              if(rows_selected.indexOf(full.programa) == -1){
                rows_selected.push(full.programa );
              }
            }
            return '<input type="checkbox" class="programas">';
            },

            className: 'programas'
         }
      ],
      'select': {
        style:    'multi',
        selector: 'td:first-child'
      },

      'columns': [
        { data: 'id' /*,
          render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="programas">';
                    }
                    return data;
          }*/

        },

        { data: 'controller' },
        { data: 'name' },

      ],
      rowCallback: function ( row, data, dataIndex ) {


        // Get row ID
         //var rowId = data.id;

         var rowId = data.id;

         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
      },



   });


   /// Handle click on checkbox
   $('#tabela2 tbody').on('click', 'input[type="checkbox"]', function(e){
      var $row = $(this).closest('tr');

      // Get row data
      var data = table.row($row).data();

      // Get row ID
      var rowId = data.id;

      // Determine whether row ID is in the list of selected row IDs
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
         rows_selected_bd.splice(index, 1);
      }

      if(this.checked){
         $row.addClass('selected');
      } else {
         $row.removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#tabela2').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
   });

   // Handle click on "Select all" control
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#tabela2 tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#tabela2 tbody input[type="checkbox"]:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });

   // Handle form submission event
   $('#frm-grupo').on('submit', function(e){
      var form = this;

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'programas[]')
                .val(rowId)
         );
      });
   });

});

</script>
@stop
