@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(3))." ".ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php if (isset($campos)):?>
              <form action="{{ route('system.enderecos.atualizar',$campos->id) }}" method="post" autocomplete="off">
            <?php else: ?>
              <form action="{{ route('system.enderecos.salvar') }}" method="post" autocomplete="off">
            <?php endif;?>
                {!! csrf_field() !!}
                  <div class="col-md-12">
                    <div class="col-md-5">
                      <label>CEP:</label>
                      <div class="input-group has-feedback {{ $errors->has('loc_ceps_id') ? 'has-error' : '' }}">
                        @if ($errors->has('loc_ceps_id'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('loc_ceps_id') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input id="loc_ceps_id" type="text" name="loc_ceps_id" class="form-control" placeholder="CEP" onblur="pesquisacep(this.value);" data-inputmask='"mask": "99.999-999"' data-mask
                          <?php if (old('loc_ceps_id')):?>
                            value="{{old('loc_ceps_id')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->loc_ceps_id }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Endereço:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_descricao') ? 'has-error' : '' }}">
                        @if ($errors->has('end_descricao'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_descricao') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="hidden" id = "rua_nome" name="rua_nome" value="{{old('rua_nome')}}">
                        <input type="hidden" id = "cidade_nome" name="cidade_nome" value="{{old('cidade_nome')}}">
                        <input type="hidden" id = "bairro_nome" name="bairro_nome" value="{{old('bairro_nome')}}">
                        <input type="hidden" id = "end_uf" name="end_uf" value="{{old('end_uf')}}">
                        <input type="hidden" id = "cod_ibge" name="cod_ibge" value="{{old('cod_ibge')}}">
                        <input type="hidden" id = "unidade" name="unidade" value="{{old('unidade')}}">
                        <input type="hidden" id = "gia" name="gia" value="{{old('gia')}}">
                        <input id = "end_descricao" type="text" name="end_descricao" class="form-control" placeholder="Endereço" readonly
                          <?php if (old('end_descricao')):?>
                            value="{{old('end_descricao')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_descricao }}"
                          <?php endif;?>
                      >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Número:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_numero') ? 'has-error' : '' }}">
                        @if ($errors->has('end_numero'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_numero') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="end_numero" class="form-control" placeholder="Número"
                          <?php if (old('end_numero')):?>
                            value="{{old('end_numero')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_numero }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="btn-group"  aria-label="Exemplo básico">
                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <a href="{{ route('system.enderecos.listar') }}">
                          <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                        </a>
                      </div>

                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                      </div>
                    </div>

                  </div>
              </form>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>

@stop
