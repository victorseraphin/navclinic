@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">Pesquisar</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form action="{{ route('system.usuarios.procurar') }}" method="GET" autocomplete="off">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">ID</span>
                </div>
                <div class="col-2">
                  <input name="id" type="text" class="form-control" onkeypress='return SomenteNumero(event)'
                    <?php if (!empty(session('search')['id'])):?>
                      value="{{ session('search')['id'] }}"
                    <?php endif;?>
                  >
                </div>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">Nome</span>
                </div>
                <div class="col-5">
                  <input name="name" type="text" class="form-control"
                    <?php if (!empty(session('search')['name'])):?>
                      value="{{ session('search')['name'] }}"
                    <?php endif;?>
                  >
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="btn-group"  aria-label="Exemplo básico">
                  <button type="submit" class="btn btn-block btn-primary btn-sm"><i class="fa fa-search"></i> Procurar</button>
                  <div class="col-xs-7 col-lg-7">
                      <a href="{{ route('system.usuarios.cadastrar') }}">
                          <button type="button" class="btn btn-block btn-outline-success btn-sm"><i class="fa fa-plus"></i> Novo</button>
                      </a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.card -->
        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabela" class="table">
                <thead>
                <tr>
                  <th width="100px"><center>Ações</center></th>
                  <th width="100px">ID</th>
                  <th>Nome</th>
                  <th width="50px">Ativo</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($tabela as $linha)
                    <tr>
                      <td>
                        <center>
                          <a href="#" onclick="excluir({{$linha->id}})" title="Excluir">
                            <span class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa-fw"></i></span>
                          </a>
                          <a href="{{ route('system.usuarios.editar',$linha->id) }}" title="Editar">
                            <span class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i></span>
                          </a>
                          @if($linha->active == "Y")
                            <a href="#" onclick="desativar({{$linha->id}})" title="Ativar/Desativar">
                          @else
                            <a href="#" onclick="ativar({{$linha->id}})" title="Ativar/Desativar">
                          @endif
                            <span class="btn btn-secondary btn-sm"><i class="fa fa-power-off fa-fw"></i></span>
                          </a>
                        </center>
                      </td>
                      <td>{{ $linha->id }}</td>
                      <td>{{ $linha->name }}</td>
                      @if($linha->active == 'Y')
                        <td><span class="badge bg-success">Sim</span></td>
                      @else
                        <td><span class="badge bg-danger">Não</span></td>
                      @endif
                    </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->
          </div>
          <!-- /.col -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<script>
  $(function () {
    $('#tabela').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "aaSorting": [[1, "asc"]],
      "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
          "sNext": "Próximo",
          "sPrevious": "Anterior",
          "sFirst": "Primeiro",
          "sLast": "Último"
        },
        "oAria": {
          "sSortAscending": ": Ordenar colunas de forma ascendente",
          "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      }
    })
  });
  function excluir(id){
        swal({
            title: "Você tem certeza?",
            text: "Deseja excluir este registro?",
            icon: "warning",
            buttons:{
              confirm: {
                text : 'Sim!',
                className : 'btn btn-success'
              },
              cancel: {
                text : 'Não!',
                visible: true,
                className: 'btn btn-danger'
              }
            },
        })
        .then(function(willDelete) {
            if (willDelete) {
                window.location = "/system/usuarios/deletar/" + id

            } else {
                swal("Operação cancelada!");
            }
        });
    }
    function desativar(id){
          swal({
              title: "Você tem certeza?",
              text: "Deseja desativar este registro?",
              icon: "warning",
              buttons:{
                confirm: {
                  text : 'Sim!',
                  className : 'btn btn-success'
                },
                cancel: {
                  text : 'Não!',
                  visible: true,
                  className: 'btn btn-danger'
                }
              },
          })
          .then(function(willDelete) {
              if (willDelete) {
                  window.location = "/system/usuarios/ativar/" + id

              } else {
                  swal("Operação cancelada!");
              }
          });
      }
      function ativar(id){
            swal({
                title: "Você tem certeza?",
                text: "Deseja ativar este registro?",
                icon: "warning",
                buttons:{
                  confirm: {
                    text : 'Sim!',
                    className : 'btn btn-success'
                  },
                  cancel: {
                    text : 'Não!',
                    visible: true,
                    className: 'btn btn-danger'
                  }
                },
            })
            .then(function(willDelete) {
                if (willDelete) {
                    window.location = "/system/usuarios/ativar/" + id

                } else {
                    swal("Operação cancelada!");
                }
            });
        }
</script>
@stop
