@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(3))." ".ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php if (isset($campos)):?>
              <form id="frm-usuario"  action="{{ route('system.usuarios.atualizar',$campos->id) }}" method="post" autocomplete="off">
            <?php else: ?>
              <form id="frm-usuario"  action="{{ route('system.usuarios.salvar') }}" class="form-line" method="post" autocomplete="off">
            <?php endif;?>
                {!! csrf_field() !!}
                  <div class="row">
                    <div class="col-md-6">
                      <div class="col-md-3">
                        <label>ID:</label>
                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}">
                          <input type="text" name="id" class="form-control" placeholder="ID" readonly
                          <?php if (old('id')):?>
                            value="{{old('id')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->id }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-8">
                        <label>Nome:</label>
                        <div class="input-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                          @if ($errors->has('name'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('name') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="name" class="form-control" placeholder="Nome"
                          <?php if (old('name')):?>
                            value="{{old('name')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->name }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-8">
                        <label>Login:</label>
                        <div class="form-group has-feedback {{ $errors->has('login') ? 'has-error' : '' }}">
                          <select id="login" name="login" class="form-control" onchange="exibir_ocultar(this)">
                            <option value="" readonly="readonly">Selecione...</option>
                              <?php if (isset($campos)):?>
                                <option value="S" @if($campos->login=='S') selected="selected" @endif> Secretaria(o) </option>
                                <option value="P" @if($campos->login=='P') selected="selected" @endif> Profissional </option>
                              <?php else:?>
                                <option @if(old('login')=="S") {{'selected="selected"'}} @endif value="S"> Secretaria(o) </option>
                                <option @if(old('login')=="P") {{'selected="selected"'}} @endif value="P"> Profissional </option>
                              <?php endif;?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="col-md-10">
                        <label>Email:</label>
                        <div class="input-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                          @if ($errors->has('email'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('email') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="email" class="form-control" placeholder="Email"
                          <?php if (old('email')):?>
                            value="{{old('email')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->email }}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>



                    <div class="col-md-4">
                      <div class="col-md-7">
                        <label>Senha:</label>
                        <div class="input-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                          @if ($errors->has('password'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('password') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="password" name="password" class="form-control" placeholder="Senha"
                          <?php if (old('password')):?>
                            value="{{old('password')}}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="col-md-7">
                        <label>Confirma senha:</label>
                        <div class="input-group has-feedback {{ $errors->has('confirma_senha') ? 'has-error' : '' }}">
                          @if ($errors->has('confirma_senha'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('confirma_senha') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="password" name="confirma_senha" class="form-control" placeholder="Confirmação da senha"
                          <?php if (old('confirma_senha')):?>
                            value="{{old('confirma_senha')}}"
                          <?php endif;?>
                          >
                        </div>
                      </div>
                    </br />
                    </div>
                    <div class="col-md-3">
                      <label>Empresa:</label>
                      <div class="form-group">
                        <select name="system_unit_id" class="form-control select2" style="width: 100%;">
                          <option value="" readonly="readonly">Selecione...</option>
                          @foreach($tabela_empresas as $value)
                          <?php if (isset($campos)):?>
                            <option value="{{ $value->id }}" @if($campos->system_unit_id==$value->id) selected="selected" @endif> {{ $value->name }} </option>
                          <?php else:?>
                          <option @if(old('system_unit_id')==$value->id) {{'selected="selected"'}} @endif value="{{ $value->id }}"> {{ $value->name }} </option>
                          <?php endif;?>
                          @endforeach
                        </select>
                        @if ($errors->has('system_unit_id'))
                        <span class="help-block">
                          <strong>{{ $errors->first('system_unit_id') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-8">
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Programas</h3>
                        <!-- /.card-header -->
                          <div class="table-responsive">
                            <table id="tabela" class="table">
                              <thead>
                              <tr>
                              <th style="width: 10px"><input name="select_all" value="1" type="checkbox"></th>
                              <th>Controller</th>
                              <th>Nome</th>
                            </tr>
                            <tbody>

                            </tbody>

                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Grupos</h3>



                          <div class="table-responsive">
                            <table id="tabela2" class="table">
                            <tr>
                              <th style="width: 10px">#</th>
                              <th>Nome</th>
                            </tr>
                            <tbody>
                              @foreach ($tabela_grupos as $linha)
                                <tr>
                                  <td>
                                    <center>
                                      <input name="grupos[]" type="checkbox" value="{{$linha->id}}"
                                        <?php
                                          if(isset($group_user) or !empty($group_user)){
                                            if(in_array($linha->id, $group_user)){
                                              echo "checked";
                                            }
                                          }elseif(old('grupos') != null){
                                            if(in_array($linha->id, old('grupos'))){
                                              echo "checked";
                                            }
                                          }
                                        ?>
                                        >
                                      </center>
                                    </td>
                                  <td>{{ $linha->name }}</td>
                                </tr>
                              @endforeach

                              </tbody>

                            </table>
                          </div>
                        </div>
                        <!-- /.card-body -->
                      </div>
                    </div>
                    </div>


                    <div class="btn-group">
                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <a href="{{ route('system.usuarios.listar') }}">
                          <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                        </a>
                      </div>

                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                      </div>
                    </div>

                  </div>
              </form>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection

  @section('javascript')
  <!-- jQuery -->
  <script src="/dist/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
  <!--<script src="/dist/plugins/datatables/dataTables.checkboxes.min.js"></script> -->
  <script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
  <script src="/dist/plugins/select2/select2.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/dist/js/adminlte.js"></script>
  <script>

  function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}
  $(document).ready(function (){
    var id = {{ (!empty($campos->id) ? $campos->id : 0)}};
    var rows_selected = [];
    var rows_selected_bd = [];
     var table = $('#tabela').DataTable({
        'ajax': '/system/usuarios/checkeds/'+ id,
        'columnDefs': [
           {
              'targets': 0,

              render: function (data, type, full, meta){
              //console.log(full.programa);
              if(full.programa != null){
                //console.log(rows_selected.indexOf(full.programa));
                //if(rows_selected.indexOf(full.programa)){
                if(rows_selected.indexOf(full.programa) == -1){
                  rows_selected.push(full.programa );
                }
              }
              return '<input type="checkbox" class="programas">';
              },

              className: 'programas'
           }
        ],
        'select': {
          style:    'multi',
          selector: 'td:first-child'
        },

        'columns': [
          { data: 'id' /*,
            render: function ( data, type, row ) {
                      if ( type === 'display' ) {
                          return '<input type="checkbox" class="programas">';
                      }
                      return data;
            }*/

          },

          { data: 'controller' },
          { data: 'name' },

        ],
        rowCallback: function ( row, data, dataIndex ) {


          // Get row ID
           //var rowId = data.id;

           var rowId = data.id;

           // If row ID is in the list of selected row IDs
           if($.inArray(rowId, rows_selected) !== -1){
              $(row).find('input[type="checkbox"]').prop('checked', true);
              $(row).addClass('selected');
           }
        },



     });


     /// Handle click on checkbox
     $('#tabela tbody').on('click', 'input[type="checkbox"]', function(e){
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = data.id;

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if(this.checked && index === -1){
           rows_selected.push(rowId);

        // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1){
           rows_selected.splice(index, 1);
           rows_selected_bd.splice(index, 1);
        }

        if(this.checked){
           $row.addClass('selected');
        } else {
           $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
     });

     // Handle click on table cells with checkboxes
     $('#tabela').on('click', 'tbody td, thead th:first-child', function(e){
        $(this).parent().find('input[type="checkbox"]').trigger('click');
     });

     // Handle click on "Select all" control
     $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
        if(this.checked){
           $('#tabela tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
           $('#tabela tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
     });

     // Handle table draw event
     table.on('draw', function(){
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
     });

     // Handle form submission event
     $('#frm-usuario').on('submit', function(e){
        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function(index, rowId){
           // Create a hidden element
           $(form).append(
               $('<input>')
                  .attr('type', 'hidden')
                  .attr('name', 'programas[]')
                  .val(rowId)
           );
        });
     });

  });


  </script>
  @stop
