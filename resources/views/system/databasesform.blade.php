@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(3))." ".ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php if (isset($campos)):?>
              <form action="{{ route('system.databases.atualizar',$campos->id) }}" method="post" autocomplete="off">
            <?php else: ?>
              <form action="{{ route('system.databases.salvar') }}" method="post" autocomplete="off">
            <?php endif;?>
                {!! csrf_field() !!}
                  <div class="col-md-12">
                    <div class="col-md-5">
                      <label>Nome:</label>
                      <div class="input-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                        @if ($errors->has('name'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('name') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="name" class="form-control" placeholder="Nome"
                        <?php if (old('name')):?>
                          value="{{old('name')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->name }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>CNPJ/CPF:</label>
                      <div class="input-group has-feedback {{ $errors->has('cnpj_cpf') ? 'has-error' : '' }}">
                        @if ($errors->has('cnpj_cpf'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('cnpj_cpf') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="cnpj_cpf" class="form-control" placeholder="Nome"
                        <?php if (old('cnpj_cpf')):?>
                          value="{{old('cnpj_cpf')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->cnpj_cpf }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Domínio:</label>
                      <div class="input-group has-feedback {{ $errors->has('domain') ? 'has-error' : '' }}">
                        @if ($errors->has('domain'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('domain') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="domain" class="form-control" placeholder="Nome"
                        <?php if (old('domain')):?>
                          value="{{old('domain')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->domain }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Banco de dados:</label>
                      <div class="input-group has-feedback {{ $errors->has('bd_database') ? 'has-error' : '' }}">
                        @if ($errors->has('bd_database'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('bd_database') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="bd_database" class="form-control" placeholder="Banco de dados"
                        <?php if (old('bd_database')):?>
                          value="{{old('bd_database')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->bd_database }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Hostname:</label>
                      <div class="input-group has-feedback {{ $errors->has('bd_hostname') ? 'has-error' : '' }}">
                        @if ($errors->has('bd_hostname'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('bd_hostname') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="bd_hostname" class="form-control" placeholder="Hostname"
                        <?php if (old('bd_hostname')):?>
                          value="{{old('bd_hostname')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->bd_hostname }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Usuário:</label>
                      <div class="input-group has-feedback {{ $errors->has('bd_username') ? 'has-error' : '' }}">
                        @if ($errors->has('bd_username'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('bd_username') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="bd_username" class="form-control" placeholder="Usuário"
                        <?php if (old('bd_username')):?>
                          value="{{old('bd_username')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->bd_username }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Senha do usuário:</label>
                      <div class="input-group has-feedback {{ $errors->has('bd_password') ? 'has-error' : '' }}">
                        @if ($errors->has('bd_password'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('bd_password') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="password" name="bd_password" class="form-control" placeholder="Senha"
                        <?php if (old('bd_password')):?>
                          value="{{old('bd_password')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->bd_password }}"
                        <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Criar banco de dados:</label>
                      <input type="checkbox" name="create_database"
                      <?php if (old('create_database')):?>
                        checked
                      <?php endif;?>
                      >
                    </div>
                    <div class="btn-group"  aria-label="Exemplo básico">
                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <a href="{{ route('system.databases.listar') }}">
                          <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                        </a>
                      </div>

                      <div class="col-xs-12 col-lg-12">
                        <br />
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                      </div>
                    </div>

                  </div>
              </form>
            <!-- /.table-responsive -->
          </div>

        </div>
        <!-- /.card -->


        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>

@stop
