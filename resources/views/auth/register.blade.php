@extends('layouts.master')
@section('content')

<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      Laravel Admin
    </div>

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Novo cadastro</p>

        <form action="{{ route('register.salvar') }}" method="post">
          @csrf
          <div class="input-group mb-3">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nome Completo"
              required autofocus> @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span> @endif
            <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="doc" type="text" class="form-control{{ $errors->has('doc') ? ' is-invalid' : '' }}" name="doc" value="{{ old('doc') }}" placeholder="CNPJ | CPF"
              required autofocus> @if ($errors->has('doc'))
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('doc') }}</strong>
                                    </span> @endif
            <div class="input-group-append">
              <span class="fa fa-address-card input-group-text"></span>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="fone" type="text" class="form-control{{ $errors->has('fone') ? ' is-invalid' : '' }}" name="fone" value="{{ old('fone') }}" placeholder="Telefone | Celular"
              required autofocus> @if ($errors->has('fone'))
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fone') }}</strong>
                                    </span> @endif
            <div class="input-group-append">
              <span class="fa fa-phone input-group-text"></span>
            </div>
          </div>

          <div class="input-group mb-3">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Endereço de Email">
            <div class="input-group-append">
              <span class="fa fa-envelope input-group-text"></span> @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span> @endif
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Senha" required>
            <div class="input-group-append">
              <span class="fa fa-lock input-group-text"></span>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirme a Senha" required>
            <div class="input-group-append">
              <span class="fa fa-lock input-group-text"></span> @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- OU -</p>
          <a href="{{ url('auth/facebook') }}" class="btn btn-block btn-primary">
              <i class="fab fa-facebook-f mr-2"></i>
              Inscreva-se usando o Facebook
            </a>
          <a href="{{ url('auth/google') }}" class="btn btn-block btn-danger">
              <i class="fab fa-google-plus-g mr-2"></i>
              Inscreva-se usando o Google+
            </a>
        </div>

        <a href="{{route('login')}}" class="text-center">Já tenho cadastro</a>
      </div>
      <!-- /.form-box -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.register-box -->
@endsection
