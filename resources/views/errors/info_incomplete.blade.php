@if(class_user_incomplete()== true || class_ender_incomplete()== true)
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fas fa-ban"></i>Cadastro incompleto!</h4>
    @if(class_user_incomplete()== true)
      <a href="{{ route('meus_dados',Auth::user()['id']) }}">Clique aqui e finalize seu cadastro.</a>
      <p>
    @endif

    @if(class_ender_incomplete()== true)
      <a href="{{ route('system.enderecos.cadastrar') }}">Clique aqui e cadastre seu endereço.</a>
    @endif
  </div>
@endif
