@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Prontuário</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card-header border-transparent">
          <div class="card-tools">
            <a href="{{route('cadastros.clientes')}}" class="btn btn-outline-danger"> Voltar</a>

          </div>
          <br />
          <br />
        </div>
        @if(count($tabela) > 0)
          <div class="row">
            <div class="col-md-12">
              <!-- The time line -->
              <div class="timeline">
                <!-- timeline time label -->
                @foreach($tabela as $value)
                <div class="time-label">
                  <?php
                  $dia = date('d',strtotime($value->data));
                  $ano = date('Y',strtotime($value->data));
                  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                  $mes = ucfirst(utf8_encode(strftime("%B", strtotime($value->data))));
                  if($mes == "MarÃ§o"){
                    $mes = "Março";
                  }
                  ?>
                  <span class="bg-red">{{$dia.' de '.$mes.' de '.$ano}}</span> @if($value->valor != null and $value->valor != 0)<span class="bg-green">R$ {{ number_format($value->valor, 2, ',', '.')}}</span>@endif
                </div>
                <!-- /.timeline-label -->
                  @if($value->queixa != null)
                    <!-- timeline item -->
                    <div>
                      <i class="fas bg-maroon"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> {{$value->hora_ini}}</span>
                        <h3 class="timeline-header"><a href="#" style="color:red">Queixa/Motivo</a></h3>

                        <div class="timeline-body">
                           {{$value->queixa}}
                        </div>

                      </div>
                    </div>
                    <!-- END timeline item -->
                  @endif
                  @if($value->historico != null)
                    <!-- timeline item -->
                    <div>
                      <i class="fas bg-pink"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> {{$value->hora_ini}}</span>
                        <h3 class="timeline-header no-border"><a href="#" style="color:#FF00BF">Historico</a></h3>
                        <div class="timeline-body">
                           {{$value->historico}}
                        </div>
                      </div>
                    </div>
                    <!-- END timeline item -->
                  @endif
                  @if($value->observacao != null)
                    <!-- timeline item -->
                    <div>
                      <i class="fas bg-Purple"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> {{$value->hora_ini}}</span>
                        <h3 class="timeline-header"><a href="#" style="color:#5F04B4">Observação</a></h3>
                        <div class="timeline-body">
                          {{$value->observacao}}
                        </div>

                      </div>
                    </div>
                    <!-- END timeline item -->
                  @endif
                  @if($value->palpacao != null)
                    <!-- timeline item -->
                    <div>
                      <i class="fas bg-Info"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> {{$value->hora_ini}}</span>
                        <h3 class="timeline-header"><a href="#" style="color:#01A9DB">Palpação</a></h3>
                        <div class="timeline-body">
                          {{$value->palpacao}}
                        </div>

                      </div>
                    </div>
                    <!-- END timeline item -->
                  @endif
                  @if($value->evolucao != null)
                    <!-- timeline item -->
                    <div>
                      <i class="fas bg-Navy"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> {{$value->hora_ini}}</span>
                        <h3 class="timeline-header"><a href="#" style="color:#0B0B3B">Evolução</a></h3>
                        <div class="timeline-body">
                          {{$value->evolucao}}
                        </div>

                      </div>
                    </div>
                    <!-- END timeline item -->
                  @endif
                @endforeach

                <div>
                  <i class="fas fa-clock bg-gray"></i>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
        @endif
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>


@section('javascript')
<!-- jQuery -->
<script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

@stop
