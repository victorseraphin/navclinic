@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(2)) !!}
              <a href="{{ route('cadastros.clientes.cadastrar') }}">
                  <button type="button" class="btn btn-outline-success btn-xs"><i class="fa fa-plus"></i> Novo CLiente</button>
              </a>
            </h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabela" class="table">
                <thead>
                <tr>
                  <th width="100px"><center>Ações</center></th>
                  <th>Nome</th>
                  <th>CPF</th>
                  <th>Fone</th>
                  <th>Email</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($tabela as $linha)
                    <tr>
                      <td>
                        <center>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default btn-flat {{($linha->status == '2' ? 'disabled' : '')}}">ações</button>
                            <button type="button" class="btn btn-default btn-flat {{($linha->status == '2' ? 'disabled' : '')}} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, 38px, 0px);">
                              @if(Auth::user()['login'] == 'P')
                              <a class="dropdown-item" href="{{route('prontuario',$linha->id)}}"><i class="fa fa-binoculars fa-fw"></i> Prontuário</a>
                              @endif
                              <a class="dropdown-item" href="{{route('cadastros.clientes.editar',$linha->id)}}"><i class="fas fa-edit fa-fw"></i> Editar</a>
                              <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" onclick="excluir({{$linha->id}})"><i class="fa fa-trash fa-fw"></i> Excluir</a>
                            </div>
                          </div>
                        </center>
                      </td>
                      <td>{{ $linha->nome }}</td>
                      <td>{{ $linha->cpf }}</td>
                      <td>{{ $linha->fone }}</td>
                      <td>{{ $linha->email }}</td>
                    </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>





        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script>
  $(function () {
    $('#tabela').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "aaSorting": [[1, "asc"]],
      "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
          "sNext": "Próximo",
          "sPrevious": "Anterior",
          "sFirst": "Primeiro",
          "sLast": "Último"
        },
        "oAria": {
          "sSortAscending": ": Ordenar colunas de forma ascendente",
          "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      }
    })
  });
  function excluir(id){
        swal({
            title: "Você tem certeza?",
            text: "Deseja excluir este registro?",
            icon: "warning",
            buttons:{
              confirm: {
                text : 'Sim!',
                className : 'btn btn-success'
              },
              cancel: {
                text : 'Não!',
                visible: true,
                className: 'btn btn-danger'
              }
            },
        })
        .then(function(willDelete) {
            if (willDelete) {
                window.location = "/cadastros/clientes/deletar/" + id

            } else {
                swal("Operação cancelada!");
            }
        });
    }
</script>
@stop
