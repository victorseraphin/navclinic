@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{!! ucwords(Request::segment(2)) !!}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- TABLE: LATEST ORDERS -->
        <div class="card">
          <div class="card-header border-transparent">
            <h3 class="card-title">{!! ucwords(Request::segment(3))." ".ucwords(Request::segment(2)) !!}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php if (isset($campos)):?>
              <form action="{{ route('cadastros.clientes.atualizar',$campos->id) }}" method="post" autocomplete="off">
            <?php else: ?>
              <form action="{{ route('cadastros.clientes.salvar') }}" method="post" autocomplete="off">
            <?php endif;?>
                {!! csrf_field() !!}
                <div class="row">
                  <div class="col-md-1">
                      <label>ID:</label>
                      <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}">
                        <input type="text" name="id" class="form-control" placeholder="ID" readonly
                        <?php if (old('id')):?>
                          value="{{old('id')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->id }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>
                  <div class="col-md-4">
                      <label>Nome:</label>
                      <div class="input-group has-feedback {{ $errors->has('nome') ? 'has-error' : '' }}">
                        @if ($errors->has('nome'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('nome') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="nome" class="form-control" placeholder="Nome"
                        <?php if (old('nome')):?>
                          value="{{old('nome')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->nome }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>
                  <div class="col-md-2">
                      <label>CPF:</label>
                      <div class="input-group has-feedback {{ $errors->has('cpf') ? 'has-error' : '' }}">
                        @if ($errors->has('cpf'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('cpf') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="cpf" class="form-control" placeholder="CPF"
                        <?php if (old('cpf')):?>
                          value="{{old('cpf')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->cpf }}"
                        <?php endif;?>
                        @if (isset($campos)) readonly @endif
                        >
                      </div>
                  </div>
                  <div class="col-md-2">
                      <label>Fone:</label>
                      <div class="input-group has-feedback {{ $errors->has('fone') ? 'has-error' : '' }}">
                        @if ($errors->has('fone'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('fone') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="fone" class="form-control" placeholder="Fone"
                        <?php if (old('fone')):?>
                          value="{{old('fone')}}"
                        <?php elseif (isset($campos)):?>
                          value="{{ $campos->fone }}"
                        <?php endif;?>
                        >
                      </div>
                  </div>


                  <div class="col-md-2">
                    <label>Sexo:</label>
                    <div class="form-group has-feedback {{ $errors->has('sexo') ? 'has-error' : '' }}">
                      <select name="sexo" class="form-control">
                        <option value="" readonly="readonly">Selecione...</option>
                          <?php if (isset($campos)):?>
                            <option value="F" @if($campos->sexo=='F') selected="selected" @endif> Feminino </option>
                            <option value="M" @if($campos->sexo=='M') selected="selected" @endif> Masculino </option>
                          <?php else:?>
                            <option @if(old('sexo')=="F") {{'selected="selected"'}} @endif value="F"> Feminino </option>
                            <option @if(old('sexo')=="M") {{'selected="selected"'}} @endif value="M"> Masculino </option>
                          <?php endif;?>
                      </select>
                      @if ($errors->has('sexo'))
                      <span class="help-block">
                        <strong>{{ $errors->first('sexo') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group has-feedback {{ $errors->has('nascimento') ? 'has-error' : '' }}">
                      <label>Aniversário:</label>
                      <div class="input-group date">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input id="nascimento" type="text" class="form-control pull-right" name="nascimento"
                        <?php if (isset($campos) and !empty($campos->nascimento)):?>
                          value="{{ date('d/m/Y',strtotime($campos->nascimento)) }}"
                        <?php else:?>
                          value="@if(old('nascimento')) {{old('nascimento')}} @endif"
                        <?php endif;?>
                        >
                        @if ($errors->has('nascimento'))
                          <span class="help-block">
                            <strong>{{ $errors->first('nascimento') }}</strong>
                          </span>
                        @endif
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>
                  <div class="col-md-2">
                      <label>CEP:</label>
                      <div class="input-group has-feedback {{ $errors->has('loc_ceps_id') ? 'has-error' : '' }}">
                        @if ($errors->has('loc_ceps_id'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('loc_ceps_id') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input id="loc_ceps_id" type="text" name="loc_ceps_id" class="form-control" placeholder="CEP" onblur="pesquisacep(this.value);" data-inputmask='"mask": "99.999-999"' data-mask
                          <?php if (old('loc_ceps_id')):?>
                            value="{{old('loc_ceps_id')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->loc_ceps_id }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <label>Endereço:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_descricao') ? 'has-error' : '' }}">
                        @if ($errors->has('end_descricao'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_descricao') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="hidden" id = "rua_nome" name="rua_nome" value="{{old('rua_nome')}}">
                        <input type="hidden" id = "cidade_nome" name="cidade_nome" value="{{old('cidade_nome')}}">
                        <input type="hidden" id = "bairro_nome" name="bairro_nome" value="{{old('bairro_nome')}}">
                        <input type="hidden" id = "end_uf" name="end_uf" value="{{old('end_uf')}}">
                        <input type="hidden" id = "cod_ibge" name="cod_ibge" value="{{old('cod_ibge')}}">
                        <input type="hidden" id = "unidade" name="unidade" value="{{old('unidade')}}">
                        <input type="hidden" id = "gia" name="gia" value="{{old('gia')}}">
                        <input id = "end_descricao" type="text" name="end_descricao" class="form-control" placeholder="Endereço" readonly
                          <?php if (old('end_descricao')):?>
                            value="{{old('end_descricao')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_descricao }}"
                          <?php endif;?>
                      >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Número:</label>
                      <div class="input-group has-feedback {{ $errors->has('end_numero') ? 'has-error' : '' }}">
                        @if ($errors->has('end_numero'))
                          <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" title="{{ $errors->first('end_numero') }}"><i class="fa fa-exclamation-triangle"></i></button>
                          </div>
                        @endif
                        <input type="text" name="end_numero" class="form-control" placeholder="Número"
                          <?php if (old('end_numero')):?>
                            value="{{old('end_numero')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->end_numero }}"
                          <?php endif;?>
                        >
                      </div>
                    </div>
                    <div class="col-md-4">
                        <label>E-mail:</label>
                        <div class="input-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                          @if ($errors->has('email'))
                            <div class="input-group-prepend">
                              <button type="button" class="btn btn-danger" title="{{ $errors->first('email') }}"><i class="fa fa-exclamation-triangle"></i></button>
                            </div>
                          @endif
                          <input type="text" name="email" class="form-control" placeholder="Email"
                          <?php if (old('email')):?>
                            value="{{old('email')}}"
                          <?php elseif (isset($campos)):?>
                            value="{{ $campos->email }}"
                          <?php endif;?>
                          >
                        </div>
                    </div>



                </div>


                  <div class="btn-group">
                    <div class="col-xs-12 col-lg-12">
                      <br />
                      <a href="{{ route('cadastros.clientes.listar') }}">
                        <button type="button" class="btn btn-block btn-danger btn-flat">Voltar</button>
                      </a>
                    </div>

                    <div class="col-xs-12 col-lg-12">
                      <br />
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Salvar</button>
                    </div>
                  </div>

                </div>
              </form>
            <!-- /.table-responsive -->
          </div>


      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script src="{{ asset('dist/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>

  $(function() {
     $( "#nascimento" ).datepicker({
       format: 'dd/mm/yyyy',
       autoclose: true,
     });
   });

</script>

@stop
