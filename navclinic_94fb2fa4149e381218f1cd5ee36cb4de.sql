--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.23
-- Dumped by pg_dump version 9.5.23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agenda; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.agenda (
    id integer NOT NULL,
    profissional_id integer,
    clientes_id integer,
    data date NOT NULL,
    hora_ini character varying(5) NOT NULL,
    hora_fin character varying(5) NOT NULL,
    status character varying(1),
    valor numeric(10,2),
    system_unit_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: agenda_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.agenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: agenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.agenda_id_seq OWNED BY public.agenda.id;


--
-- Name: clientes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clientes (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    cpf character varying(200),
    email character varying(200),
    fone character varying(200),
    nascimento date,
    sexo character varying(1),
    system_unit_id integer,
    loc_ceps_id integer,
    end_descricao character varying(200),
    end_numero character varying(8),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: clientes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.clientes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clientes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.clientes_id_seq OWNED BY public.clientes.id;


--
-- Name: enderecos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.enderecos (
    id integer NOT NULL,
    system_user_id integer,
    loc_ceps_id integer,
    end_descricao character varying(200) NOT NULL,
    end_numero character varying(8) NOT NULL,
    end_padrao character varying(1) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: enderecos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.enderecos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: enderecos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.enderecos_id_seq OWNED BY public.enderecos.id;


--
-- Name: loc_ceps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loc_ceps (
    id integer NOT NULL,
    cid_id integer NOT NULL,
    rua_nome character varying(191) NOT NULL,
    bairro_nome character varying(191) NOT NULL,
    unidade character varying(191),
    cod_ibge character varying(191),
    gia character varying(191),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: loc_cidades; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loc_cidades (
    id integer NOT NULL,
    cod_ibge integer NOT NULL,
    cid_nome character varying(191) NOT NULL,
    est_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: loc_cidades_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loc_cidades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loc_cidades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loc_cidades_id_seq OWNED BY public.loc_cidades.id;


--
-- Name: loc_estados; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loc_estados (
    id integer NOT NULL,
    est_nome character varying(191) NOT NULL,
    est_uf character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(191) NOT NULL,
    batch integer NOT NULL
);


--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: prontuario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prontuario (
    id integer NOT NULL,
    profissional_id integer,
    clientes_id integer,
    agenda_id integer,
    queixa text,
    historico text,
    observacao text,
    palpacao text,
    evolucao text,
    data date NOT NULL,
    status character varying(1),
    valor numeric(10,2),
    system_unit_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: prontuario_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.prontuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prontuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.prontuario_id_seq OWNED BY public.prontuario.id;


--
-- Name: system_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_group_id_seq OWNED BY public.system_group.id;


--
-- Name: system_group_program; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_group_program (
    id integer NOT NULL,
    system_group_id integer,
    system_program_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_group_program_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_group_program_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_group_program_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_group_program_id_seq OWNED BY public.system_group_program.id;


--
-- Name: system_group_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_group_user (
    id integer NOT NULL,
    system_user_id integer,
    system_group_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_group_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_group_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_group_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_group_user_id_seq OWNED BY public.system_group_user.id;


--
-- Name: system_password_resets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


--
-- Name: system_program; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_program (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    controller character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_program_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_program_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_program_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_program_id_seq OWNED BY public.system_program.id;


--
-- Name: system_program_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_program_user (
    id integer NOT NULL,
    system_user_id integer,
    system_program_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_program_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_program_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_program_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_program_user_id_seq OWNED BY public.system_program_user.id;


--
-- Name: system_unit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_unit (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_unit_id_seq OWNED BY public.system_unit.id;


--
-- Name: system_unit_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_unit_user (
    id integer NOT NULL,
    system_user_id integer,
    system_unit_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_unit_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_unit_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_unit_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_unit_user_id_seq OWNED BY public.system_unit_user.id;


--
-- Name: system_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_user (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    razao character varying(200),
    doc character varying(20),
    fone character varying(20),
    login character varying(20),
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    system_unit_id integer,
    frontpage_id integer,
    facebook_id character varying(255),
    google_id character varying(255),
    active character varying(1) DEFAULT 'Y'::character varying NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: system_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_user_id_seq OWNED BY public.system_user.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agenda ALTER COLUMN id SET DEFAULT nextval('public.agenda_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clientes ALTER COLUMN id SET DEFAULT nextval('public.clientes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.enderecos ALTER COLUMN id SET DEFAULT nextval('public.enderecos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_cidades ALTER COLUMN id SET DEFAULT nextval('public.loc_cidades_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario ALTER COLUMN id SET DEFAULT nextval('public.prontuario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group ALTER COLUMN id SET DEFAULT nextval('public.system_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_program ALTER COLUMN id SET DEFAULT nextval('public.system_group_program_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_user ALTER COLUMN id SET DEFAULT nextval('public.system_group_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program ALTER COLUMN id SET DEFAULT nextval('public.system_program_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program_user ALTER COLUMN id SET DEFAULT nextval('public.system_program_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit ALTER COLUMN id SET DEFAULT nextval('public.system_unit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit_user ALTER COLUMN id SET DEFAULT nextval('public.system_unit_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user ALTER COLUMN id SET DEFAULT nextval('public.system_user_id_seq'::regclass);


--
-- Data for Name: agenda; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.agenda VALUES (2, 2, 1, '2019-09-13', '10:00', '10:30', '3', NULL, 1, '2019-09-13 11:52:49', '2019-09-13 11:53:02', NULL);
INSERT INTO public.agenda VALUES (3, 2, 1, '2019-09-17', '17:10', '01:40', NULL, NULL, 1, '2019-09-16 20:29:58', '2019-09-16 20:29:58', NULL);
INSERT INTO public.agenda VALUES (1, 2, 1, '2019-09-13', '16:00', '16:30', '5', NULL, 1, '2019-09-12 18:14:23', '2019-09-12 18:16:25', NULL);
INSERT INTO public.agenda VALUES (5, 2, 1, '2019-09-17', '17:00', '17:30', '3', NULL, 1, '2019-09-16 20:43:25', '2019-09-19 20:57:06', NULL);
INSERT INTO public.agenda VALUES (4, 2, 1, '2019-09-17', '17:10', '01:40', '2', NULL, 1, '2019-09-16 20:29:59', '2019-09-19 20:58:17', NULL);
INSERT INTO public.agenda VALUES (6, 2, 1, '2019-09-20', '17:00', '17:30', NULL, NULL, 1, '2019-09-19 21:01:06', '2019-09-19 21:01:06', NULL);
INSERT INTO public.agenda VALUES (7, 2, 3, '2019-10-28', '14:00', '15:00', '3', NULL, 1, '2019-10-28 17:00:14', '2019-10-28 17:00:24', NULL);
INSERT INTO public.agenda VALUES (8, 2, 9, '2019-10-28', '14:50', '15:00', '3', NULL, 1, '2019-10-28 18:01:48', '2019-10-28 18:02:17', NULL);
INSERT INTO public.agenda VALUES (9, 2, 29, '2019-10-28', '15:00', '15:15', '1', NULL, 1, '2019-10-28 18:04:14', '2019-10-28 18:11:01', NULL);
INSERT INTO public.agenda VALUES (10, 2, 11, '2019-10-28', '15:15', '16:00', '3', NULL, 1, '2019-10-28 18:07:05', '2019-10-28 18:23:33', NULL);
INSERT INTO public.agenda VALUES (23, 2, 26, '2019-10-30', '16:30', '17:00', NULL, NULL, 1, '2019-10-28 18:35:43', '2019-10-28 18:35:43', NULL);
INSERT INTO public.agenda VALUES (24, 2, 13, '2019-10-30', '17:30', '18:00', NULL, NULL, 1, '2019-10-28 18:36:07', '2019-10-28 18:36:07', NULL);
INSERT INTO public.agenda VALUES (32, 2, 19, '2019-10-30', '15:00', '15:30', '2', NULL, 1, '2019-10-30 12:30:14', '2019-10-30 12:30:25', NULL);
INSERT INTO public.agenda VALUES (40, 2, 20, '2019-11-06', '10:30', '11:00', NULL, NULL, 1, '2019-11-06 13:27:29', '2019-11-06 13:27:29', NULL);
INSERT INTO public.agenda VALUES (20, 2, 30, '2019-10-28', '16:00', '16:30', '3', NULL, 1, '2019-10-28 18:23:02', '2019-10-28 19:02:20', NULL);
INSERT INTO public.agenda VALUES (11, 2, 12, '2019-10-28', '16:30', '17:00', '3', NULL, 1, '2019-10-28 18:07:51', '2019-10-28 19:29:04', NULL);
INSERT INTO public.agenda VALUES (30, 2, 1, '2019-10-28', '18:00', '19:00', '2', NULL, 1, '2019-10-28 19:30:00', '2019-10-28 19:30:15', NULL);
INSERT INTO public.agenda VALUES (21, 2, 31, '2019-10-28', '17:00', '17:30', '3', NULL, 1, '2019-10-28 18:31:33', '2019-10-28 20:18:15', NULL);
INSERT INTO public.agenda VALUES (12, 2, 13, '2019-10-28', '17:30', '18:00', '2', NULL, 1, '2019-10-28 18:08:19', '2019-10-28 20:37:30', NULL);
INSERT INTO public.agenda VALUES (31, 2, 1, '2019-10-28', '17:30', '18:00', '1', NULL, 1, '2019-10-28 20:38:22', '2019-10-28 20:42:32', NULL);
INSERT INTO public.agenda VALUES (34, 2, 21, '2019-11-01', '11:00', '11:30', '2', NULL, 1, '2019-10-30 13:28:10', '2019-10-30 13:28:18', NULL);
INSERT INTO public.agenda VALUES (13, 2, 23, '2019-10-30', '10:30', '11:00', '3', NULL, 1, '2019-10-28 18:13:04', '2019-10-30 13:28:25', NULL);
INSERT INTO public.agenda VALUES (14, 2, 5, '2019-10-30', '11:00', '11:30', '3', NULL, 1, '2019-10-28 18:13:55', '2019-10-30 14:00:41', NULL);
INSERT INTO public.agenda VALUES (36, 2, 33, '2019-11-01', '10:30', '11:00', '3', NULL, 1, '2019-11-01 13:24:23', '2019-11-01 13:24:33', NULL);
INSERT INTO public.agenda VALUES (15, 2, 24, '2019-10-30', '11:30', '12:00', '3', NULL, 1, '2019-10-28 18:14:24', '2019-10-30 17:01:53', NULL);
INSERT INTO public.agenda VALUES (16, 2, 16, '2019-10-30', '13:15', '14:00', '3', NULL, 1, '2019-10-28 18:14:53', '2019-10-30 17:02:18', NULL);
INSERT INTO public.agenda VALUES (17, 2, 11, '2019-10-30', '14:00', '14:30', '2', NULL, 1, '2019-10-28 18:15:19', '2019-10-30 17:07:54', NULL);
INSERT INTO public.agenda VALUES (35, 2, 30, '2019-10-30', '14:00', '14:30', '3', NULL, 1, '2019-10-30 17:08:37', '2019-10-30 17:08:51', NULL);
INSERT INTO public.agenda VALUES (18, 2, 18, '2019-10-30', '14:30', '15:00', '3', NULL, 1, '2019-10-28 18:15:50', '2019-10-30 18:29:15', NULL);
INSERT INTO public.agenda VALUES (33, 2, 21, '2019-10-30', '15:00', '15:30', '3', NULL, 1, '2019-10-30 13:27:30', '2019-10-30 18:29:23', NULL);
INSERT INTO public.agenda VALUES (19, 2, 20, '2019-10-30', '15:30', '16:00', '3', NULL, 1, '2019-10-28 18:19:58', '2019-10-30 18:29:30', NULL);
INSERT INTO public.agenda VALUES (22, 2, 32, '2019-10-30', '16:00', '16:30', '3', NULL, 1, '2019-10-28 18:35:14', '2019-10-30 19:02:54', NULL);
INSERT INTO public.agenda VALUES (37, 2, 34, '2019-11-01', '11:00', '11:30', '3', NULL, 1, '2019-11-01 14:15:04', '2019-11-01 14:15:15', NULL);
INSERT INTO public.agenda VALUES (28, 2, 27, '2019-11-01', '15:00', '15:30', '3', NULL, 1, '2019-10-28 18:39:55', '2019-11-01 18:00:27', NULL);
INSERT INTO public.agenda VALUES (27, 2, 17, '2019-11-01', '14:00', '15:00', '2', NULL, 1, '2019-10-28 18:39:11', '2019-11-01 18:01:33', NULL);
INSERT INTO public.agenda VALUES (41, 2, 5, '2019-11-06', '14:00', '14:30', NULL, NULL, 1, '2019-11-06 17:01:21', '2019-11-06 17:01:21', NULL);
INSERT INTO public.agenda VALUES (25, 2, 22, '2019-11-01', '13:00', '13:30', '3', NULL, 1, '2019-10-28 18:36:50', '2019-11-01 18:01:51', NULL);
INSERT INTO public.agenda VALUES (26, 2, 23, '2019-11-01', '13:30', '14:00', '3', NULL, 1, '2019-10-28 18:38:38', '2019-11-01 18:08:10', NULL);
INSERT INTO public.agenda VALUES (29, 2, 18, '2019-11-01', '15:30', '16:00', '3', NULL, 1, '2019-10-28 18:40:26', '2019-11-01 18:37:27', NULL);
INSERT INTO public.agenda VALUES (38, 2, 32, '2019-11-01', '16:00', '16:30', '2', NULL, 1, '2019-11-01 18:09:07', '2019-11-01 19:27:45', NULL);
INSERT INTO public.agenda VALUES (39, 2, 11, '2019-11-01', '16:30', '17:00', '2', NULL, 1, '2019-11-01 18:09:38', '2019-11-01 19:27:53', NULL);
INSERT INTO public.agenda VALUES (51, 2, 38, '2019-11-20', '10:00', '10:30', NULL, NULL, 1, '2019-11-20 10:35:49', '2019-11-20 10:35:49', NULL);
INSERT INTO public.agenda VALUES (52, 2, 39, '2019-11-20', '10:30', '11:00', NULL, NULL, 1, '2019-11-20 10:37:58', '2019-11-20 10:37:58', NULL);
INSERT INTO public.agenda VALUES (44, 2, 17, '2019-11-08', '14:00', '15:00', '3', NULL, 1, '2019-11-06 18:23:57', '2019-11-08 18:32:25', NULL);
INSERT INTO public.agenda VALUES (45, 2, 11, '2019-11-08', '15:00', '15:30', '3', NULL, 1, '2019-11-06 18:24:54', '2019-11-08 18:32:33', NULL);
INSERT INTO public.agenda VALUES (46, 2, 23, '2019-11-08', '15:30', '16:00', '3', NULL, 1, '2019-11-06 18:25:41', '2019-11-08 18:32:40', NULL);
INSERT INTO public.agenda VALUES (47, 2, 28, '2019-11-08', '16:00', '16:30', '3', NULL, 1, '2019-11-06 18:26:47', '2019-11-08 19:09:01', NULL);
INSERT INTO public.agenda VALUES (48, 2, 5, '2019-11-08', '16:30', '17:00', '3', NULL, 1, '2019-11-06 18:27:43', '2019-11-08 19:30:56', NULL);
INSERT INTO public.agenda VALUES (49, 2, 36, '2019-11-08', '17:00', '17:30', '3', NULL, 1, '2019-11-08 20:03:33', '2019-11-08 20:03:41', NULL);
INSERT INTO public.agenda VALUES (50, 2, 37, '2019-11-08', '17:30', '18:00', '3', NULL, 1, '2019-11-08 20:32:24', '2019-11-08 20:32:31', NULL);
INSERT INTO public.agenda VALUES (42, 2, 22, '2019-11-08', '13:00', '13:30', '4', NULL, 1, '2019-11-06 18:21:57', '2019-11-08 21:01:59', NULL);
INSERT INTO public.agenda VALUES (43, 2, 30, '2019-11-08', '13:30', '14:00', '4', NULL, 1, '2019-11-06 18:23:04', '2019-11-08 21:02:46', NULL);
INSERT INTO public.agenda VALUES (53, 2, 5, '2019-11-20', '11:00', '11:30', NULL, NULL, 1, '2019-11-20 10:38:39', '2019-11-20 10:38:39', NULL);
INSERT INTO public.agenda VALUES (54, 2, 24, '2019-11-20', '11:30', '12:00', NULL, NULL, 1, '2019-11-20 10:39:12', '2019-11-20 10:39:12', NULL);
INSERT INTO public.agenda VALUES (55, 2, 16, '2019-11-20', '13:15', '14:00', NULL, NULL, 1, '2019-11-20 10:39:37', '2019-11-20 10:39:37', NULL);
INSERT INTO public.agenda VALUES (56, 2, 40, '2019-11-20', '14:00', '14:30', NULL, NULL, 1, '2019-11-20 10:40:10', '2019-11-20 10:40:10', NULL);
INSERT INTO public.agenda VALUES (57, 2, 18, '2019-11-20', '14:30', '15:00', NULL, NULL, 1, '2019-11-20 10:40:33', '2019-11-20 10:40:33', NULL);
INSERT INTO public.agenda VALUES (58, 2, 21, '2019-11-20', '15:00', '15:30', NULL, NULL, 1, '2019-11-20 10:40:55', '2019-11-20 10:40:55', NULL);
INSERT INTO public.agenda VALUES (59, 2, 32, '2019-11-20', '16:00', '16:30', NULL, NULL, 1, '2019-11-20 10:41:23', '2019-11-20 10:41:23', NULL);
INSERT INTO public.agenda VALUES (60, 2, 26, '2019-11-20', '16:30', '17:15', NULL, NULL, 1, '2019-11-20 10:41:49', '2019-11-20 10:41:49', NULL);
INSERT INTO public.agenda VALUES (61, 2, 41, '2019-11-20', '17:15', '18:00', NULL, NULL, 1, '2019-11-20 10:42:47', '2019-11-20 10:42:47', NULL);
INSERT INTO public.agenda VALUES (65, 2, 7, '2019-12-09', '13:00', '14:00', NULL, NULL, 1, '2019-12-06 14:01:43', '2019-12-06 14:01:43', NULL);
INSERT INTO public.agenda VALUES (66, 2, 3, '2019-12-09', '14:00', '15:00', NULL, NULL, 1, '2019-12-06 14:02:31', '2019-12-06 14:02:31', NULL);
INSERT INTO public.agenda VALUES (67, 2, 22, '2019-12-06', '13:00', '13:30', '3', NULL, 1, '2019-12-06 14:10:12', '2019-12-06 16:10:57', NULL);
INSERT INTO public.agenda VALUES (73, 2, 11, '2019-12-09', '15:30', '16:00', NULL, NULL, 1, '2019-12-06 17:42:07', '2019-12-06 17:42:07', NULL);
INSERT INTO public.agenda VALUES (74, 2, 34, '2019-12-09', '16:00', '16:30', NULL, NULL, 1, '2019-12-06 17:42:53', '2019-12-06 17:42:53', NULL);
INSERT INTO public.agenda VALUES (63, 2, 4, '2019-12-09', '10:30', '11:15', '2', NULL, 1, '2019-12-06 14:00:24', '2019-12-09 12:19:03', NULL);
INSERT INTO public.agenda VALUES (68, 2, 23, '2019-12-06', '13:30', '14:00', '3', NULL, 1, '2019-12-06 14:14:18', '2019-12-06 18:43:02', NULL);
INSERT INTO public.agenda VALUES (70, 2, 11, '2019-12-06', '15:00', '15:30', '3', NULL, 1, '2019-12-06 14:15:24', '2019-12-06 18:43:23', NULL);
INSERT INTO public.agenda VALUES (71, 2, 18, '2019-12-06', '15:30', '16:00', '3', NULL, 1, '2019-12-06 14:15:47', '2019-12-06 18:43:29', NULL);
INSERT INTO public.agenda VALUES (72, 2, 28, '2019-12-06', '16:00', '16:30', '3', NULL, 1, '2019-12-06 14:16:15', '2019-12-06 18:58:08', NULL);
INSERT INTO public.agenda VALUES (69, 2, 17, '2019-12-06', '14:00', '15:00', '2', NULL, 1, '2019-12-06 14:14:53', '2019-12-06 18:58:29', NULL);
INSERT INTO public.agenda VALUES (75, 2, 30, '2019-12-09', '10:30', '11:00', '3', NULL, 1, '2019-12-09 12:19:37', '2019-12-09 13:12:25', NULL);
INSERT INTO public.agenda VALUES (62, 2, 35, '2019-12-09', '10:00', '10:30', '3', NULL, 1, '2019-12-06 13:59:40', '2019-12-09 13:12:31', NULL);
INSERT INTO public.agenda VALUES (64, 2, 39, '2019-12-09', '11:15', '11:30', '3', NULL, 1, '2019-12-06 14:01:04', '2019-12-09 13:56:17', NULL);
INSERT INTO public.agenda VALUES (76, 2, 35, '2019-12-11', '10:00', '10:30', NULL, NULL, 1, '2019-12-11 13:04:37', '2019-12-11 13:04:37', NULL);
INSERT INTO public.agenda VALUES (77, 2, 5, '2019-12-11', '11:00', '11:30', NULL, NULL, 1, '2019-12-11 13:05:40', '2019-12-11 13:05:40', NULL);
INSERT INTO public.agenda VALUES (78, 2, 24, '2019-12-12', '11:30', '12:00', '2', NULL, 1, '2019-12-11 13:06:02', '2019-12-11 13:07:02', NULL);
INSERT INTO public.agenda VALUES (80, 2, 24, '2019-12-11', '11:30', '12:00', NULL, NULL, 1, '2019-12-11 13:07:19', '2019-12-11 13:07:19', NULL);
INSERT INTO public.agenda VALUES (81, 2, 38, '2019-12-11', '14:00', '14:10', NULL, NULL, 1, '2019-12-11 13:08:14', '2019-12-11 13:08:14', NULL);
INSERT INTO public.agenda VALUES (79, 2, 16, '2019-12-12', '13:15', '14:00', '2', NULL, 1, '2019-12-11 13:06:39', '2019-12-11 13:08:28', NULL);
INSERT INTO public.agenda VALUES (82, 2, 16, '2019-12-11', '13:00', '14:00', NULL, NULL, 1, '2019-12-11 13:08:49', '2019-12-11 13:08:49', NULL);
INSERT INTO public.agenda VALUES (83, 2, 30, '2019-12-11', '14:30', '15:00', NULL, NULL, 1, '2019-12-11 13:10:15', '2019-12-11 13:10:15', NULL);
INSERT INTO public.agenda VALUES (84, 2, 28, '2019-12-11', '16:00', '16:30', '5', NULL, 1, '2019-12-11 13:16:16', '2019-12-13 19:11:58', NULL);
INSERT INTO public.agenda VALUES (85, 2, 22, '2020-01-20', '09:30', '10:00', '5', NULL, 1, '2020-01-20 17:14:05', '2020-01-20 17:18:13', NULL);
INSERT INTO public.agenda VALUES (86, 2, 39, '2020-01-20', '10:00', '10:30', '5', NULL, 1, '2020-01-20 17:15:18', '2020-01-20 17:19:22', NULL);
INSERT INTO public.agenda VALUES (88, 2, 5, '2020-01-20', '11:30', '12:00', NULL, NULL, 1, '2020-01-20 18:54:41', '2020-01-20 18:54:41', NULL);
INSERT INTO public.agenda VALUES (87, 2, 4, '2020-01-20', '10:30', '11:30', '5', NULL, 1, '2020-01-20 17:20:50', '2020-01-20 17:21:39', NULL);
INSERT INTO public.agenda VALUES (89, 2, 5, '2020-01-20', '11:30', '12:00', NULL, NULL, 1, '2020-01-20 18:55:06', '2020-01-20 18:55:06', NULL);
INSERT INTO public.agenda VALUES (90, 2, 5, '2020-01-20', '11:30', '12:00', NULL, NULL, 1, '2020-01-20 18:56:40', '2020-01-20 18:56:40', NULL);
INSERT INTO public.agenda VALUES (91, 2, 43, '2020-01-20', '13:00', '14:00', NULL, NULL, 1, '2020-01-20 18:58:54', '2020-01-20 18:58:54', NULL);
INSERT INTO public.agenda VALUES (92, 2, 42, '2020-01-20', '14:30', '15:00', NULL, NULL, 1, '2020-01-20 18:59:55', '2020-01-20 18:59:55', NULL);
INSERT INTO public.agenda VALUES (93, 2, 44, '2020-01-20', '15:00', '15:30', NULL, NULL, 1, '2020-01-20 19:01:06', '2020-01-20 19:01:06', NULL);
INSERT INTO public.agenda VALUES (94, 2, 11, '2020-01-20', '15:30', '16:00', NULL, NULL, 1, '2020-01-20 19:20:23', '2020-01-20 19:20:23', NULL);
INSERT INTO public.agenda VALUES (95, 2, 45, '2020-01-20', '16:00', '16:30', NULL, NULL, 1, '2020-01-20 19:21:58', '2020-01-20 19:21:58', NULL);
INSERT INTO public.agenda VALUES (96, 2, 46, '2020-01-20', '16:30', '17:00', NULL, NULL, 1, '2020-01-20 19:23:03', '2020-01-20 19:23:03', NULL);
INSERT INTO public.agenda VALUES (97, 2, 47, '2020-01-20', '17:00', '17:30', NULL, NULL, 1, '2020-01-20 19:24:35', '2020-01-20 19:24:35', NULL);
INSERT INTO public.agenda VALUES (99, 2, 31, '2020-01-20', '17:30', '18:00', '2', NULL, 1, '2020-01-20 20:03:48', '2020-01-20 20:04:08', NULL);
INSERT INTO public.agenda VALUES (103, 2, 24, '2020-01-22', '11:30', '12:00', NULL, NULL, 1, '2020-01-20 20:19:13', '2020-01-20 20:19:13', NULL);
INSERT INTO public.agenda VALUES (106, 2, 18, '2020-01-22', '14:30', '15:00', NULL, NULL, 1, '2020-01-20 20:21:58', '2020-01-20 20:21:58', NULL);
INSERT INTO public.agenda VALUES (108, 2, 32, '2020-01-22', '15:30', '16:00', NULL, NULL, 1, '2020-01-20 20:23:37', '2020-01-20 20:23:37', NULL);
INSERT INTO public.agenda VALUES (113, 2, 53, '2020-01-24', '13:00', '13:30', NULL, NULL, 1, '2020-01-20 20:27:29', '2020-01-20 20:27:29', NULL);
INSERT INTO public.agenda VALUES (112, 2, 53, '2020-01-24', '13:30', '14:00', '2', NULL, 1, '2020-01-20 20:26:54', '2020-01-20 20:27:39', NULL);
INSERT INTO public.agenda VALUES (101, 2, 48, '2020-01-22', '10:30', '11:00', '3', NULL, 1, '2020-01-20 20:18:25', '2020-01-22 13:32:36', NULL);
INSERT INTO public.agenda VALUES (98, 2, 31, '2020-01-20', '17:30', '18:00', '5', NULL, 1, '2020-01-20 20:03:48', '2020-01-20 20:44:36', NULL);
INSERT INTO public.agenda VALUES (100, 2, 35, '2020-01-22', '10:00', '10:30', '3', NULL, 1, '2020-01-20 20:17:26', '2020-01-22 13:32:44', NULL);
INSERT INTO public.agenda VALUES (140, 2, 49, '2020-01-29', '14:00', '14:30', '5', NULL, 1, '2020-01-27 14:19:55', '2020-01-29 17:30:56', NULL);
INSERT INTO public.agenda VALUES (102, 2, 5, '2020-01-22', '11:00', '11:30', '3', NULL, 1, '2020-01-20 20:18:51', '2020-01-22 14:00:03', NULL);
INSERT INTO public.agenda VALUES (125, 2, 4, '2020-01-27', '10:30', '11:30', '3', NULL, 1, '2020-01-24 19:21:04', '2020-01-27 13:27:54', NULL);
INSERT INTO public.agenda VALUES (104, 2, 49, '2020-01-22', '13:30', '14:00', '5', NULL, 1, '2020-01-20 20:20:04', '2020-01-22 16:42:44', NULL);
INSERT INTO public.agenda VALUES (121, 2, 57, '2020-01-24', '13:30', '14:00', '5', NULL, 1, '2020-01-22 17:21:42', '2020-01-24 16:45:33', NULL);
INSERT INTO public.agenda VALUES (105, 2, 50, '2020-01-22', '14:00', '14:30', '5', NULL, 1, '2020-01-20 20:21:22', '2020-01-22 17:07:38', NULL);
INSERT INTO public.agenda VALUES (107, 2, 51, '2020-01-22', '15:00', '15:30', '2', NULL, 1, '2020-01-20 20:23:06', '2020-01-22 17:14:02', NULL);
INSERT INTO public.agenda VALUES (111, 2, 42, '2020-01-22', '17:00', '17:30', '2', NULL, 1, '2020-01-20 20:25:48', '2020-01-22 17:18:58', NULL);
INSERT INTO public.agenda VALUES (120, 2, 31, '2020-01-22', '17:00', '17:30', NULL, NULL, 1, '2020-01-22 17:19:39', '2020-01-22 17:19:39', NULL);
INSERT INTO public.agenda VALUES (115, 2, 54, '2020-01-24', '15:00', '15:30', '2', NULL, 1, '2020-01-20 20:46:19', '2020-01-24 16:45:47', NULL);
INSERT INTO public.agenda VALUES (110, 2, 52, '2020-01-22', '16:30', '17:00', '5', NULL, 1, '2020-01-20 20:25:18', '2020-01-22 19:39:22', NULL);
INSERT INTO public.agenda VALUES (109, 2, 30, '2020-01-22', '16:00', '16:30', '3', NULL, 1, '2020-01-20 20:24:08', '2020-01-22 21:10:06', NULL);
INSERT INTO public.agenda VALUES (114, 2, 17, '2020-01-24', '14:00', '15:00', '2', NULL, 1, '2020-01-20 20:28:58', '2020-01-24 17:40:52', NULL);
INSERT INTO public.agenda VALUES (122, 2, 45, '2020-01-24', '14:45', '15:00', '5', NULL, 1, '2020-01-24 17:58:46', '2020-01-24 18:01:06', NULL);
INSERT INTO public.agenda VALUES (116, 2, 18, '2020-01-24', '15:30', '16:00', '2', NULL, 1, '2020-01-20 20:47:30', '2020-01-24 18:24:15', NULL);
INSERT INTO public.agenda VALUES (123, 2, 31, '2020-01-24', '15:30', '16:00', '3', NULL, 1, '2020-01-24 18:25:36', '2020-01-24 18:25:55', NULL);
INSERT INTO public.agenda VALUES (117, 2, 32, '2020-01-24', '16:00', '16:30', '5', NULL, 1, '2020-01-20 20:48:21', '2020-01-24 19:15:05', NULL);
INSERT INTO public.agenda VALUES (156, 2, 24, '2020-01-29', '11:30', '12:00', '5', NULL, 1, '2020-01-29 13:56:12', '2020-01-29 16:28:25', NULL);
INSERT INTO public.agenda VALUES (124, 2, 4, '2020-01-27', '10:30', '11:30', '5', NULL, 1, '2020-01-24 19:21:03', '2020-01-27 14:04:40', NULL);
INSERT INTO public.agenda VALUES (118, 2, 47, '2020-01-24', '16:30', '17:00', '5', NULL, 1, '2020-01-20 20:49:11', '2020-01-24 20:07:16', NULL);
INSERT INTO public.agenda VALUES (119, 2, 41, '2020-01-24', '17:00', '17:30', '2', NULL, 1, '2020-01-20 20:50:13', '2020-01-24 20:07:26', NULL);
INSERT INTO public.agenda VALUES (131, 2, 23, '2020-01-27', '13:30', '14:00', NULL, NULL, 1, '2020-01-27 14:13:17', '2020-01-27 14:13:17', NULL);
INSERT INTO public.agenda VALUES (135, 2, 46, '2020-01-27', '16:30', '17:00', NULL, NULL, 1, '2020-01-27 14:17:21', '2020-01-27 14:17:21', NULL);
INSERT INTO public.agenda VALUES (136, 2, 47, '2020-01-27', '17:00', '17:30', NULL, NULL, 1, '2020-01-27 14:17:51', '2020-01-27 14:17:51', NULL);
INSERT INTO public.agenda VALUES (146, 2, 39, '2020-01-30', '14:00', '14:30', NULL, NULL, 1, '2020-01-27 16:40:57', '2020-01-27 16:40:57', NULL);
INSERT INTO public.agenda VALUES (163, 2, 4, '2020-02-03', '10:00', '11:00', '5', NULL, 1, '2020-01-29 19:25:47', '2020-02-03 13:33:41', NULL);
INSERT INTO public.agenda VALUES (130, 2, 23, '2020-01-27', '13:30', '14:00', '5', NULL, 1, '2020-01-27 14:13:16', '2020-01-27 16:46:42', NULL);
INSERT INTO public.agenda VALUES (138, 2, 50, '2020-01-29', '13:00', '13:30', '5', NULL, 1, '2020-01-27 14:18:58', '2020-01-29 16:30:25', NULL);
INSERT INTO public.agenda VALUES (129, 2, 60, '2020-01-29', '16:00', '16:30', '5', NULL, 1, '2020-01-27 13:50:21', '2020-01-29 19:24:04', NULL);
INSERT INTO public.agenda VALUES (128, 2, 3, '2020-01-27', '14:00', '14:30', '5', NULL, 1, '2020-01-27 13:30:52', '2020-01-27 18:01:17', NULL);
INSERT INTO public.agenda VALUES (126, 2, 5, '2020-01-27', '11:30', '12:00', '5', NULL, 1, '2020-01-24 19:24:42', '2020-01-27 18:01:35', NULL);
INSERT INTO public.agenda VALUES (127, 2, 59, '2020-01-29', '10:30', '11:00', '5', NULL, 1, '2020-01-27 13:30:15', '2020-01-29 13:44:05', NULL);
INSERT INTO public.agenda VALUES (132, 2, 45, '2020-01-27', '15:00', '15:30', '5', NULL, 1, '2020-01-27 14:13:53', '2020-01-27 18:02:54', NULL);
INSERT INTO public.agenda VALUES (134, 2, 22, '2020-01-27', '16:00', '16:30', '5', NULL, 1, '2020-01-27 14:16:50', '2020-01-27 19:27:23', NULL);
INSERT INTO public.agenda VALUES (137, 2, 35, '2020-01-29', '10:00', '10:30', '5', NULL, 1, '2020-01-27 14:18:27', '2020-01-29 16:31:06', NULL);
INSERT INTO public.agenda VALUES (133, 2, 11, '2020-01-27', '15:30', '16:00', '5', NULL, 1, '2020-01-27 14:14:25', '2020-01-27 19:28:18', NULL);
INSERT INTO public.agenda VALUES (161, 2, 54, '2020-01-27', '17:30', '18:00', NULL, NULL, 1, '2020-01-29 16:33:26', '2020-01-29 16:33:26', NULL);
INSERT INTO public.agenda VALUES (153, 2, 41, '2020-01-31', '17:00', '18:00', '2', NULL, 1, '2020-01-27 16:45:37', '2020-01-29 16:33:43', NULL);
INSERT INTO public.agenda VALUES (155, 2, 5, '2020-01-29', '11:00', '11:30', '5', NULL, 1, '2020-01-29 13:54:36', '2020-01-29 14:22:27', NULL);
INSERT INTO public.agenda VALUES (151, 2, 28, '2020-01-31', '16:00', '16:30', '5', NULL, 1, '2020-01-27 16:44:24', '2020-01-31 19:19:44', NULL);
INSERT INTO public.agenda VALUES (141, 2, 18, '2020-01-29', '14:30', '15:00', '5', NULL, 1, '2020-01-27 14:20:20', '2020-01-29 17:43:34', NULL);
INSERT INTO public.agenda VALUES (143, 2, 52, '2020-01-29', '15:30', '16:00', '2', NULL, 1, '2020-01-27 14:21:16', '2020-01-29 17:54:07', NULL);
INSERT INTO public.agenda VALUES (139, 2, 48, '2020-01-29', '13:30', '14:00', '5', NULL, 1, '2020-01-27 14:19:20', '2020-01-29 16:47:20', NULL);
INSERT INTO public.agenda VALUES (142, 2, 30, '2020-01-29', '15:00', '15:30', '5', NULL, 1, '2020-01-27 14:20:49', '2020-01-29 18:27:38', NULL);
INSERT INTO public.agenda VALUES (171, 2, 46, '2020-02-03', '16:30', '17:00', '5', NULL, 1, '2020-01-29 19:30:22', '2020-02-04 18:26:55', NULL);
INSERT INTO public.agenda VALUES (154, 2, 16, '2020-01-29', '17:00', '18:00', '5', NULL, 1, '2020-01-27 18:25:55', '2020-01-29 20:12:18', NULL);
INSERT INTO public.agenda VALUES (144, 2, 51, '2020-01-29', '16:30', '17:00', '5', NULL, 1, '2020-01-27 14:22:11', '2020-01-29 20:11:58', NULL);
INSERT INTO public.agenda VALUES (145, 2, 23, '2020-01-30', '13:00', '13:30', '5', NULL, 1, '2020-01-27 16:40:25', '2020-01-30 16:31:52', NULL);
INSERT INTO public.agenda VALUES (291, 2, 11, '2020-02-19', '14:00', '14:30', NULL, NULL, 1, '2020-02-19 14:18:35', '2020-02-19 14:18:35', NULL);
INSERT INTO public.agenda VALUES (158, 2, 59, '2020-01-30', '15:30', '16:00', '5', NULL, 1, '2020-01-29 14:24:34', '2020-01-30 18:38:11', NULL);
INSERT INTO public.agenda VALUES (162, 2, 64, '2020-01-31', '13:30', '14:00', '5', NULL, 1, '2020-01-29 18:46:50', '2020-01-31 16:53:33', NULL);
INSERT INTO public.agenda VALUES (159, 2, 62, '2020-01-30', '16:00', '16:30', '5', NULL, 1, '2020-01-29 14:26:13', '2020-01-30 19:25:26', NULL);
INSERT INTO public.agenda VALUES (160, 2, 63, '2020-01-30', '16:30', '17:30', '5', NULL, 1, '2020-01-29 16:32:11', '2020-01-30 20:04:09', NULL);
INSERT INTO public.agenda VALUES (150, 2, 18, '2020-01-31', '15:30', '16:00', '5', NULL, 1, '2020-01-27 16:43:49', '2020-01-31 19:11:36', NULL);
INSERT INTO public.agenda VALUES (147, 2, 53, '2020-01-31', '13:00', '13:30', '5', NULL, 1, '2020-01-27 16:41:37', '2020-01-31 17:05:29', NULL);
INSERT INTO public.agenda VALUES (149, 2, 61, '2020-01-31', '15:00', '15:30', '5', NULL, 1, '2020-01-27 16:43:19', '2020-01-31 18:31:14', NULL);
INSERT INTO public.agenda VALUES (152, 2, 47, '2020-01-31', '16:30', '17:00', '3', NULL, 1, '2020-01-27 16:44:58', '2020-01-31 19:28:18', NULL);
INSERT INTO public.agenda VALUES (167, 2, 3, '2020-02-03', '14:00', '15:00', '2', NULL, 1, '2020-01-29 19:28:08', '2020-02-03 13:44:48', NULL);
INSERT INTO public.agenda VALUES (164, 2, 49, '2020-02-03', '11:00', '11:30', '5', NULL, 1, '2020-01-29 19:26:15', '2020-02-03 17:03:22', NULL);
INSERT INTO public.agenda VALUES (169, 2, 11, '2020-02-03', '15:30', '16:00', '2', NULL, 1, '2020-01-29 19:29:12', '2020-02-03 17:05:35', NULL);
INSERT INTO public.agenda VALUES (165, 2, 5, '2020-02-03', '11:30', '12:00', '5', NULL, 1, '2020-01-29 19:26:44', '2020-02-03 17:04:22', NULL);
INSERT INTO public.agenda VALUES (168, 2, 44, '2020-02-03', '15:00', '15:30', '5', NULL, 1, '2020-01-29 19:28:42', '2020-02-03 18:12:42', NULL);
INSERT INTO public.agenda VALUES (172, 2, 47, '2020-02-03', '17:00', '17:30', '5', NULL, 1, '2020-01-30 16:32:59', '2020-02-04 18:28:02', NULL);
INSERT INTO public.agenda VALUES (170, 2, 60, '2020-02-03', '16:00', '16:30', '5', NULL, 1, '2020-01-29 19:29:46', '2020-02-03 19:20:38', NULL);
INSERT INTO public.agenda VALUES (175, 2, 5, '2020-02-05', '11:00', '11:30', '5', NULL, 1, '2020-01-30 16:35:06', '2020-02-05 16:50:22', NULL);
INSERT INTO public.agenda VALUES (174, 2, 22, '2020-02-05', '10:30', '11:00', '5', NULL, 1, '2020-01-30 16:34:32', '2020-02-05 13:54:15', NULL);
INSERT INTO public.agenda VALUES (176, 2, 16, '2020-02-05', '13:15', '14:00', '5', NULL, 1, '2020-01-30 16:35:58', '2020-02-05 16:59:43', NULL);
INSERT INTO public.agenda VALUES (177, 2, 18, '2020-02-05', '14:30', '15:00', '5', NULL, 1, '2020-01-30 16:36:35', '2020-02-05 17:49:46', NULL);
INSERT INTO public.agenda VALUES (178, 2, 30, '2020-02-05', '15:00', '15:30', '5', NULL, 1, '2020-01-30 16:37:11', '2020-02-05 18:14:49', NULL);
INSERT INTO public.agenda VALUES (182, 2, 49, '2020-02-06', '14:00', '14:30', NULL, NULL, 1, '2020-01-30 16:40:07', '2020-01-30 16:40:07', NULL);
INSERT INTO public.agenda VALUES (157, 2, 54, '2020-01-30', '14:30', '15:30', '5', NULL, 1, '2020-01-29 14:23:18', '2020-01-30 17:52:17', NULL);
INSERT INTO public.agenda VALUES (148, 2, 17, '2020-01-31', '14:00', '15:00', '5', NULL, 1, '2020-01-27 16:42:15', '2020-01-31 17:44:01', NULL);
INSERT INTO public.agenda VALUES (166, 2, 43, '2020-02-03', '13:15', '14:00', '5', NULL, 1, '2020-01-29 19:27:30', '2020-02-03 17:04:39', NULL);
INSERT INTO public.agenda VALUES (186, 2, 28, '2020-02-06', '15:00', '15:30', NULL, NULL, 1, '2020-02-03 19:27:49', '2020-02-03 19:27:49', NULL);
INSERT INTO public.agenda VALUES (190, 2, 65, '2020-02-04', '15:00', '15:30', '2', NULL, 1, '2020-02-03 20:50:54', '2020-02-03 20:51:18', NULL);
INSERT INTO public.agenda VALUES (189, 2, 65, '2020-02-04', '15:00', '15:30', '5', NULL, 1, '2020-02-03 20:50:53', '2020-02-04 18:29:46', NULL);
INSERT INTO public.agenda VALUES (185, 2, 63, '2020-02-04', '16:30', '17:30', '5', NULL, 1, '2020-02-03 18:37:02', '2020-02-05 13:51:21', NULL);
INSERT INTO public.agenda VALUES (173, 2, 35, '2020-02-05', '10:00', '10:30', '5', NULL, 1, '2020-01-30 16:34:02', '2020-02-05 13:52:45', NULL);
INSERT INTO public.agenda VALUES (184, 2, 62, '2020-02-04', '16:00', '16:30', '5', NULL, 1, '2020-02-03 18:36:14', '2020-02-04 19:01:25', NULL);
INSERT INTO public.agenda VALUES (191, 2, 24, '2020-02-05', '11:30', '12:00', '2', NULL, 1, '2020-02-05 13:54:43', '2020-02-05 16:50:35', NULL);
INSERT INTO public.agenda VALUES (187, 2, 62, '2020-02-06', '15:30', '16:00', '3', NULL, 1, '2020-02-03 19:28:23', '2020-02-06 18:12:59', NULL);
INSERT INTO public.agenda VALUES (193, 2, 54, '2020-02-06', '16:00', '16:30', '3', NULL, 1, '2020-02-05 13:56:53', '2020-02-06 18:57:11', NULL);
INSERT INTO public.agenda VALUES (188, 2, 39, '2020-02-05', '14:00', '14:30', '5', NULL, 1, '2020-02-03 19:47:28', '2020-02-05 17:48:32', NULL);
INSERT INTO public.agenda VALUES (202, 2, 41, '2020-02-07', '17:00', '17:30', NULL, NULL, 1, '2020-02-05 17:55:23', '2020-02-05 17:55:23', NULL);
INSERT INTO public.agenda VALUES (194, 2, 66, '2020-02-06', '16:30', '17:00', '3', NULL, 1, '2020-02-05 13:58:28', '2020-02-06 19:35:24', NULL);
INSERT INTO public.agenda VALUES (179, 2, 52, '2020-02-05', '15:30', '16:00', '5', NULL, 1, '2020-01-30 16:37:51', '2020-02-05 18:50:31', NULL);
INSERT INTO public.agenda VALUES (180, 2, 28, '2020-02-05', '16:00', '16:30', '2', NULL, 1, '2020-01-30 16:38:39', '2020-02-05 19:13:43', NULL);
INSERT INTO public.agenda VALUES (181, 2, 48, '2020-02-05', '16:30', '17:00', '5', NULL, 1, '2020-01-30 16:39:13', '2020-02-05 19:42:26', NULL);
INSERT INTO public.agenda VALUES (198, 2, 17, '2020-02-07', '14:00', '15:00', '3', NULL, 1, '2020-02-05 17:52:04', '2020-02-07 16:56:47', NULL);
INSERT INTO public.agenda VALUES (197, 2, 59, '2020-02-05', '17:00', '17:30', '5', NULL, 1, '2020-02-05 17:50:49', '2020-02-05 20:35:22', NULL);
INSERT INTO public.agenda VALUES (196, 2, 50, '2020-02-07', '13:30', '14:00', '5', NULL, 1, '2020-02-05 13:59:52', '2020-02-07 17:01:36', NULL);
INSERT INTO public.agenda VALUES (192, 2, 63, '2020-02-07', '10:00', '10:30', '5', NULL, 1, '2020-02-05 13:56:02', '2020-02-07 17:02:09', NULL);
INSERT INTO public.agenda VALUES (195, 2, 53, '2020-02-07', '13:00', '13:30', '5', NULL, 1, '2020-02-05 13:59:11', '2020-02-07 17:03:04', NULL);
INSERT INTO public.agenda VALUES (214, 2, 69, '2020-02-11', '15:30', '16:30', NULL, NULL, 1, '2020-02-10 14:05:35', '2020-02-10 14:05:35', NULL);
INSERT INTO public.agenda VALUES (204, 2, 23, '2020-02-06', '13:15', '14:00', '5', NULL, 1, '2020-02-05 19:56:05', '2020-02-07 17:03:23', NULL);
INSERT INTO public.agenda VALUES (183, 2, 51, '2020-02-06', '14:30', '15:00', '4', NULL, 1, '2020-01-30 16:40:44', '2020-02-07 17:03:34', NULL);
INSERT INTO public.agenda VALUES (199, 2, 61, '2020-02-07', '15:00', '15:30', '3', NULL, 1, '2020-02-05 17:52:56', '2020-02-07 18:05:40', NULL);
INSERT INTO public.agenda VALUES (200, 2, 18, '2020-02-07', '15:30', '16:00', '3', NULL, 1, '2020-02-05 17:53:47', '2020-02-07 18:27:02', NULL);
INSERT INTO public.agenda VALUES (203, 2, 67, '2020-02-07', '16:00', '16:30', '3', NULL, 1, '2020-02-05 19:15:53', '2020-02-07 18:46:14', NULL);
INSERT INTO public.agenda VALUES (201, 2, 47, '2020-02-07', '16:30', '17:00', '3', NULL, 1, '2020-02-05 17:54:36', '2020-02-07 19:22:31', NULL);
INSERT INTO public.agenda VALUES (221, 2, 28, '2020-02-12', '14:00', '14:30', '2', NULL, 1, '2020-02-10 14:13:36', '2020-02-10 14:13:48', NULL);
INSERT INTO public.agenda VALUES (205, 2, 4, '2020-02-10', '10:00', '11:00', '5', NULL, 1, '2020-02-10 13:45:02', '2020-02-10 16:44:49', NULL);
INSERT INTO public.agenda VALUES (206, 2, 5, '2020-02-10', '11:30', '12:00', '5', NULL, 1, '2020-02-10 13:45:32', '2020-02-10 16:45:10', NULL);
INSERT INTO public.agenda VALUES (207, 2, 39, '2020-02-10', '13:15', '14:00', '5', NULL, 1, '2020-02-10 13:46:17', '2020-02-10 16:45:52', NULL);
INSERT INTO public.agenda VALUES (211, 2, 46, '2020-02-10', '16:30', '17:00', '5', NULL, 1, '2020-02-10 13:49:14', '2020-02-11 18:30:16', NULL);
INSERT INTO public.agenda VALUES (241, 2, 73, '2020-02-24', '09:00', '09:30', NULL, NULL, 1, '2020-02-10 18:10:12', '2020-02-10 18:10:12', NULL);
INSERT INTO public.agenda VALUES (242, 2, 74, '2020-02-25', '09:00', '09:30', NULL, NULL, 1, '2020-02-10 18:11:11', '2020-02-10 18:11:11', NULL);
INSERT INTO public.agenda VALUES (217, 2, 50, '2020-02-12', '10:30', '11:00', '5', NULL, 1, '2020-02-10 14:09:07', '2020-02-12 13:50:23', NULL);
INSERT INTO public.agenda VALUES (208, 2, 44, '2020-02-10', '15:00', '15:30', '5', NULL, 1, '2020-02-10 13:46:58', '2020-02-10 18:12:54', NULL);
INSERT INTO public.agenda VALUES (209, 2, 11, '2020-02-10', '15:30', '16:00', '2', NULL, 1, '2020-02-10 13:47:53', '2020-02-10 18:13:07', NULL);
INSERT INTO public.agenda VALUES (243, 2, 75, '2020-02-26', '08:00', '18:00', '2', NULL, 1, '2020-02-10 18:12:58', '2020-02-10 18:14:19', NULL);
INSERT INTO public.agenda VALUES (244, 2, 76, '2020-02-26', '08:00', '13:00', NULL, NULL, 1, '2020-02-10 18:16:58', '2020-02-10 18:16:58', NULL);
INSERT INTO public.agenda VALUES (212, 2, 47, '2020-02-10', '17:00', '17:30', '5', NULL, 1, '2020-02-10 13:49:50', '2020-02-11 18:32:02', NULL);
INSERT INTO public.agenda VALUES (219, 2, 24, '2020-02-12', '11:30', '12:00', '5', NULL, 1, '2020-02-10 14:10:25', '2020-02-12 19:16:38', NULL);
INSERT INTO public.agenda VALUES (235, 2, 17, '2020-02-14', '14:00', '15:00', '2', NULL, 1, '2020-02-10 16:40:39', '2020-02-11 18:04:14', NULL);
INSERT INTO public.agenda VALUES (218, 2, 5, '2020-02-12', '11:00', '11:30', '5', NULL, 1, '2020-02-10 14:09:44', '2020-02-12 14:14:15', NULL);
INSERT INTO public.agenda VALUES (245, 2, 78, '2020-02-11', '14:30', '15:00', '5', NULL, 1, '2020-02-10 19:38:28', '2020-02-11 18:07:15', NULL);
INSERT INTO public.agenda VALUES (215, 2, 70, '2020-02-11', '16:30', '17:30', '5', NULL, 1, '2020-02-10 14:07:23', '2020-02-11 19:51:38', NULL);
INSERT INTO public.agenda VALUES (213, 2, 68, '2020-02-11', '15:00', '15:30', '5', NULL, 1, '2020-02-10 14:04:08', '2020-02-11 18:25:38', NULL);
INSERT INTO public.agenda VALUES (222, 2, 18, '2020-02-12', '14:30', '15:00', '3', NULL, 1, '2020-02-10 14:14:29', '2020-02-12 19:21:49', NULL);
INSERT INTO public.agenda VALUES (210, 2, 51, '2020-02-10', '16:00', '16:30', '5', NULL, 1, '2020-02-10 13:48:34', '2020-02-11 18:29:32', NULL);
INSERT INTO public.agenda VALUES (246, 2, 22, '2020-02-12', '09:30', '10:00', '5', NULL, 1, '2020-02-11 18:07:41', '2020-02-12 13:48:44', NULL);
INSERT INTO public.agenda VALUES (220, 2, 59, '2020-02-12', '13:15', '14:00', '2', NULL, 1, '2020-02-10 14:11:16', '2020-02-12 16:01:14', NULL);
INSERT INTO public.agenda VALUES (216, 2, 35, '2020-02-12', '10:00', '10:30', '5', NULL, 1, '2020-02-10 14:08:33', '2020-02-12 13:49:16', NULL);
INSERT INTO public.agenda VALUES (223, 2, 30, '2020-02-12', '15:00', '15:30', '3', NULL, 1, '2020-02-10 14:15:08', '2020-02-12 19:22:06', NULL);
INSERT INTO public.agenda VALUES (224, 2, 52, '2020-02-12', '15:30', '16:00', '3', NULL, 1, '2020-02-10 14:15:48', '2020-02-12 19:22:15', NULL);
INSERT INTO public.agenda VALUES (225, 2, 48, '2020-02-12', '16:00', '16:30', '5', NULL, 1, '2020-02-10 14:16:24', '2020-02-12 19:16:21', NULL);
INSERT INTO public.agenda VALUES (228, 2, 53, '2020-02-13', '13:15', '14:00', '3', NULL, 1, '2020-02-10 14:21:08', '2020-02-13 16:14:00', NULL);
INSERT INTO public.agenda VALUES (226, 2, 62, '2020-02-12', '16:30', '17:00', '3', NULL, 1, '2020-02-10 14:17:07', '2020-02-12 19:28:03', NULL);
INSERT INTO public.agenda VALUES (227, 2, 54, '2020-02-12', '17:00', '17:30', '3', NULL, 1, '2020-02-10 14:17:48', '2020-02-12 19:50:31', NULL);
INSERT INTO public.agenda VALUES (247, 2, 17, '2020-02-13', '14:30', '15:30', '5', NULL, 1, '2020-02-11 18:08:17', '2020-02-13 18:19:38', NULL);
INSERT INTO public.agenda VALUES (230, 2, 54, '2020-02-13', '15:30', '16:00', '3', NULL, 1, '2020-02-10 14:22:56', '2020-02-13 18:35:52', NULL);
INSERT INTO public.agenda VALUES (231, 2, 62, '2020-02-13', '16:00', '16:30', '3', NULL, 1, '2020-02-10 16:35:49', '2020-02-13 18:36:03', NULL);
INSERT INTO public.agenda VALUES (229, 2, 64, '2020-02-13', '14:00', '14:30', '5', NULL, 1, '2020-02-10 14:21:52', '2020-02-13 17:23:13', NULL);
INSERT INTO public.agenda VALUES (232, 2, 71, '2020-02-13', '16:30', '17:00', '3', NULL, 1, '2020-02-10 16:38:00', '2020-02-13 19:33:54', NULL);
INSERT INTO public.agenda VALUES (233, 2, 63, '2020-02-14', '10:00', '11:00', '2', NULL, 1, '2020-02-10 16:39:06', '2020-02-14 13:47:47', NULL);
INSERT INTO public.agenda VALUES (234, 2, 16, '2020-02-14', '13:15', '14:00', '2', NULL, 1, '2020-02-10 16:39:58', '2020-02-13 20:04:16', NULL);
INSERT INTO public.agenda VALUES (248, 2, 16, '2020-02-13', '17:00', '17:30', '3', NULL, 1, '2020-02-13 20:04:38', '2020-02-13 20:04:52', NULL);
INSERT INTO public.agenda VALUES (238, 2, 72, '2020-02-14', '16:00', '16:30', '5', NULL, 1, '2020-02-10 16:43:29', '2020-02-14 19:26:42', NULL);
INSERT INTO public.agenda VALUES (237, 2, 18, '2020-02-14', '15:30', '16:00', '5', NULL, 1, '2020-02-10 16:41:39', '2020-02-14 20:17:29', NULL);
INSERT INTO public.agenda VALUES (239, 2, 47, '2020-02-14', '16:30', '17:00', '5', NULL, 1, '2020-02-10 16:43:59', '2020-02-14 20:15:26', NULL);
INSERT INTO public.agenda VALUES (240, 2, 41, '2020-02-14', '17:00', '17:45', '2', NULL, 1, '2020-02-10 16:44:30', '2020-02-14 20:15:37', NULL);
INSERT INTO public.agenda VALUES (236, 2, 61, '2020-02-14', '15:00', '15:30', '5', NULL, 1, '2020-02-10 16:41:12', '2020-02-14 20:17:10', NULL);
INSERT INTO public.agenda VALUES (261, 2, 77, '2020-02-18', '14:30', '15:00', '5', NULL, 1, '2020-02-17 13:44:46', '2020-02-18 18:49:53', NULL);
INSERT INTO public.agenda VALUES (256, 2, 11, '2020-02-17', '15:30', '16:00', '2', NULL, 1, '2020-02-17 13:42:01', '2020-02-17 13:42:11', NULL);
INSERT INTO public.agenda VALUES (257, 2, 30, '2020-02-17', '16:00', '16:30', NULL, NULL, 1, '2020-02-17 13:42:40', '2020-02-17 13:42:40', NULL);
INSERT INTO public.agenda VALUES (258, 2, 46, '2020-02-17', '16:30', '17:00', NULL, NULL, 1, '2020-02-17 13:43:08', '2020-02-17 13:43:08', NULL);
INSERT INTO public.agenda VALUES (259, 2, 47, '2020-02-17', '17:00', '17:30', NULL, NULL, 1, '2020-02-17 13:43:43', '2020-02-17 13:43:43', NULL);
INSERT INTO public.agenda VALUES (347, 2, 93, '2020-09-17', '09:00', '09:30', NULL, NULL, 1, '2020-09-17 12:51:46', '2020-09-17 12:51:46', NULL);
INSERT INTO public.agenda VALUES (262, 2, 51, '2020-02-18', '15:00', '15:30', '3', NULL, 1, '2020-02-17 13:45:14', '2020-02-18 18:25:59', NULL);
INSERT INTO public.agenda VALUES (254, 2, 3, '2020-02-17', '14:00', '15:00', '5', NULL, 1, '2020-02-17 13:40:08', '2020-02-17 18:05:37', NULL);
INSERT INTO public.agenda VALUES (255, 2, 44, '2020-02-17', '15:00', '15:30', '5', NULL, 1, '2020-02-17 13:40:41', '2020-02-17 18:06:31', NULL);
INSERT INTO public.agenda VALUES (250, 2, 4, '2020-02-17', '10:00', '11:00', '5', NULL, 1, '2020-02-17 13:38:00', '2020-02-17 18:06:48', NULL);
INSERT INTO public.agenda VALUES (252, 2, 5, '2020-02-17', '11:30', '12:00', '5', NULL, 1, '2020-02-17 13:38:52', '2020-02-17 18:07:59', NULL);
INSERT INTO public.agenda VALUES (285, 2, 68, '2020-02-20', '14:00', '14:30', '2', NULL, 1, '2020-02-18 14:24:33', '2020-02-20 17:04:00', NULL);
INSERT INTO public.agenda VALUES (263, 2, 54, '2020-02-18', '15:30', '16:00', NULL, NULL, 1, '2020-02-17 13:45:45', '2020-02-17 13:45:45', NULL);
INSERT INTO public.agenda VALUES (267, 2, 22, '2020-02-19', '09:30', '10:00', NULL, NULL, 1, '2020-02-17 14:28:33', '2020-02-17 14:28:33', NULL);
INSERT INTO public.agenda VALUES (271, 2, 24, '2020-02-19', '11:30', '12:00', NULL, NULL, 1, '2020-02-17 14:35:54', '2020-02-17 14:35:54', NULL);
INSERT INTO public.agenda VALUES (276, 2, 16, '2020-02-19', '17:00', '18:00', NULL, NULL, 1, '2020-02-17 14:41:41', '2020-02-17 14:41:41', NULL);
INSERT INTO public.agenda VALUES (277, 2, 53, '2020-02-20', '13:15', '14:00', NULL, NULL, 1, '2020-02-17 14:43:55', '2020-02-17 14:43:55', NULL);
INSERT INTO public.agenda VALUES (253, 2, 43, '2020-02-17', '13:15', '14:00', '2', NULL, 1, '2020-02-17 13:39:35', '2020-02-17 16:57:25', NULL);
INSERT INTO public.agenda VALUES (251, 2, 39, '2020-02-17', '11:00', '11:30', '5', NULL, 1, '2020-02-17 13:38:20', '2020-02-17 18:07:12', NULL);
INSERT INTO public.agenda VALUES (281, 2, 81, '2020-02-19', '13:00', '13:30', NULL, NULL, 1, '2020-02-17 18:09:50', '2020-02-17 18:09:50', NULL);
INSERT INTO public.agenda VALUES (282, 2, 81, '2020-02-19', '13:30', '14:00', NULL, NULL, 1, '2020-02-17 18:10:29', '2020-02-17 18:10:29', NULL);
INSERT INTO public.agenda VALUES (283, 2, 81, '2020-02-18', '10:00', '11:00', NULL, NULL, 1, '2020-02-17 18:11:48', '2020-02-17 18:11:48', NULL);
INSERT INTO public.agenda VALUES (284, 2, 81, '2020-02-20', '10:00', '11:00', NULL, NULL, 1, '2020-02-17 18:12:27', '2020-02-17 18:12:27', NULL);
INSERT INTO public.agenda VALUES (264, 2, 62, '2020-02-18', '16:00', '16:30', '2', NULL, 1, '2020-02-17 13:46:27', '2020-02-18 17:33:43', NULL);
INSERT INTO public.agenda VALUES (272, 2, 18, '2020-02-19', '14:30', '15:00', '2', NULL, 1, '2020-02-17 14:37:23', '2020-02-19 11:30:52', NULL);
INSERT INTO public.agenda VALUES (287, 2, 82, '2020-02-19', '16:00', '16:30', NULL, NULL, 1, '2020-02-18 18:46:27', '2020-02-18 18:46:27', NULL);
INSERT INTO public.agenda VALUES (290, 2, 17, '2020-02-20', '14:30', '15:30', '2', NULL, 1, '2020-02-18 20:26:33', '2020-02-19 12:07:21', NULL);
INSERT INTO public.agenda VALUES (249, 2, 7, '2020-02-18', '13:15', '14:00', '5', NULL, 1, '2020-02-14 19:14:03', '2020-02-18 18:48:44', NULL);
INSERT INTO public.agenda VALUES (280, 2, 71, '2020-02-20', '16:30', '17:00', '2', NULL, 1, '2020-02-17 14:49:29', '2020-02-20 16:43:21', NULL);
INSERT INTO public.agenda VALUES (260, 2, 64, '2020-02-18', '14:00', '14:30', '5', NULL, 1, '2020-02-17 13:44:18', '2020-02-18 18:49:22', NULL);
INSERT INTO public.agenda VALUES (289, 2, 81, '2020-02-21', '13:00', '13:30', NULL, NULL, 1, '2020-02-18 18:51:42', '2020-02-18 18:51:42', NULL);
INSERT INTO public.agenda VALUES (265, 2, 70, '2020-02-18', '16:30', '17:15', '5', NULL, 1, '2020-02-17 13:46:59', '2020-02-18 20:20:47', NULL);
INSERT INTO public.agenda VALUES (286, 2, 68, '2020-02-19', '14:00', '14:30', '2', NULL, 1, '2020-02-18 14:26:13', '2020-02-19 14:14:40', NULL);
INSERT INTO public.agenda VALUES (266, 2, 79, '2020-02-18', '17:00', '17:30', '5', NULL, 1, '2020-02-17 13:47:57', '2020-02-18 20:34:48', NULL);
INSERT INTO public.agenda VALUES (268, 2, 35, '2020-02-19', '10:00', '10:30', '5', NULL, 1, '2020-02-17 14:29:02', '2020-02-19 14:20:55', NULL);
INSERT INTO public.agenda VALUES (269, 2, 80, '2020-02-19', '10:30', '11:00', '2', NULL, 1, '2020-02-17 14:29:52', '2020-02-19 14:21:08', NULL);
INSERT INTO public.agenda VALUES (295, 2, 53, '2020-02-27', '13:15', '14:00', '2', NULL, 1, '2020-02-20 19:51:28', '2020-02-27 17:07:27', NULL);
INSERT INTO public.agenda VALUES (270, 2, 5, '2020-02-19', '11:00', '11:30', '5', NULL, 1, '2020-02-17 14:30:21', '2020-02-19 14:22:20', NULL);
INSERT INTO public.agenda VALUES (279, 2, 62, '2020-02-20', '16:00', '16:30', '5', NULL, 1, '2020-02-17 14:48:10', '2020-02-20 19:31:19', NULL);
INSERT INTO public.agenda VALUES (293, 2, 23, '2020-02-19', '14:30', '15:00', '5', NULL, 1, '2020-02-19 17:46:32', '2020-02-19 17:47:10', NULL);
INSERT INTO public.agenda VALUES (273, 2, 48, '2020-02-19', '15:00', '15:30', '5', NULL, 1, '2020-02-17 14:38:37', '2020-02-19 18:04:47', NULL);
INSERT INTO public.agenda VALUES (292, 2, 83, '2020-02-20', '14:30', '15:00', '5', NULL, 1, '2020-02-19 17:06:00', '2020-02-20 19:34:49', NULL);
INSERT INTO public.agenda VALUES (274, 2, 52, '2020-02-19', '15:30', '16:00', '5', NULL, 1, '2020-02-17 14:39:28', '2020-02-19 18:41:52', NULL);
INSERT INTO public.agenda VALUES (275, 2, 71, '2020-02-19', '16:30', '17:00', '2', NULL, 1, '2020-02-17 14:40:23', '2020-02-19 19:38:33', NULL);
INSERT INTO public.agenda VALUES (278, 2, 54, '2020-02-20', '15:30', '16:00', '3', NULL, 1, '2020-02-17 14:47:02', '2020-02-20 19:36:31', NULL);
INSERT INTO public.agenda VALUES (288, 2, 47, '2020-02-20', '17:00', '17:30', '3', NULL, 1, '2020-02-18 18:50:55', '2020-02-20 20:02:06', NULL);
INSERT INTO public.agenda VALUES (294, 2, 31, '2020-02-20', '17:30', '18:00', '3', NULL, 1, '2020-02-19 18:29:54', '2020-02-20 20:19:42', NULL);
INSERT INTO public.agenda VALUES (306, 2, 39, '2020-03-09', '13:15', '14:00', NULL, NULL, 1, '2020-03-06 20:20:35', '2020-03-06 20:20:35', NULL);
INSERT INTO public.agenda VALUES (296, 2, 70, '2020-02-27', '14:00', '15:00', '5', NULL, 1, '2020-02-20 19:52:16', '2020-02-27 17:54:24', NULL);
INSERT INTO public.agenda VALUES (297, 2, 64, '2020-02-27', '15:00', '15:30', '5', NULL, 1, '2020-02-20 19:54:09', '2020-02-27 18:26:06', NULL);
INSERT INTO public.agenda VALUES (298, 2, 54, '2020-02-27', '15:30', '16:00', '5', NULL, 1, '2020-02-20 19:56:02', '2020-02-27 19:06:44', NULL);
INSERT INTO public.agenda VALUES (299, 2, 62, '2020-02-27', '16:00', '16:30', '5', NULL, 1, '2020-02-20 19:56:43', '2020-02-27 19:44:08', NULL);
INSERT INTO public.agenda VALUES (314, 2, 23, '2020-03-10', '14:30', '15:00', NULL, NULL, 1, '2020-03-06 20:30:35', '2020-03-06 20:30:35', NULL);
INSERT INTO public.agenda VALUES (302, 2, 84, '2020-02-27', '17:30', '18:00', '3', NULL, 1, '2020-02-27 17:55:27', '2020-02-27 20:18:05', NULL);
INSERT INTO public.agenda VALUES (300, 2, 71, '2020-02-27', '16:30', '17:00', '5', NULL, 1, '2020-02-20 19:57:43', '2020-02-27 20:36:37', NULL);
INSERT INTO public.agenda VALUES (301, 2, 79, '2020-02-27', '17:00', '17:30', '4', NULL, 1, '2020-02-20 19:58:30', '2020-02-27 20:38:04', NULL);
INSERT INTO public.agenda VALUES (316, 2, 88, '2020-03-10', '15:30', '16:00', NULL, NULL, 1, '2020-03-06 20:33:37', '2020-03-06 20:33:37', NULL);
INSERT INTO public.agenda VALUES (317, 2, 62, '2020-03-10', '16:00', '16:30', NULL, NULL, 1, '2020-03-06 20:34:11', '2020-03-06 20:34:11', NULL);
INSERT INTO public.agenda VALUES (307, 2, 85, '2020-03-09', '14:00', '14:30', '2', NULL, 1, '2020-03-06 20:22:21', '2020-03-09 12:23:34', NULL);
INSERT INTO public.agenda VALUES (303, 2, 4, '2020-03-09', '10:00', '11:00', '1', NULL, 1, '2020-03-06 20:18:43', '2020-03-09 13:05:37', NULL);
INSERT INTO public.agenda VALUES (304, 2, 14, '2020-03-09', '11:00', '11:30', '1', NULL, 1, '2020-03-06 20:19:13', '2020-03-09 13:45:46', NULL);
INSERT INTO public.agenda VALUES (309, 2, 44, '2020-03-09', '15:00', '15:30', '2', NULL, 1, '2020-03-06 20:26:19', '2020-03-09 13:46:01', NULL);
INSERT INTO public.agenda VALUES (320, 2, 44, '2020-03-16', '15:00', '15:30', NULL, NULL, 1, '2020-03-09 13:48:41', '2020-03-09 13:48:41', NULL);
INSERT INTO public.agenda VALUES (328, 2, 7, '2020-03-11', '13:15', '14:00', NULL, NULL, 1, '2020-03-09 19:17:33', '2020-03-09 19:17:33', NULL);
INSERT INTO public.agenda VALUES (329, 2, 85, '2020-03-11', '14:00', '14:30', NULL, NULL, 1, '2020-03-09 19:18:14', '2020-03-09 19:18:14', NULL);
INSERT INTO public.agenda VALUES (308, 2, 51, '2020-03-09', '14:30', '15:00', '5', NULL, 1, '2020-03-06 20:24:19', '2020-03-09 18:06:40', NULL);
INSERT INTO public.agenda VALUES (305, 2, 5, '2020-03-09', '11:30', '12:00', '5', NULL, 1, '2020-03-06 20:19:50', '2020-03-09 18:07:40', NULL);
INSERT INTO public.agenda VALUES (310, 2, 86, '2020-03-09', '15:30', '16:00', '5', NULL, 1, '2020-03-06 20:27:54', '2020-03-09 19:04:39', NULL);
INSERT INTO public.agenda VALUES (335, 2, 79, '2020-03-11', '17:00', '17:30', NULL, NULL, 1, '2020-03-09 19:21:35', '2020-03-09 19:21:35', NULL);
INSERT INTO public.agenda VALUES (311, 2, 30, '2020-03-09', '16:00', '16:30', '5', NULL, 1, '2020-03-06 20:28:31', '2020-03-09 19:10:25', NULL);
INSERT INTO public.agenda VALUES (340, 2, 71, '2020-03-12', '16:30', '17:00', NULL, NULL, 1, '2020-03-09 19:25:29', '2020-03-09 19:25:29', NULL);
INSERT INTO public.agenda VALUES (346, 2, 47, '2020-03-13', '17:00', '17:30', NULL, NULL, 1, '2020-03-09 19:30:21', '2020-03-09 19:30:21', NULL);
INSERT INTO public.agenda VALUES (313, 2, 47, '2020-03-09', '17:00', '17:30', '1', NULL, 1, '2020-03-06 20:29:44', '2020-03-09 20:21:16', NULL);
INSERT INTO public.agenda VALUES (312, 2, 46, '2020-03-09', '16:30', '17:00', '1', NULL, 1, '2020-03-06 20:29:05', '2020-03-09 19:31:21', NULL);
INSERT INTO public.agenda VALUES (315, 2, 87, '2020-03-10', '15:00', '15:30', '5', NULL, 1, '2020-03-06 20:32:33', '2020-03-10 20:29:53', NULL);
INSERT INTO public.agenda VALUES (323, 2, 22, '2020-03-11', '09:30', '10:00', '3', NULL, 1, '2020-03-09 19:13:48', '2020-03-11 12:35:17', NULL);
INSERT INTO public.agenda VALUES (321, 2, 89, '2020-03-10', '16:30', '17:00', '5', NULL, 1, '2020-03-09 19:12:17', '2020-03-10 19:57:44', NULL);
INSERT INTO public.agenda VALUES (322, 2, 16, '2020-03-10', '17:00', '18:00', '5', NULL, 1, '2020-03-09 19:13:11', '2020-03-10 20:19:43', NULL);
INSERT INTO public.agenda VALUES (324, 2, 35, '2020-03-11', '10:00', '10:30', '3', NULL, 1, '2020-03-09 19:14:14', '2020-03-11 13:03:41', NULL);
INSERT INTO public.agenda VALUES (325, 2, 80, '2020-03-11', '10:30', '11:00', '3', NULL, 1, '2020-03-09 19:14:41', '2020-03-11 13:23:44', NULL);
INSERT INTO public.agenda VALUES (326, 2, 5, '2020-03-11', '11:00', '11:30', '3', NULL, 1, '2020-03-09 19:15:15', '2020-03-11 13:55:54', NULL);
INSERT INTO public.agenda VALUES (327, 2, 24, '2020-03-11', '11:30', '12:00', '3', NULL, 1, '2020-03-09 19:15:49', '2020-03-11 14:35:37', NULL);
INSERT INTO public.agenda VALUES (330, 2, 18, '2020-03-11', '14:30', '15:00', '3', NULL, 1, '2020-03-09 19:18:44', '2020-03-11 17:28:31', NULL);
INSERT INTO public.agenda VALUES (334, 2, 71, '2020-03-11', '16:30', '17:00', '1', NULL, 1, '2020-03-09 19:20:55', '2020-03-11 18:08:13', NULL);
INSERT INTO public.agenda VALUES (331, 2, 64, '2020-03-11', '15:00', '15:30', '3', NULL, 1, '2020-03-09 19:19:17', '2020-03-11 17:59:05', NULL);
INSERT INTO public.agenda VALUES (332, 2, 52, '2020-03-11', '15:30', '16:00', '3', NULL, 1, '2020-03-09 19:19:47', '2020-03-11 18:21:31', NULL);
INSERT INTO public.agenda VALUES (333, 2, 86, '2020-03-11', '16:00', '16:30', '3', NULL, 1, '2020-03-09 19:20:19', '2020-03-11 19:03:24', NULL);
INSERT INTO public.agenda VALUES (336, 2, 53, '2020-03-12', '13:15', '14:00', '1', NULL, 1, '2020-03-09 19:22:25', '2020-03-12 16:47:39', NULL);
INSERT INTO public.agenda VALUES (338, 2, 70, '2020-03-12', '14:00', '15:00', '1', NULL, 1, '2020-03-09 19:24:03', '2020-03-12 16:47:52', NULL);
INSERT INTO public.agenda VALUES (337, 2, 87, '2020-03-12', '15:00', '15:30', '1', NULL, 1, '2020-03-09 19:23:24', '2020-03-12 18:06:12', NULL);
INSERT INTO public.agenda VALUES (339, 2, 54, '2020-03-12', '15:30', '16:00', '1', NULL, 1, '2020-03-09 19:24:44', '2020-03-12 18:30:31', NULL);
INSERT INTO public.agenda VALUES (341, 2, 92, '2020-03-12', '17:00', '17:30', '2', NULL, 1, '2020-03-09 19:27:06', '2020-03-13 17:15:17', NULL);
INSERT INTO public.agenda VALUES (342, 2, 17, '2020-03-13', '14:00', '15:00', '1', NULL, 1, '2020-03-09 19:28:09', '2020-03-13 18:06:37', NULL);
INSERT INTO public.agenda VALUES (343, 2, 61, '2020-03-13', '15:00', '15:30', '1', NULL, 1, '2020-03-09 19:28:41', '2020-03-13 18:06:44', NULL);
INSERT INTO public.agenda VALUES (344, 2, 18, '2020-03-13', '15:30', '16:00', '1', NULL, 1, '2020-03-09 19:29:11', '2020-03-13 18:22:28', NULL);
INSERT INTO public.agenda VALUES (345, 2, 3, '2020-03-13', '16:00', '17:00', '1', NULL, 1, '2020-03-09 19:29:45', '2020-03-13 19:43:04', NULL);
INSERT INTO public.agenda VALUES (318, 2, 85, '2020-03-16', '14:00', '14:30', '2', NULL, 1, '2020-03-09 12:24:22', '2020-03-16 11:07:11', NULL);
INSERT INTO public.agenda VALUES (319, 2, 85, '2020-03-18', '14:00', '14:30', '2', NULL, 1, '2020-03-09 12:25:01', '2020-03-16 11:07:20', NULL);
INSERT INTO public.agenda VALUES (348, 2, 94, '2020-09-17', '09:30', '10:00', NULL, NULL, 1, '2020-09-17 12:52:47', '2020-09-17 12:52:47', NULL);
INSERT INTO public.agenda VALUES (349, 2, 7, '2020-09-17', '10:00', '11:00', NULL, NULL, 1, '2020-09-17 12:53:26', '2020-09-17 12:53:26', NULL);
INSERT INTO public.agenda VALUES (350, 2, 95, '2020-09-17', '11:00', '11:30', NULL, NULL, 1, '2020-09-17 12:54:30', '2020-09-17 12:54:30', NULL);
INSERT INTO public.agenda VALUES (351, 2, 96, '2020-09-17', '11:30', '12:00', NULL, NULL, 1, '2020-09-17 12:55:20', '2020-09-17 12:55:20', NULL);


--
-- Name: agenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.agenda_id_seq', 351, true);


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.clientes VALUES (1, 'Cliente de Teste', '000.000.000-00', 'clienteteste@email.com', '(00)00000-0000', '2019-12-09', 'M', 1, 13631094, 'Rua 1 de Maio - Centro - Pirassununga - SP', '9887', '2019-09-12 18:13:44', '2019-09-12 18:13:44', NULL);
INSERT INTO public.clientes VALUES (2, 'usuario de teste', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2019-10-24 14:57:19', '2019-10-24 14:57:25', '2019-10-24 14:57:25');
INSERT INTO public.clientes VALUES (3, 'Débora', NULL, NULL, '19991908492', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 16:59:03', '2019-10-28 16:59:03', NULL);
INSERT INTO public.clientes VALUES (4, 'Ana Luiza', NULL, NULL, '19 99784-6278/3562-1232', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:08:06', '2019-10-28 17:09:26', NULL);
INSERT INTO public.clientes VALUES (6, 'Sarita', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:09:41', '2019-10-28 17:09:41', NULL);
INSERT INTO public.clientes VALUES (8, 'Eliana', NULL, NULL, '1999694-2577', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:10:26', '2019-10-28 17:10:26', NULL);
INSERT INTO public.clientes VALUES (9, 'Iracema', NULL, NULL, '1999625-5191', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:11:13', '2019-10-28 17:11:13', NULL);
INSERT INTO public.clientes VALUES (5, 'Silvana Maria Moreno Mechila', NULL, NULL, '1998872-2013/3561-2013', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:08:55', '2019-10-28 17:11:58', NULL);
INSERT INTO public.clientes VALUES (10, 'Valdemir', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2019-10-28 17:44:23', '2019-10-28 17:44:23', NULL);
INSERT INTO public.clientes VALUES (12, 'Iandara', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:45:08', '2019-10-28 17:45:08', NULL);
INSERT INTO public.clientes VALUES (13, 'Francieli Soares Correa', NULL, NULL, '1997133-4497', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:46:32', '2019-10-28 17:46:32', NULL);
INSERT INTO public.clientes VALUES (14, 'Neide Cinat', NULL, NULL, '193561-2980', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:47:14', '2019-10-28 17:47:14', NULL);
INSERT INTO public.clientes VALUES (15, 'Sergio Ricardo Campos', NULL, NULL, '1997145-9291', NULL, 'M', 1, NULL, NULL, NULL, '2019-10-28 17:47:52', '2019-10-28 17:47:52', NULL);
INSERT INTO public.clientes VALUES (16, 'Tricia', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:48:07', '2019-10-28 17:48:07', NULL);
INSERT INTO public.clientes VALUES (17, 'Dra Maria Luiza', NULL, NULL, '193561-7501', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:50:02', '2019-10-28 17:50:02', NULL);
INSERT INTO public.clientes VALUES (18, 'Sr.Siqueira', NULL, NULL, '19-3561-5521', NULL, 'M', 1, NULL, NULL, NULL, '2019-10-28 17:50:49', '2019-10-28 17:50:49', NULL);
INSERT INTO public.clientes VALUES (19, 'Maria do Carmo', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:51:24', '2019-10-28 17:51:24', NULL);
INSERT INTO public.clientes VALUES (20, 'Gabriela Suhet Pereira', NULL, NULL, '1999285-1516//3561-7807', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:52:56', '2019-10-28 17:52:56', NULL);
INSERT INTO public.clientes VALUES (21, 'Maisa  Maganha Tucumantel', NULL, NULL, '193561-4096', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:54:48', '2019-10-28 17:54:48', NULL);
INSERT INTO public.clientes VALUES (22, 'Silvia Valle de Oliveira', NULL, NULL, '19-99791-7602-//3562-7574', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:55:26', '2019-10-28 17:55:26', NULL);
INSERT INTO public.clientes VALUES (24, 'Renata', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:56:20', '2019-10-28 17:56:20', NULL);
INSERT INTO public.clientes VALUES (26, 'Sônia', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:56:56', '2019-10-28 17:56:56', NULL);
INSERT INTO public.clientes VALUES (29, 'Valdemir', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2019-10-28 18:03:26', '2019-10-28 18:03:26', NULL);
INSERT INTO public.clientes VALUES (25, 'Maria do Carmo', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:56:40', '2019-10-28 18:28:08', '2019-10-28 18:28:08');
INSERT INTO public.clientes VALUES (31, 'Célia', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 18:30:34', '2019-10-28 18:30:34', NULL);
INSERT INTO public.clientes VALUES (32, 'Maria Aparecida', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 18:34:43', '2019-10-28 18:34:43', NULL);
INSERT INTO public.clientes VALUES (30, 'Maria José', NULL, NULL, '(19) 99299-4831//(19) 3561-1633', NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 18:22:14', '2019-10-28 19:05:02', NULL);
INSERT INTO public.clientes VALUES (33, 'Caio', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2019-11-01 13:23:44', '2019-11-01 13:23:44', NULL);
INSERT INTO public.clientes VALUES (34, 'Silvana (ADV)', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-01 14:14:21', '2019-11-01 14:14:21', NULL);
INSERT INTO public.clientes VALUES (27, 'Benedito', NULL, NULL, '3562-5316', NULL, 'M', 1, NULL, NULL, NULL, '2019-10-28 17:57:21', '2019-11-01 18:01:19', NULL);
INSERT INTO public.clientes VALUES (36, 'Eliane Haite', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-08 20:02:52', '2019-11-08 20:02:52', NULL);
INSERT INTO public.clientes VALUES (37, 'Paula', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-08 20:31:47', '2019-11-08 20:31:47', NULL);
INSERT INTO public.clientes VALUES (7, 'Paula M. Meyer', NULL, 'paulamarquesmeyer@yahoo.com.br', '1935612845  35630493', NULL, 'F', 1, 13635002, 'Rua Nilo Maialle - Jardim Elite - Pirassununga - SP', '157', '2019-10-28 17:10:02', '2019-11-11 16:58:04', NULL);
INSERT INTO public.clientes VALUES (11, 'Carlos Alexandre Celin', NULL, NULL, '996074918  997114761(a)', NULL, 'M', 1, 13631045, 'Avenida Newton Prado - Centro - Pirassununga - SP', '3525', '2019-10-28 17:44:45', '2019-11-11 18:57:10', NULL);
INSERT INTO public.clientes VALUES (38, 'joana', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-20 10:35:04', '2019-11-20 10:35:04', NULL);
INSERT INTO public.clientes VALUES (39, 'Manoela', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-20 10:36:15', '2019-11-20 10:36:15', NULL);
INSERT INTO public.clientes VALUES (40, 'Eliana C.', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-20 10:37:10', '2019-11-20 10:37:10', NULL);
INSERT INTO public.clientes VALUES (41, 'Juliana', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-11-20 10:42:14', '2019-11-20 10:42:14', NULL);
INSERT INTO public.clientes VALUES (23, 'Giulia', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2019-10-28 17:56:01', '2019-12-06 14:12:29', NULL);
INSERT INTO public.clientes VALUES (28, 'Maria Aparecida de Fatima Correa', NULL, NULL, '19996653729', NULL, 'F', 1, NULL, NULL, '212', '2019-10-28 17:59:47', '2019-12-13 19:15:25', NULL);
INSERT INTO public.clientes VALUES (35, 'Eliana Buchara', NULL, NULL, '99154-4981', NULL, 'F', 1, NULL, NULL, NULL, '2019-11-07 17:32:57', '2020-01-20 17:53:03', NULL);
INSERT INTO public.clientes VALUES (42, 'Benedito Augusto Silva', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-01-20 18:18:33', '2020-01-20 18:18:33', NULL);
INSERT INTO public.clientes VALUES (43, 'Helena', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 18:58:19', '2020-01-20 18:58:19', NULL);
INSERT INTO public.clientes VALUES (44, 'Maria Francisca', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 19:00:31', '2020-01-20 19:00:31', NULL);
INSERT INTO public.clientes VALUES (45, 'Nilso Kohl', NULL, NULL, '19996317023', NULL, 'M', 1, NULL, NULL, NULL, '2020-01-20 19:21:13', '2020-01-20 19:21:13', NULL);
INSERT INTO public.clientes VALUES (46, 'Lucilene Calixto', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 19:22:29', '2020-01-20 19:22:29', NULL);
INSERT INTO public.clientes VALUES (47, 'Paulo', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-01-20 19:24:07', '2020-01-20 19:24:07', NULL);
INSERT INTO public.clientes VALUES (48, 'Vilma Leitão', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:17:57', '2020-01-20 20:17:57', NULL);
INSERT INTO public.clientes VALUES (49, 'Thayna', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:19:39', '2020-01-20 20:19:39', NULL);
INSERT INTO public.clientes VALUES (50, 'Maria Isabel', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:20:47', '2020-01-20 20:20:47', NULL);
INSERT INTO public.clientes VALUES (51, 'Daniela Donderi', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:22:34', '2020-01-20 20:22:34', NULL);
INSERT INTO public.clientes VALUES (52, 'Maria Augusta Maruci', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:24:47', '2020-01-20 20:24:47', NULL);
INSERT INTO public.clientes VALUES (53, 'Lucimara Baldin', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:26:25', '2020-01-20 20:26:25', NULL);
INSERT INTO public.clientes VALUES (54, 'Daniela Sardinha', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:29:41', '2020-01-20 20:29:41', NULL);
INSERT INTO public.clientes VALUES (55, 'Daniela Sardinha', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:45:39', '2020-01-20 20:45:39', NULL);
INSERT INTO public.clientes VALUES (56, 'Juliana', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-20 20:49:44', '2020-01-20 20:49:44', NULL);
INSERT INTO public.clientes VALUES (57, 'Maria de Lourdes', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-22 17:20:58', '2020-01-22 17:20:58', NULL);
INSERT INTO public.clientes VALUES (58, 'Nilso Kohl', NULL, NULL, '19 996317023', NULL, 'M', 1, NULL, NULL, NULL, '2020-01-24 17:57:50', '2020-01-24 17:57:50', NULL);
INSERT INTO public.clientes VALUES (59, 'Agnes', NULL, NULL, '(12)98255-0872', NULL, 'F', 1, NULL, NULL, NULL, '2020-01-27 13:29:46', '2020-01-27 13:29:46', NULL);
INSERT INTO public.clientes VALUES (60, 'Márcia Aparecida Marostegan Silva', NULL, NULL, '(19)99817-6463', NULL, 'F', 1, NULL, NULL, NULL, '2020-01-27 13:49:45', '2020-01-27 13:49:45', NULL);
INSERT INTO public.clientes VALUES (61, 'Rachel Hypolito', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-27 16:42:47', '2020-01-27 16:42:47', NULL);
INSERT INTO public.clientes VALUES (62, 'Expedita Androsio', NULL, NULL, '35854825', NULL, 'F', 1, NULL, NULL, NULL, '2020-01-29 14:25:39', '2020-01-29 14:25:39', NULL);
INSERT INTO public.clientes VALUES (63, 'Margarida', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-29 16:31:37', '2020-01-29 16:31:37', NULL);
INSERT INTO public.clientes VALUES (64, 'Aline Landgraf', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-01-29 18:46:21', '2020-01-29 18:46:21', NULL);
INSERT INTO public.clientes VALUES (65, 'Maria Eugenia Defino', NULL, NULL, '3561-1158', NULL, 'F', 1, NULL, NULL, NULL, '2020-02-03 20:49:49', '2020-02-03 20:49:49', NULL);
INSERT INTO public.clientes VALUES (67, 'Diane', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-02-05 19:15:20', '2020-02-05 19:15:20', NULL);
INSERT INTO public.clientes VALUES (68, 'Ivete Bussab', NULL, NULL, '19 997662512', NULL, 'F', 1, NULL, NULL, NULL, '2020-02-10 14:03:34', '2020-02-10 14:03:34', NULL);
INSERT INTO public.clientes VALUES (69, 'Rubens', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 14:04:54', '2020-02-10 14:04:54', NULL);
INSERT INTO public.clientes VALUES (70, 'Geraldo de Lima Rodrigues', NULL, NULL, '35627180/997371584', NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 14:06:49', '2020-02-10 14:06:49', NULL);
INSERT INTO public.clientes VALUES (71, 'Audrey R. Silva', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-02-10 16:37:22', '2020-02-10 16:37:22', NULL);
INSERT INTO public.clientes VALUES (72, 'Ana Maria Ap. Pereira', NULL, NULL, '19 997149823', NULL, 'F', 1, NULL, NULL, NULL, '2020-02-10 16:42:42', '2020-02-10 16:42:42', NULL);
INSERT INTO public.clientes VALUES (73, 'carnaval', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 18:09:36', '2020-02-10 18:09:36', NULL);
INSERT INTO public.clientes VALUES (74, 'carnaval', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 18:09:37', '2020-02-10 18:09:37', NULL);
INSERT INTO public.clientes VALUES (75, 'cinzas', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 18:12:19', '2020-02-10 18:13:58', '2020-02-10 18:13:58');
INSERT INTO public.clientes VALUES (76, 'cinzas', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 18:16:16', '2020-02-10 18:16:16', NULL);
INSERT INTO public.clientes VALUES (77, 'Anderson Honorio dos Santos', NULL, NULL, '99835-1312', NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 19:38:07', '2020-02-10 19:38:07', NULL);
INSERT INTO public.clientes VALUES (78, 'Anderson Honorio dos Santos', NULL, NULL, '99835-1312', NULL, 'M', 1, NULL, NULL, NULL, '2020-02-10 19:38:08', '2020-02-10 19:38:08', NULL);
INSERT INTO public.clientes VALUES (80, 'gabriela', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-02-17 14:29:28', '2020-02-17 14:29:28', NULL);
INSERT INTO public.clientes VALUES (81, 'NÃO AGENDAR', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-17 18:09:10', '2020-02-17 18:09:10', NULL);
INSERT INTO public.clientes VALUES (82, 'Antonio Santos', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-18 18:45:53', '2020-02-18 18:45:53', NULL);
INSERT INTO public.clientes VALUES (83, 'Deia', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-02-19 17:05:09', '2020-02-19 17:05:09', NULL);
INSERT INTO public.clientes VALUES (66, 'Audrey R Silva', NULL, NULL, '19996086267', NULL, 'F', 1, NULL, NULL, NULL, '2020-02-05 13:57:44', '2020-02-19 17:50:09', NULL);
INSERT INTO public.clientes VALUES (84, 'Wladimir', NULL, NULL, NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-02-27 17:55:00', '2020-02-27 17:55:00', NULL);
INSERT INTO public.clientes VALUES (79, 'Claudia Bertoldo', NULL, NULL, '19 997592454', NULL, 'F', 1, NULL, NULL, NULL, '2020-02-17 13:47:31', '2020-02-27 19:09:15', NULL);
INSERT INTO public.clientes VALUES (85, 'Ana Maria Pereira', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-03-06 20:21:41', '2020-03-06 20:21:41', NULL);
INSERT INTO public.clientes VALUES (86, 'Lidiane Lopes Santos', NULL, NULL, '19997271128', NULL, 'F', 1, NULL, NULL, NULL, '2020-03-06 20:27:19', '2020-03-06 20:27:19', NULL);
INSERT INTO public.clientes VALUES (87, 'Gersumina Oliveira Navarro', NULL, NULL, '998347766', NULL, 'F', 1, NULL, NULL, NULL, '2020-03-06 20:31:48', '2020-03-06 20:31:48', NULL);
INSERT INTO public.clientes VALUES (88, 'Nair', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-03-06 20:33:07', '2020-03-06 20:33:07', NULL);
INSERT INTO public.clientes VALUES (89, 'Monica Domingos', NULL, NULL, '971517778', NULL, 'F', 1, NULL, NULL, NULL, '2020-03-06 20:35:36', '2020-03-06 20:35:36', NULL);
INSERT INTO public.clientes VALUES (90, 'Monica Domingos', NULL, NULL, '971517778', NULL, 'F', 1, NULL, NULL, NULL, '2020-03-09 19:11:39', '2020-03-09 19:11:39', NULL);
INSERT INTO public.clientes VALUES (91, 'Paula Meyer', NULL, NULL, NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-03-09 19:16:46', '2020-03-09 19:16:46', NULL);
INSERT INTO public.clientes VALUES (92, 'Karina Batista', NULL, NULL, '997996358', NULL, 'F', 1, NULL, NULL, NULL, '2020-03-09 19:26:40', '2020-03-09 19:26:40', NULL);
INSERT INTO public.clientes VALUES (93, 'Sonia Regina Rodrigues dos Santos', NULL, 'São Francisco', NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-09-17 12:51:16', '2020-09-17 12:51:16', NULL);
INSERT INTO public.clientes VALUES (94, 'Anezia', NULL, 'Unimed', NULL, NULL, 'F', 1, NULL, NULL, NULL, '2020-09-17 12:52:23', '2020-09-17 12:52:23', NULL);
INSERT INTO public.clientes VALUES (95, 'Germano', NULL, 'Unimed', NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-09-17 12:54:00', '2020-09-17 12:54:00', NULL);
INSERT INTO public.clientes VALUES (96, 'Paulo Carbonaro', NULL, 'Unimed', NULL, NULL, 'M', 1, NULL, NULL, NULL, '2020-09-17 12:54:59', '2020-09-17 12:54:59', NULL);


--
-- Name: clientes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.clientes_id_seq', 96, true);


--
-- Data for Name: enderecos; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: enderecos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.enderecos_id_seq', 1, false);


--
-- Data for Name: loc_ceps; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.loc_ceps VALUES (13631094, 1, 'Rua 1 de Maio', 'Centro', NULL, '3539301', '5368', '2019-09-12 18:13:44', '2019-09-12 18:13:44', NULL);
INSERT INTO public.loc_ceps VALUES (13635002, 1, 'Rua Nilo Maialle', 'Jardim Elite', NULL, '3539301', '5368', '2019-11-11 16:58:04', '2019-11-11 16:58:04', NULL);
INSERT INTO public.loc_ceps VALUES (13631045, 1, 'Avenida Newton Prado', 'Centro', NULL, '3539301', '5368', '2019-11-11 18:57:10', '2019-11-11 18:57:10', NULL);


--
-- Data for Name: loc_cidades; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.loc_cidades VALUES (1, 3539301, 'Pirassununga', 35, '2019-09-12 18:13:44', '2019-09-12 18:13:44', NULL);


--
-- Name: loc_cidades_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.loc_cidades_id_seq', 1, true);


--
-- Data for Name: loc_estados; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.loc_estados VALUES (12, 'Acre', 'AC', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (27, 'Alagoas', 'AL', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (13, 'Amazonas', 'AM', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (16, 'Amapá', 'AP', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (29, 'Bahia', 'BA', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (23, 'Ceara', 'CE', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (53, 'Distrito Federal', 'DF', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (32, 'Espirito Santo', 'ES', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (52, 'Goiás', 'GO', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (21, 'Maranhão', 'MA', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (31, 'Minas Gerais', 'MG', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (50, 'Mato Grosso do Sul', 'MS', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (51, 'Mato Grosso', 'MT', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (15, 'Pará', 'PA', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (25, 'Paraíba', 'PB', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (26, 'Pernambuco', 'PE', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (22, 'Piauí', 'PI', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (41, 'Paraná', 'PR', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (33, 'Rio de Janeiro', 'RJ', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (24, 'Rio Grande do Norte', 'RN', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (11, 'Rondônia', 'RO', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (14, 'Roraima', 'RR', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (43, 'Rio Grande do Sul', 'RS', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (42, 'Santa Catarina', 'SC', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (28, 'Sergipe', 'SE', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (35, 'São Paulo', 'SP', NULL, NULL, NULL);
INSERT INTO public.loc_estados VALUES (17, 'Tocantins', 'TO', NULL, NULL, NULL);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.migrations VALUES (1, '2019_01_17_123609_create_password_resets_table', 1);
INSERT INTO public.migrations VALUES (2, '2019_02_20_163607_tabela_localidades', 1);
INSERT INTO public.migrations VALUES (3, '2019_02_25_122303_tabela_empresas', 1);
INSERT INTO public.migrations VALUES (4, '2019_02_25_122322_tabela_grupos', 1);
INSERT INTO public.migrations VALUES (5, '2019_02_25_122331_tabela_programas', 1);
INSERT INTO public.migrations VALUES (6, '2019_02_25_122350_tabela_usuarios', 1);
INSERT INTO public.migrations VALUES (7, '2019_02_25_122405_tabela_grupos_programas', 1);
INSERT INTO public.migrations VALUES (8, '2019_02_25_122421_tabela_grupos_usuarios', 1);
INSERT INTO public.migrations VALUES (9, '2019_02_25_122449_tabela_programas_usuarios', 1);
INSERT INTO public.migrations VALUES (10, '2019_02_25_122523_tabela_empresas_usuarios', 1);
INSERT INTO public.migrations VALUES (11, '2019_02_25_123133_tabela_enderecos', 1);
INSERT INTO public.migrations VALUES (12, '2019_09_05_195146_criar_tabela_clientes', 1);
INSERT INTO public.migrations VALUES (13, '2019_09_06_170818_criar_tabela_agenda', 1);
INSERT INTO public.migrations VALUES (14, '2019_09_10_135858_criar_tabela_prontuario', 1);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.migrations_id_seq', 14, true);


--
-- Data for Name: prontuario; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.prontuario VALUES (1, 2, 1, 1, 'Queixa de teste', NULL, NULL, NULL, 'Evolução de teste', '2019-09-13', '5', 150.00, 1, '2019-09-12 18:14:23', '2019-09-12 18:16:25', NULL);
INSERT INTO public.prontuario VALUES (2, 2, 1, 2, 'Queixando', NULL, NULL, NULL, NULL, '2019-09-13', NULL, NULL, 1, '2019-09-13 11:52:50', '2019-09-13 11:52:50', NULL);
INSERT INTO public.prontuario VALUES (3, 2, 1, 3, 'Rv', NULL, NULL, NULL, NULL, '2019-09-17', NULL, NULL, 1, '2019-09-16 20:29:58', '2019-09-16 20:29:58', NULL);
INSERT INTO public.prontuario VALUES (4, 2, 1, 4, 'Rv', NULL, NULL, NULL, NULL, '2019-09-17', NULL, NULL, 1, '2019-09-16 20:29:59', '2019-09-16 20:29:59', NULL);
INSERT INTO public.prontuario VALUES (5, 2, 1, 5, 'rv', NULL, NULL, NULL, NULL, '2019-09-17', NULL, NULL, 1, '2019-09-16 20:43:25', '2019-09-16 20:43:25', NULL);
INSERT INTO public.prontuario VALUES (6, 2, 1, 6, 'teste', NULL, NULL, NULL, NULL, '2019-09-20', NULL, NULL, 1, '2019-09-19 21:01:06', '2019-09-19 21:01:06', NULL);
INSERT INTO public.prontuario VALUES (7, 2, 3, 7, 'Acupuntura', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 17:00:14', '2019-10-28 17:00:14', NULL);
INSERT INTO public.prontuario VALUES (8, 2, 9, 8, 'R.V.', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:01:48', '2019-10-28 18:01:48', NULL);
INSERT INTO public.prontuario VALUES (9, 2, 29, 9, 'R.V', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:04:14', '2019-10-28 18:04:14', NULL);
INSERT INTO public.prontuario VALUES (10, 2, 11, 10, 'R.V', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:07:05', '2019-10-28 18:07:05', NULL);
INSERT INTO public.prontuario VALUES (11, 2, 12, 11, 'Uro', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:07:51', '2019-10-28 18:07:51', NULL);
INSERT INTO public.prontuario VALUES (12, 2, 13, 12, 'URO', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:08:19', '2019-10-28 18:08:19', NULL);
INSERT INTO public.prontuario VALUES (13, 2, 23, 13, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:13:04', '2019-10-28 18:13:04', NULL);
INSERT INTO public.prontuario VALUES (14, 2, 5, 14, 'URO', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:13:55', '2019-10-28 18:13:55', NULL);
INSERT INTO public.prontuario VALUES (15, 2, 24, 15, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:14:24', '2019-10-28 18:14:24', NULL);
INSERT INTO public.prontuario VALUES (16, 2, 16, 16, 'Acupuntura', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:14:53', '2019-10-28 18:14:53', NULL);
INSERT INTO public.prontuario VALUES (17, 2, 11, 17, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:15:19', '2019-10-28 18:15:19', NULL);
INSERT INTO public.prontuario VALUES (18, 2, 18, 18, 'URO', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:15:50', '2019-10-28 18:15:50', NULL);
INSERT INTO public.prontuario VALUES (19, 2, 20, 19, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:19:58', '2019-10-28 18:19:58', NULL);
INSERT INTO public.prontuario VALUES (20, 2, 30, 20, 'URO', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:23:02', '2019-10-28 18:23:02', NULL);
INSERT INTO public.prontuario VALUES (21, 2, 31, 21, 'RV', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 18:31:33', '2019-10-28 18:31:33', NULL);
INSERT INTO public.prontuario VALUES (22, 2, 32, 22, 'URO', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:35:14', '2019-10-28 18:35:14', NULL);
INSERT INTO public.prontuario VALUES (23, 2, 26, 23, 'URO', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:35:43', '2019-10-28 18:35:43', NULL);
INSERT INTO public.prontuario VALUES (24, 2, 13, 24, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-28 18:36:07', '2019-10-28 18:36:07', NULL);
INSERT INTO public.prontuario VALUES (25, 2, 22, 25, 'RV', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-28 18:36:50', '2019-10-28 18:36:50', NULL);
INSERT INTO public.prontuario VALUES (26, 2, 23, 26, 'RV', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-28 18:38:38', '2019-10-28 18:38:38', NULL);
INSERT INTO public.prontuario VALUES (27, 2, 17, 27, 'Acupuntura', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-28 18:39:11', '2019-10-28 18:39:11', NULL);
INSERT INTO public.prontuario VALUES (28, 2, 27, 28, 'Rv', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-28 18:39:55', '2019-10-28 18:39:55', NULL);
INSERT INTO public.prontuario VALUES (29, 2, 18, 29, 'URO', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-28 18:40:26', '2019-10-28 18:40:26', NULL);
INSERT INTO public.prontuario VALUES (30, 2, 1, 30, 'rv', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 19:30:00', '2019-10-28 19:30:00', NULL);
INSERT INTO public.prontuario VALUES (31, 2, 1, 31, 'RV', NULL, NULL, NULL, NULL, '2019-10-28', NULL, NULL, 1, '2019-10-28 20:38:22', '2019-10-28 20:38:22', NULL);
INSERT INTO public.prontuario VALUES (32, 2, 19, 32, 'RV', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-30 12:30:14', '2019-10-30 12:30:14', NULL);
INSERT INTO public.prontuario VALUES (33, 2, 21, 33, 'RV  (UNIMED)', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-30 13:27:30', '2019-10-30 13:27:30', NULL);
INSERT INTO public.prontuario VALUES (34, 2, 21, 34, 'RV  (UNIMED)', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-10-30 13:28:10', '2019-10-30 13:28:10', NULL);
INSERT INTO public.prontuario VALUES (35, 2, 30, 35, 'URO', NULL, NULL, NULL, NULL, '2019-10-30', NULL, NULL, 1, '2019-10-30 17:08:37', '2019-10-30 17:08:37', NULL);
INSERT INTO public.prontuario VALUES (36, 2, 33, 36, 'Part. Fisio', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-11-01 13:24:23', '2019-11-01 13:24:23', NULL);
INSERT INTO public.prontuario VALUES (37, 2, 34, 37, 'PART.', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-11-01 14:15:04', '2019-11-01 14:15:04', NULL);
INSERT INTO public.prontuario VALUES (38, 2, 32, 38, 'URO', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-11-01 18:09:07', '2019-11-01 18:09:07', NULL);
INSERT INTO public.prontuario VALUES (39, 2, 11, 39, 'RV', NULL, NULL, NULL, NULL, '2019-11-01', NULL, NULL, 1, '2019-11-01 18:09:38', '2019-11-01 18:09:38', NULL);
INSERT INTO public.prontuario VALUES (40, 2, 20, 40, 'uro', NULL, NULL, NULL, NULL, '2019-11-06', NULL, NULL, 1, '2019-11-06 13:27:29', '2019-11-06 13:27:29', NULL);
INSERT INTO public.prontuario VALUES (41, 2, 5, 41, 'uro', NULL, NULL, NULL, NULL, '2019-11-06', NULL, NULL, 1, '2019-11-06 17:01:21', '2019-11-06 17:01:21', NULL);
INSERT INTO public.prontuario VALUES (42, 2, 22, 42, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:21:57', '2019-11-06 18:21:57', NULL);
INSERT INTO public.prontuario VALUES (43, 2, 30, 43, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:23:04', '2019-11-06 18:23:04', NULL);
INSERT INTO public.prontuario VALUES (44, 2, 17, 44, 'acup', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:23:57', '2019-11-06 18:23:57', NULL);
INSERT INTO public.prontuario VALUES (45, 2, 11, 45, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:24:54', '2019-11-06 18:24:54', NULL);
INSERT INTO public.prontuario VALUES (46, 2, 23, 46, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:25:41', '2019-11-06 18:25:41', NULL);
INSERT INTO public.prontuario VALUES (47, 2, 28, 47, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:26:47', '2019-11-06 18:26:47', NULL);
INSERT INTO public.prontuario VALUES (48, 2, 5, 48, 'uro', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-06 18:27:43', '2019-11-06 18:27:43', NULL);
INSERT INTO public.prontuario VALUES (49, 2, 36, 49, 'SF  URO', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-08 20:03:33', '2019-11-08 20:03:33', NULL);
INSERT INTO public.prontuario VALUES (50, 2, 37, 50, 'Acup.', NULL, NULL, NULL, NULL, '2019-11-08', NULL, NULL, 1, '2019-11-08 20:32:24', '2019-11-08 20:32:24', NULL);
INSERT INTO public.prontuario VALUES (51, 2, 38, 51, 'URO (SF)', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:35:49', '2019-11-20 10:35:49', NULL);
INSERT INTO public.prontuario VALUES (52, 2, 39, 52, 'RV (UNIM)', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:37:58', '2019-11-20 10:37:58', NULL);
INSERT INTO public.prontuario VALUES (53, 2, 5, 53, 'URO', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:38:39', '2019-11-20 10:38:39', NULL);
INSERT INTO public.prontuario VALUES (54, 2, 24, 54, 'URO', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:39:12', '2019-11-20 10:39:12', NULL);
INSERT INTO public.prontuario VALUES (55, 2, 16, 55, 'RV', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:39:37', '2019-11-20 10:39:37', NULL);
INSERT INTO public.prontuario VALUES (56, 2, 40, 56, 'uro', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:40:10', '2019-11-20 10:40:10', NULL);
INSERT INTO public.prontuario VALUES (57, 2, 18, 57, 'uro', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:40:33', '2019-11-20 10:40:33', NULL);
INSERT INTO public.prontuario VALUES (58, 2, 21, 58, 'rv', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:40:55', '2019-11-20 10:40:55', NULL);
INSERT INTO public.prontuario VALUES (59, 2, 32, 59, 'uro', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:41:23', '2019-11-20 10:41:23', NULL);
INSERT INTO public.prontuario VALUES (60, 2, 26, 60, 'uro', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:41:49', '2019-11-20 10:41:49', NULL);
INSERT INTO public.prontuario VALUES (61, 2, 41, 61, 'RV', NULL, NULL, NULL, NULL, '2019-11-20', NULL, NULL, 1, '2019-11-20 10:42:47', '2019-11-20 10:42:47', NULL);
INSERT INTO public.prontuario VALUES (62, 2, 35, 62, 'Urog', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 13:59:40', '2019-12-06 13:59:40', NULL);
INSERT INTO public.prontuario VALUES (63, 2, 4, 63, 'acup', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 14:00:24', '2019-12-06 14:00:24', NULL);
INSERT INTO public.prontuario VALUES (64, 2, 39, 64, 'RV', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 14:01:04', '2019-12-06 14:01:04', NULL);
INSERT INTO public.prontuario VALUES (65, 2, 7, 65, 'acup', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 14:01:43', '2019-12-06 14:01:43', NULL);
INSERT INTO public.prontuario VALUES (66, 2, 3, 66, 'acup', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 14:02:31', '2019-12-06 14:02:31', NULL);
INSERT INTO public.prontuario VALUES (67, 2, 22, 67, 'uro', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:10:12', '2019-12-06 14:10:12', NULL);
INSERT INTO public.prontuario VALUES (68, 2, 23, 68, 'rv', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:14:18', '2019-12-06 14:14:18', NULL);
INSERT INTO public.prontuario VALUES (69, 2, 17, 69, 'ac', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:14:53', '2019-12-06 14:14:53', NULL);
INSERT INTO public.prontuario VALUES (70, 2, 11, 70, 'rv', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:15:24', '2019-12-06 14:15:24', NULL);
INSERT INTO public.prontuario VALUES (71, 2, 18, 71, 'uro', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:15:47', '2019-12-06 14:15:47', NULL);
INSERT INTO public.prontuario VALUES (72, 2, 28, 72, 'uro', NULL, NULL, NULL, NULL, '2019-12-06', NULL, NULL, 1, '2019-12-06 14:16:15', '2019-12-06 14:16:15', NULL);
INSERT INTO public.prontuario VALUES (73, 2, 11, 73, 'urog', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 17:42:07', '2019-12-06 17:42:07', NULL);
INSERT INTO public.prontuario VALUES (74, 2, 34, 74, 'urog', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-06 17:42:53', '2019-12-06 17:42:53', NULL);
INSERT INTO public.prontuario VALUES (75, 2, 30, 75, 'rv', NULL, NULL, NULL, NULL, '2019-12-09', NULL, NULL, 1, '2019-12-09 12:19:37', '2019-12-09 12:19:37', NULL);
INSERT INTO public.prontuario VALUES (76, 2, 35, 76, 'rv', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:04:37', '2019-12-11 13:04:37', NULL);
INSERT INTO public.prontuario VALUES (77, 2, 5, 77, 'uro', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:05:40', '2019-12-11 13:05:40', NULL);
INSERT INTO public.prontuario VALUES (78, 2, 24, 78, 'uro', NULL, NULL, NULL, NULL, '2019-12-12', NULL, NULL, 1, '2019-12-11 13:06:02', '2019-12-11 13:06:02', NULL);
INSERT INTO public.prontuario VALUES (79, 2, 16, 79, 'uro', NULL, NULL, NULL, NULL, '2019-12-12', NULL, NULL, 1, '2019-12-11 13:06:39', '2019-12-11 13:06:39', NULL);
INSERT INTO public.prontuario VALUES (80, 2, 24, 80, 'uro', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:07:19', '2019-12-11 13:07:19', NULL);
INSERT INTO public.prontuario VALUES (81, 2, 38, 81, 'rv', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:08:14', '2019-12-11 13:08:14', NULL);
INSERT INTO public.prontuario VALUES (82, 2, 16, 82, 'rv', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:08:49', '2019-12-11 13:08:49', NULL);
INSERT INTO public.prontuario VALUES (83, 2, 30, 83, 'rv', NULL, NULL, NULL, NULL, '2019-12-11', NULL, NULL, 1, '2019-12-11 13:10:15', '2019-12-11 13:10:15', NULL);
INSERT INTO public.prontuario VALUES (84, 2, 28, 84, 'uro', 'Iu mista', NULL, NULL, 'Tibial post', '2019-12-11', '5', NULL, 1, '2019-12-11 13:16:16', '2019-12-13 19:11:58', NULL);
INSERT INTO public.prontuario VALUES (85, 2, 22, 85, 'uro', 'Paciente com queixa de infecção de urina de repetiçao', 'associação de acupuntura', NULL, '20/01- eletro tibial post., laser ton bexiga. Pc esta com infecção de urina( E. Coli).', '2020-01-20', '5', NULL, 1, '2020-01-20 17:14:05', '2020-01-20 17:18:13', NULL);
INSERT INTO public.prontuario VALUES (86, 2, 39, 86, 'rv', 'VPPb canal posterior esquerdo', NULL, NULL, '20/01- manobra negativa. Tontura as vezes ao se levantar', '2020-01-20', '5', NULL, 1, '2020-01-20 17:15:18', '2020-01-20 17:19:22', NULL);
INSERT INTO public.prontuario VALUES (87, 2, 4, 87, 'acupuntura', 'Sindrome do pânico e insônia e tpm', NULL, NULL, '20/01-insonia', '2020-01-20', '5', NULL, 1, '2020-01-20 17:20:50', '2020-01-20 17:21:39', NULL);
INSERT INTO public.prontuario VALUES (88, 2, 5, 88, 'urog', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 18:54:41', '2020-01-20 18:54:41', NULL);
INSERT INTO public.prontuario VALUES (89, 2, 5, 89, 'urog', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 18:55:06', '2020-01-20 18:55:06', NULL);
INSERT INTO public.prontuario VALUES (90, 2, 5, 90, 'urog', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 18:56:40', '2020-01-20 18:56:40', NULL);
INSERT INTO public.prontuario VALUES (91, 2, 43, 91, 'acupunt', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 18:58:54', '2020-01-20 18:58:54', NULL);
INSERT INTO public.prontuario VALUES (92, 2, 42, 92, 'uro', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 18:59:55', '2020-01-20 18:59:55', NULL);
INSERT INTO public.prontuario VALUES (93, 2, 44, 93, 'acupunt', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 19:01:06', '2020-01-20 19:01:06', NULL);
INSERT INTO public.prontuario VALUES (94, 2, 11, 94, 'uro', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 19:20:23', '2020-01-20 19:20:23', NULL);
INSERT INTO public.prontuario VALUES (95, 2, 45, 95, 'rv', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 19:21:58', '2020-01-20 19:21:58', NULL);
INSERT INTO public.prontuario VALUES (96, 2, 46, 96, 'urog', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 19:23:03', '2020-01-20 19:23:03', NULL);
INSERT INTO public.prontuario VALUES (97, 2, 47, 97, 'uro', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 19:24:35', '2020-01-20 19:24:35', NULL);
INSERT INTO public.prontuario VALUES (98, 2, 31, 99, 'rv', NULL, NULL, NULL, NULL, '2020-01-20', NULL, NULL, 1, '2020-01-20 20:03:48', '2020-01-20 20:03:48', NULL);
INSERT INTO public.prontuario VALUES (100, 2, 35, 100, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:17:26', '2020-01-20 20:17:26', NULL);
INSERT INTO public.prontuario VALUES (101, 2, 48, 101, 'rv', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:18:25', '2020-01-20 20:18:25', NULL);
INSERT INTO public.prontuario VALUES (102, 2, 5, 102, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:18:51', '2020-01-20 20:18:51', NULL);
INSERT INTO public.prontuario VALUES (103, 2, 24, 103, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:19:13', '2020-01-20 20:19:13', NULL);
INSERT INTO public.prontuario VALUES (106, 2, 18, 106, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:21:58', '2020-01-20 20:21:58', NULL);
INSERT INTO public.prontuario VALUES (107, 2, 51, 107, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:23:06', '2020-01-20 20:23:06', NULL);
INSERT INTO public.prontuario VALUES (108, 2, 32, 108, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:23:38', '2020-01-20 20:23:38', NULL);
INSERT INTO public.prontuario VALUES (109, 2, 30, 109, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:24:08', '2020-01-20 20:24:08', NULL);
INSERT INTO public.prontuario VALUES (111, 2, 42, 111, 'urog', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-20 20:25:48', '2020-01-20 20:25:48', NULL);
INSERT INTO public.prontuario VALUES (112, 2, 53, 112, 'urog', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:26:54', '2020-01-20 20:26:54', NULL);
INSERT INTO public.prontuario VALUES (113, 2, 53, 113, 'urog', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:27:29', '2020-01-20 20:27:29', NULL);
INSERT INTO public.prontuario VALUES (114, 2, 17, 114, 'acupunt', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:28:58', '2020-01-20 20:28:58', NULL);
INSERT INTO public.prontuario VALUES (99, 2, 31, 98, 'rv', 'Tontura do tipo rotatoria', 'Dor cervical', NULL, '20/01/20- canal post direito positivo forte', '2020-01-20', '5', NULL, 1, '2020-01-20 20:03:48', '2020-01-20 20:44:36', NULL);
INSERT INTO public.prontuario VALUES (115, 2, 54, 115, 'urog', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:46:19', '2020-01-20 20:46:19', NULL);
INSERT INTO public.prontuario VALUES (116, 2, 18, 116, 'urog', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:47:30', '2020-01-20 20:47:30', NULL);
INSERT INTO public.prontuario VALUES (119, 2, 41, 119, 'urog', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-20 20:50:13', '2020-01-20 20:50:13', NULL);
INSERT INTO public.prontuario VALUES (104, 2, 49, 104, 'urog', 'Perda urinaria há meses, 24 anos. Perda aos esforços e urgeincontinencia. 0 gestações. Acorda 2x ou mais para ir ao banheiro.', 'Pedi diário miccional e fara urodinamica dia 27/01.', NULL, NULL, '2020-01-22', '5', NULL, 1, '2020-01-20 20:20:04', '2020-01-22 16:42:44', NULL);
INSERT INTO public.prontuario VALUES (105, 2, 50, 105, 'rv', 'tontura rotatória. Canal post esp positivo', NULL, NULL, '22/01- canal anterior. Positivo no retorno da manobra. Sentiu tontura forte e náusea.', '2020-01-22', '5', NULL, 1, '2020-01-20 20:21:22', '2020-01-22 17:07:38', NULL);
INSERT INTO public.prontuario VALUES (120, 2, 31, 120, 'rv', NULL, NULL, NULL, NULL, '2020-01-22', NULL, NULL, 1, '2020-01-22 17:19:39', '2020-01-22 17:19:39', NULL);
INSERT INTO public.prontuario VALUES (110, 2, 52, 110, 'urog', 'Perda por hiperatividade detrusora', NULL, NULL, '22/01/20- tibial posterior', '2020-01-22', '5', NULL, 1, '2020-01-20 20:25:18', '2020-01-22 19:39:22', NULL);
INSERT INTO public.prontuario VALUES (121, 2, 57, 121, 'rv', 'canal post d negativo', 'dei alta', NULL, NULL, '2020-01-24', '5', NULL, 1, '2020-01-22 17:21:42', '2020-01-24 16:45:33', NULL);
INSERT INTO public.prontuario VALUES (122, 2, 45, 122, 'Rv', 'tonturas do tipo rotatoria', 'canal posterior d positivo forte
Após 6 manobras mantem tontura.
Fez vecto - canal post esq positivo', NULL, '24/01/2020- canal post esq positivo fraco', '2020-01-24', '5', NULL, 1, '2020-01-24 17:58:46', '2020-01-24 18:01:06', NULL);
INSERT INTO public.prontuario VALUES (123, 2, 31, 123, 'rv', NULL, NULL, NULL, NULL, '2020-01-24', NULL, NULL, 1, '2020-01-24 18:25:36', '2020-01-24 18:25:36', NULL);
INSERT INTO public.prontuario VALUES (117, 2, 32, 117, 'urog', 'perda urinaria mista', NULL, NULL, '24/01/20- tibial posterior', '2020-01-24', '5', NULL, 1, '2020-01-20 20:48:21', '2020-01-24 19:15:05', NULL);
INSERT INTO public.prontuario VALUES (125, 2, 4, 125, 'acup', NULL, NULL, NULL, NULL, '2020-01-27', NULL, NULL, 1, '2020-01-24 19:21:04', '2020-01-24 19:21:04', NULL);
INSERT INTO public.prontuario VALUES (118, 2, 47, 118, 'uro', 'Dor pélvica crônica. Dor em virilha direita com irradiação para glande do pênis. Sindrome da bexiga hiperativa', NULL, 'Dor aguda em sacroiliaca a direita. tensão de assoalho pélvico com ponto gatilho a direita.', '24/01/20- tens intra anal 100hz continuo', '2020-01-24', '5', NULL, 1, '2020-01-20 20:49:11', '2020-01-24 20:07:16', NULL);
INSERT INTO public.prontuario VALUES (145, 2, 23, 145, 'urog', 'Hiperatividade detrusora. Perda noturna e urgeincontinencia.', NULL, NULL, NULL, '2020-01-30', '5', NULL, 1, '2020-01-27 16:40:25', '2020-01-30 16:31:52', NULL);
INSERT INTO public.prontuario VALUES (124, 2, 4, 124, 'acup', NULL, NULL, NULL, NULL, '2020-01-27', '5', NULL, 1, '2020-01-24 19:21:03', '2020-01-27 14:04:40', NULL);
INSERT INTO public.prontuario VALUES (131, 2, 23, 131, 'urog', NULL, NULL, NULL, NULL, '2020-01-27', NULL, NULL, 1, '2020-01-27 14:13:17', '2020-01-27 14:13:17', NULL);
INSERT INTO public.prontuario VALUES (135, 2, 46, 135, 'urog', NULL, NULL, NULL, NULL, '2020-01-27', NULL, NULL, 1, '2020-01-27 14:17:21', '2020-01-27 14:17:21', NULL);
INSERT INTO public.prontuario VALUES (136, 2, 47, 136, 'urog', NULL, NULL, NULL, NULL, '2020-01-27', NULL, NULL, 1, '2020-01-27 14:17:51', '2020-01-27 14:17:51', NULL);
INSERT INTO public.prontuario VALUES (160, 2, 63, 160, 'acupunt', 'Há uma semana dor em lombar localizada e dor no joelho D.', NULL, 'Nodulo muscular em lombar a D.', '30/01/20- tens 2-100hz.', '2020-01-30', '5', NULL, 1, '2020-01-29 16:32:11', '2020-01-30 20:04:09', NULL);
INSERT INTO public.prontuario VALUES (148, 2, 17, 148, 'acupunt', 'Retenção urinaria. já realizada cirurgia de retocele, porem não houve melhora. Fez fluxometria atual e houve grande  residuo miccional novamente. Não ha perda urinaria.', NULL, NULL, NULL, '2020-01-31', '5', NULL, 1, '2020-01-27 16:42:15', '2020-01-31 17:44:00', NULL);
INSERT INTO public.prontuario VALUES (143, 2, 52, 143, 'urog', NULL, NULL, NULL, NULL, '2020-01-29', NULL, NULL, 1, '2020-01-27 14:21:16', '2020-01-27 14:21:16', NULL);
INSERT INTO public.prontuario VALUES (150, 2, 18, 150, 'urog', NULL, NULL, NULL, NULL, '2020-01-31', '5', NULL, 1, '2020-01-27 16:43:49', '2020-01-31 19:11:36', NULL);
INSERT INTO public.prontuario VALUES (146, 2, 39, 146, 'rv', NULL, NULL, NULL, NULL, '2020-01-30', NULL, NULL, 1, '2020-01-27 16:40:57', '2020-01-27 16:40:57', NULL);
INSERT INTO public.prontuario VALUES (163, 2, 4, 163, 'acup', NULL, NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:25:47', '2020-02-03 13:33:41', NULL);
INSERT INTO public.prontuario VALUES (164, 2, 49, 164, 'urog', 'perda urinaria por urgeincontinencia. Bexiga hiperativa?', NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:26:15', '2020-02-03 17:03:22', NULL);
INSERT INTO public.prontuario VALUES (152, 2, 47, 152, 'urog', NULL, NULL, NULL, NULL, '2020-01-31', NULL, NULL, 1, '2020-01-27 16:44:58', '2020-01-27 16:44:58', NULL);
INSERT INTO public.prontuario VALUES (153, 2, 41, 153, 'gestante', NULL, NULL, NULL, NULL, '2020-01-31', NULL, NULL, 1, '2020-01-27 16:45:37', '2020-01-27 16:45:37', NULL);
INSERT INTO public.prontuario VALUES (130, 2, 23, 130, 'urog', 'hiperatividade detrusora com enurese noturna desde a infancia', NULL, NULL, 'tibial posterior', '2020-01-27', '5', NULL, 1, '2020-01-27 14:13:16', '2020-01-27 16:46:42', NULL);
INSERT INTO public.prontuario VALUES (128, 2, 3, 128, 'rv', NULL, NULL, NULL, NULL, '2020-01-27', '5', NULL, 1, '2020-01-27 13:30:52', '2020-01-27 18:01:17', NULL);
INSERT INTO public.prontuario VALUES (126, 2, 5, 126, 'urog', NULL, NULL, NULL, NULL, '2020-01-27', '5', NULL, 1, '2020-01-24 19:24:42', '2020-01-27 18:01:35', NULL);
INSERT INTO public.prontuario VALUES (132, 2, 45, 132, 'rv', NULL, NULL, NULL, '27/01/20- canal post D e E reposicionados. Paciente de alta, sem sintomas.', '2020-01-27', '5', NULL, 1, '2020-01-27 14:13:53', '2020-01-27 18:02:54', NULL);
INSERT INTO public.prontuario VALUES (134, 2, 22, 134, 'urog', 'Infecção de urina de repetiçao', 'Escoliose lombar a D e cervical a D', NULL, NULL, '2020-01-27', '5', NULL, 1, '2020-01-27 14:16:50', '2020-01-27 19:27:23', NULL);
INSERT INTO public.prontuario VALUES (133, 2, 11, 133, 'urog', 'Melanona com metástase em região sacral evoluindo com sintomas de cauda equina', NULL, NULL, NULL, '2020-01-27', '5', NULL, 1, '2020-01-27 14:14:25', '2020-01-27 19:28:18', NULL);
INSERT INTO public.prontuario VALUES (127, 2, 59, 127, 'Perda de urina aos esforços após parto vaginal. Bebe de 47 cm e 2,900kg. Fez episiotomia. Nega perda durante a gestante. Relata idas frequentes ao banheiro em poucas quantidades de urina.', NULL, 'Pedi anotações de horário de idas ao banheiro.', NULL, NULL, '2020-01-29', '5', NULL, 1, '2020-01-27 13:30:15', '2020-01-29 13:44:05', NULL);
INSERT INTO public.prontuario VALUES (155, 2, 5, 155, 'uro', NULL, NULL, NULL, '29/01/20- tibial posterior', '2020-01-29', '5', NULL, 1, '2020-01-29 13:54:36', '2020-01-29 14:22:27', NULL);
INSERT INTO public.prontuario VALUES (151, 2, 28, 151, 'urog', NULL, NULL, NULL, NULL, '2020-01-31', '5', NULL, 1, '2020-01-27 16:44:24', '2020-01-31 19:19:44', NULL);
INSERT INTO public.prontuario VALUES (185, 2, 63, 185, 'acup', NULL, NULL, NULL, NULL, '2020-02-04', '5', NULL, 1, '2020-02-03 18:37:02', '2020-02-05 13:51:21', NULL);
INSERT INTO public.prontuario VALUES (156, 2, 24, 156, 'uro', NULL, NULL, NULL, NULL, '2020-01-29', '5', NULL, 1, '2020-01-29 13:56:12', '2020-01-29 16:28:25', NULL);
INSERT INTO public.prontuario VALUES (138, 2, 50, 138, 'rv', NULL, NULL, NULL, 'Foi feita manobra de canal post esq. Manteve tontura, principalmente ao olhar para cima. Realizei manobra para canal ant e pc se sentiu muito mal, se recusou a realizar outras manobras.
29/01/20- so tens', '2020-01-29', '5', NULL, 1, '2020-01-27 14:18:58', '2020-01-29 16:30:25', NULL);
INSERT INTO public.prontuario VALUES (137, 2, 35, 137, 'urog', NULL, NULL, NULL, '29/01/20- contrações rápidas com eletroestimulaçao', '2020-01-29', '5', NULL, 1, '2020-01-27 14:18:27', '2020-01-29 16:31:06', NULL);
INSERT INTO public.prontuario VALUES (161, 2, 54, 161, 'urog', NULL, NULL, NULL, NULL, '2020-01-27', NULL, NULL, 1, '2020-01-29 16:33:26', '2020-01-29 16:33:26', NULL);
INSERT INTO public.prontuario VALUES (139, 2, 48, 139, 'rv', NULL, 'Não teve mais tontura, so realizando tens na cervical', NULL, NULL, '2020-01-29', '5', NULL, 1, '2020-01-27 14:19:20', '2020-01-29 16:47:20', NULL);
INSERT INTO public.prontuario VALUES (141, 2, 18, 141, 'urog', 'Perda urinaria após prostatectomia radical. Sintomas de hiperatividade detrusora. Uso de 3 absorventes por dia.', NULL, NULL, '29/01/20- tibial posterior', '2020-01-29', '5', NULL, 1, '2020-01-27 14:20:20', '2020-01-29 17:43:34', NULL);
INSERT INTO public.prontuario VALUES (191, 2, 24, 191, 'gestante', NULL, NULL, NULL, NULL, '2020-02-05', NULL, NULL, 1, '2020-02-05 13:54:43', '2020-02-05 13:54:43', NULL);
INSERT INTO public.prontuario VALUES (154, 2, 16, 154, 'acupunt', NULL, NULL, NULL, NULL, '2020-01-29', '5', NULL, 1, '2020-01-27 18:25:55', '2020-01-29 20:12:18', NULL);
INSERT INTO public.prontuario VALUES (140, 2, 49, 140, 'urog', 'Perda urinaria aos esforços e urgência miccional. Exame de urodinamica normal, porem com todas as características de bexiga hiperativa.
Diário miccional apresentando idas frequentes ao banheiro e algumas perdas com urgência.', 'Proposta de tto. Regular tônus e depois fortaceler', 'Hipertonia de AP a palpação. (após ser questionada, relata dor em relação sexual). Ponto gatilho em obturador int a E. 
Indicada massagem de Thiele.', '29/01/20- massagem em u. exercícios para casa 15 contraçoes elevador 3x ao dia', '2020-01-29', '5', NULL, 1, '2020-01-27 14:19:55', '2020-01-29 17:30:56', NULL);
INSERT INTO public.prontuario VALUES (162, 2, 64, 162, 'urog', NULL, NULL, NULL, '31/01/20- avaliação: tensão em ap (relata dor em relação sexual após parto). 3 seg de contração correta e conseguiu 15 contrações rápidas sem fadiga.
Não iniciamos tto pois esta há 3 semanas com sangramento.', '2020-01-31', '5', NULL, 1, '2020-01-29 18:46:50', '2020-01-31 16:53:33', NULL);
INSERT INTO public.prontuario VALUES (147, 2, 53, 147, 'urog', 'Pac com queix de perda urinaria ao realizar at física e urgeincontinencia, há 2 anos, com piora progressiva.', 'Urodinamica: CCM 400ml 
pressão detrusora: 1cm h20, com sensibilidade vesical aumentada, contrações involuntárias presentes. Não houve perda aos esforços
Conclusão: Hiperatividade detrusora desencadeada por esforços', 'normotonia
1 segundo de contração', '31/01/20- est pudendo (4hz) intavaginal + 5 min de contraçoes de 1 seg pra 3seg de relax', '2020-01-31', '5', NULL, 1, '2020-01-27 16:41:38', '2020-01-31 17:05:29', NULL);
INSERT INTO public.prontuario VALUES (149, 2, 61, 149, 'urog', 'Em dez de 2019, mov de torção da coluna com dor no cóccix. Faz RPG semanalmente. Atualmente dor em região sacral a E e sacroiliaca. Após RM atual, relata que bexiga centralizou novamente. 
Nega perda urinaria, se urina com pressa tem gotejamento pós-miccional.', 'Proposta relaxamento de pontos gatilhos e após fortalecimento.', 'Dor em obturador int a d e a Esq (com ponto gatilho) . Irradiou para glúteo.
3 seg de contração fraca. Contração anal maior.', NULL, '2020-01-31', '5', NULL, 1, '2020-01-27 16:43:19', '2020-01-31 18:31:14', NULL);
INSERT INTO public.prontuario VALUES (186, 2, 28, 186, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-02-03 19:27:50', '2020-02-03 19:27:50', NULL);
INSERT INTO public.prontuario VALUES (172, 2, 47, 172, 'urog', NULL, NULL, NULL, 'Relatório para o medico. Compressao do n. pudendo?', '2020-02-03', '5', NULL, 1, '2020-01-30 16:32:59', '2020-02-04 18:28:02', NULL);
INSERT INTO public.prontuario VALUES (184, 2, 62, 184, 'urog', NULL, NULL, NULL, '04/03/20- eletroestimulação para fortalecimento e inibição do pudendo', '2020-02-04', '5', NULL, 1, '2020-02-03 18:36:14', '2020-02-04 19:01:25', NULL);
INSERT INTO public.prontuario VALUES (192, 2, 63, 192, 'acupunt', 'Joelho D e lombar a D.', NULL, NULL, NULL, '2020-02-07', '5', NULL, 1, '2020-02-05 13:56:02', '2020-02-07 17:02:09', NULL);
INSERT INTO public.prontuario VALUES (209, 2, 11, 209, 'uro', NULL, NULL, NULL, NULL, '2020-02-10', NULL, NULL, 1, '2020-02-10 13:47:53', '2020-02-10 13:47:53', NULL);
INSERT INTO public.prontuario VALUES (214, 2, 69, 214, 'acup', NULL, NULL, NULL, NULL, '2020-02-11', NULL, NULL, 1, '2020-02-10 14:05:35', '2020-02-10 14:05:35', NULL);
INSERT INTO public.prontuario VALUES (220, 2, 59, 220, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:11:16', '2020-02-10 14:11:16', NULL);
INSERT INTO public.prontuario VALUES (221, 2, 28, 221, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:13:36', '2020-02-10 14:13:36', NULL);
INSERT INTO public.prontuario VALUES (222, 2, 18, 222, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:14:29', '2020-02-10 14:14:29', NULL);
INSERT INTO public.prontuario VALUES (223, 2, 30, 223, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:15:08', '2020-02-10 14:15:08', NULL);
INSERT INTO public.prontuario VALUES (224, 2, 52, 224, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:15:48', '2020-02-10 14:15:48', NULL);
INSERT INTO public.prontuario VALUES (226, 2, 62, 226, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:17:08', '2020-02-10 14:17:08', NULL);
INSERT INTO public.prontuario VALUES (227, 2, 54, 227, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', NULL, NULL, 1, '2020-02-10 14:17:48', '2020-02-10 14:17:48', NULL);
INSERT INTO public.prontuario VALUES (228, 2, 53, 228, 'urog', NULL, NULL, NULL, NULL, '2020-02-13', NULL, NULL, 1, '2020-02-10 14:21:08', '2020-02-10 14:21:08', NULL);
INSERT INTO public.prontuario VALUES (230, 2, 54, 230, 'urog', NULL, NULL, NULL, NULL, '2020-02-13', NULL, NULL, 1, '2020-02-10 14:22:56', '2020-02-10 14:22:56', NULL);
INSERT INTO public.prontuario VALUES (231, 2, 62, 231, 'urog', NULL, NULL, NULL, NULL, '2020-02-13', NULL, NULL, 1, '2020-02-10 16:35:49', '2020-02-10 16:35:49', NULL);
INSERT INTO public.prontuario VALUES (232, 2, 71, 232, 'urog', NULL, NULL, NULL, NULL, '2020-02-13', NULL, NULL, 1, '2020-02-10 16:38:00', '2020-02-10 16:38:00', NULL);
INSERT INTO public.prontuario VALUES (233, 2, 63, 233, 'acup', NULL, NULL, NULL, NULL, '2020-02-14', NULL, NULL, 1, '2020-02-10 16:39:06', '2020-02-10 16:39:06', NULL);
INSERT INTO public.prontuario VALUES (234, 2, 16, 234, 'acup', NULL, NULL, NULL, NULL, '2020-02-14', NULL, NULL, 1, '2020-02-10 16:39:58', '2020-02-10 16:39:58', NULL);
INSERT INTO public.prontuario VALUES (235, 2, 17, 235, 'acup', NULL, NULL, NULL, NULL, '2020-02-14', NULL, NULL, 1, '2020-02-10 16:40:39', '2020-02-10 16:40:39', NULL);
INSERT INTO public.prontuario VALUES (285, 2, 68, 285, 'RV', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-18 14:24:33', '2020-02-18 14:24:33', NULL);
INSERT INTO public.prontuario VALUES (240, 2, 41, 240, 'gestante', NULL, NULL, NULL, NULL, '2020-02-14', NULL, NULL, 1, '2020-02-10 16:44:30', '2020-02-10 16:44:30', NULL);
INSERT INTO public.prontuario VALUES (205, 2, 4, 205, 'acup', NULL, NULL, NULL, NULL, '2020-02-10', '5', NULL, 1, '2020-02-10 13:45:02', '2020-02-10 16:44:49', NULL);
INSERT INTO public.prontuario VALUES (206, 2, 5, 206, 'urog', NULL, NULL, NULL, NULL, '2020-02-10', '5', NULL, 1, '2020-02-10 13:45:32', '2020-02-10 16:45:10', NULL);
INSERT INTO public.prontuario VALUES (207, 2, 39, 207, 'rv', NULL, NULL, NULL, '10/02/20- manobra subjetiva a esq', '2020-02-10', '5', NULL, 1, '2020-02-10 13:46:17', '2020-02-10 16:45:52', NULL);
INSERT INTO public.prontuario VALUES (241, 2, 73, 241, 'car', NULL, NULL, NULL, NULL, '2020-02-24', NULL, NULL, 1, '2020-02-10 18:10:12', '2020-02-10 18:10:12', NULL);
INSERT INTO public.prontuario VALUES (242, 2, 74, 242, 'car', NULL, NULL, NULL, NULL, '2020-02-25', NULL, NULL, 1, '2020-02-10 18:11:11', '2020-02-10 18:11:11', NULL);
INSERT INTO public.prontuario VALUES (208, 2, 44, 208, 'acup', NULL, NULL, NULL, NULL, '2020-02-10', '5', NULL, 1, '2020-02-10 13:46:58', '2020-02-10 18:12:54', NULL);
INSERT INTO public.prontuario VALUES (243, 2, 75, 243, 'cinzas', NULL, NULL, NULL, NULL, '2020-02-26', NULL, NULL, 1, '2020-02-10 18:12:58', '2020-02-10 18:12:58', NULL);
INSERT INTO public.prontuario VALUES (244, 2, 76, 244, 'cinzas', NULL, NULL, NULL, NULL, '2020-02-26', NULL, NULL, 1, '2020-02-10 18:16:58', '2020-02-10 18:16:58', NULL);
INSERT INTO public.prontuario VALUES (245, 2, 78, 245, 'rv', 'cinetose somente com movimento de carro', NULL, NULL, '11/02/20- x1', '2020-02-11', '5', NULL, 1, '2020-02-10 19:38:28', '2020-02-11 18:07:15', NULL);
INSERT INTO public.prontuario VALUES (213, 2, 68, 213, 'rv', 'canal anterior D positivo por exame no dia 04/02/20.
Na clinica, deu tontura e nistagmo leve e não consegui verificar a direção que bateu.', NULL, NULL, '04/02/20- manobra para reposição de canal anterior D', '2020-02-11', '5', NULL, 1, '2020-02-10 14:04:08', '2020-02-11 18:25:38', NULL);
INSERT INTO public.prontuario VALUES (210, 2, 51, 210, 'urog', NULL, NULL, NULL, '10/02/20- exercícios com bola e solo', '2020-02-10', '5', NULL, 1, '2020-02-10 13:48:34', '2020-02-11 18:29:32', NULL);
INSERT INTO public.prontuario VALUES (211, 2, 46, 211, 'urog', NULL, NULL, NULL, '10/02/20- exercícios com bola e solo', '2020-02-10', '5', NULL, 1, '2020-02-10 13:49:14', '2020-02-11 18:30:16', NULL);
INSERT INTO public.prontuario VALUES (212, 2, 47, 212, 'urog', NULL, NULL, NULL, '10/02/20- na palpação, dor irradiada na glande do pênis , com massagem em corpo do períneo. Uso de peridell com ponteira fina', '2020-02-10', '5', NULL, 1, '2020-02-10 13:49:50', '2020-02-11 18:32:02', NULL);
INSERT INTO public.prontuario VALUES (215, 2, 70, 215, 'acup', NULL, NULL, NULL, NULL, '2020-02-11', '5', NULL, 1, '2020-02-10 14:07:23', '2020-02-11 19:51:38', NULL);
INSERT INTO public.prontuario VALUES (246, 2, 22, 246, 'urog', NULL, NULL, NULL, NULL, '2020-02-12', '5', NULL, 1, '2020-02-11 18:07:41', '2020-02-12 13:48:44', NULL);
INSERT INTO public.prontuario VALUES (216, 2, 35, 216, 'urog', NULL, NULL, NULL, '12/02/20- exercícios com bola e solo', '2020-02-12', '5', NULL, 1, '2020-02-10 14:08:33', '2020-02-12 13:49:16', NULL);
INSERT INTO public.prontuario VALUES (217, 2, 50, 217, 'rv', NULL, NULL, NULL, '12/02/20- exercícios para hiperreflexia bilateral. Olhando as mãos em movimentos para cima e para as laterais', '2020-02-12', '5', NULL, 1, '2020-02-10 14:09:07', '2020-02-12 13:50:23', NULL);
INSERT INTO public.prontuario VALUES (218, 2, 5, 218, 'urog', NULL, NULL, NULL, '12/02/20- acupuntura em R3 Bp6 VC4', '2020-02-12', '5', NULL, 1, '2020-02-10 14:09:44', '2020-02-12 14:14:15', NULL);
INSERT INTO public.prontuario VALUES (225, 2, 48, 225, 'rv', NULL, NULL, NULL, '12/02/20- teve tontura ao levantar da cama. Manobra subjetiva a D.', '2020-02-12', '5', NULL, 1, '2020-02-10 14:16:24', '2020-02-12 19:16:21', NULL);
INSERT INTO public.prontuario VALUES (219, 2, 24, 219, 'gestante', NULL, NULL, NULL, NULL, '2020-02-12', '5', NULL, 1, '2020-02-10 14:10:25', '2020-02-12 19:16:38', NULL);
INSERT INTO public.prontuario VALUES (229, 2, 64, 229, 'RELATA PERDA URINARIA APOS PARTO NORMAL. 5 MESES APOS. URGEINCONTINENCIA. GOTEJAMENTO POS- MICCIONAL. NÃO RELATA PERDA AOS ESFORÇOS.

iNDICADA POR dR SERGIO. Após parto, dor em relação sexual. Tensao em obturador interno a esq e ponto gatilho na entrada da vagina.', NULL, NULL, '2seg de contração profunda . boa contração rápida.', 'passei elevador e contrações rápidas após miccçao', '2020-02-13', '5', NULL, 1, '2020-02-10 14:21:52', '2020-02-13 17:23:12', NULL);
INSERT INTO public.prontuario VALUES (247, 2, 17, 247, 'rv', NULL, NULL, NULL, NULL, '2020-02-13', '5', NULL, 1, '2020-02-11 18:08:17', '2020-02-13 18:19:38', NULL);
INSERT INTO public.prontuario VALUES (248, 2, 16, 248, 'rv', NULL, NULL, NULL, NULL, '2020-02-13', NULL, NULL, 1, '2020-02-13 20:04:38', '2020-02-13 20:04:38', NULL);
INSERT INTO public.prontuario VALUES (291, 2, 11, 291, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-19 14:18:35', '2020-02-19 14:18:35', NULL);
INSERT INTO public.prontuario VALUES (238, 2, 72, 238, 'urog. 62 anos.', 'Há dois meses percebeu uma queda de bexiga. Relata ardência as vezes. Relata que não são todas as vezes que a bexiga sai. Sente mais quando vai evacuar, porem nega constipação. 
Vai ao banheiro varias vezes, mas sempre foi assim.', NULL, NULL, NULL, '2020-02-14', '5', NULL, 1, '2020-02-10 16:43:29', '2020-02-14 19:26:42', NULL);
INSERT INTO public.prontuario VALUES (239, 2, 47, 239, 'urog', NULL, NULL, NULL, '14/02/20- massagem e peridell em corpo do períneo. Houve melhora da dor no pênis após massagem perineal externa.', '2020-02-14', '5', NULL, 1, '2020-02-10 16:43:59', '2020-02-14 20:15:26', NULL);
INSERT INTO public.prontuario VALUES (237, 2, 18, 237, 'urog', NULL, NULL, NULL, NULL, '2020-02-14', '5', NULL, 1, '2020-02-10 16:41:39', '2020-02-14 20:17:29', NULL);
INSERT INTO public.prontuario VALUES (251, 2, 39, 251, 'rv', NULL, NULL, NULL, NULL, '2020-02-17', '5', NULL, 1, '2020-02-17 13:38:20', '2020-02-17 18:07:12', NULL);
INSERT INTO public.prontuario VALUES (142, 2, 30, 142, 'urog
pERDA FECAL POR HIPOTONIA DE ap. eM MANOMETRIA ANORRETAL HÁ SENSIBILIDADE.', NULL, NULL, NULL, '29/01/20- est parasacral 35 hz', '2020-01-29', '5', NULL, 1, '2020-01-27 14:20:49', '2020-01-29 18:27:38', NULL);
INSERT INTO public.prontuario VALUES (129, 2, 60, 129, 'rv/unimed', 'Tontura do tipo rotatória que melhoraram com manobras em outra profissional há anos. Atualmente tontura somente ao deitar e levantar. De ambos os lados.', NULL, NULL, '29/01/20- manobra positiva a D para canal post D (Nistagmo forte)', '2020-01-29', '5', NULL, 1, '2020-01-27 13:50:21', '2020-01-29 19:24:04', NULL);
INSERT INTO public.prontuario VALUES (180, 2, 28, 180, 'urog', NULL, NULL, NULL, NULL, '2020-02-05', NULL, NULL, 1, '2020-01-30 16:38:39', '2020-01-30 16:38:39', NULL);
INSERT INTO public.prontuario VALUES (182, 2, 49, 182, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-01-30 16:40:07', '2020-01-30 16:40:07', NULL);
INSERT INTO public.prontuario VALUES (183, 2, 51, 183, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-01-30 16:40:45', '2020-01-30 16:40:45', NULL);
INSERT INTO public.prontuario VALUES (157, 2, 54, 157, 'urog', 'Dor pélvica cronica', NULL, NULL, '30/01/20- dor em sacro ilíaca a esq. Massagem e tens local e sacral', '2020-01-30', '5', NULL, 1, '2020-01-29 14:23:18', '2020-01-30 17:52:17', NULL);
INSERT INTO public.prontuario VALUES (158, 2, 59, 158, 'urog', 'Perda urinaria aos esforços após 1 ano de parto normal. Relata também idas frequentes ao banheiro com pouca quantidade de urina.', 'Trabalhar resistência da musculatura(60%) . Inibir por 1 min as idas ao banheiro após realizar 3 contrações max.', 'Não há tensão em AP ou aderência em episiotomia. Normotonia.
3 seg de contração para força. 10 contrações rapidas', NULL, '2020-01-30', '5', NULL, 1, '2020-01-29 14:24:34', '2020-01-30 18:38:11', NULL);
INSERT INTO public.prontuario VALUES (159, 2, 62, 159, 'urog', 'Perda de urina há anos, com cirurgia de prolapso de bexiga em junho de 2019. Continua com perda urinaria mesmo após cirurgia. Não foi feito sling. Polaciuria . Perda aos pequenos esforços. Troca de absorvente que chega a ficar pesado.', NULL, 'Contração palpável . Flacidez de períneo. Não mantem contração.', NULL, '2020-01-30', '5', NULL, 1, '2020-01-29 14:26:13', '2020-01-30 19:25:26', NULL);
INSERT INTO public.prontuario VALUES (187, 2, 62, 187, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-02-03 19:28:23', '2020-02-03 19:28:23', NULL);
INSERT INTO public.prontuario VALUES (190, 2, 65, 190, 'rv', NULL, NULL, NULL, NULL, '2020-02-04', NULL, NULL, 1, '2020-02-03 20:50:54', '2020-02-03 20:50:54', NULL);
INSERT INTO public.prontuario VALUES (189, 2, 65, 189, 'rv', 'VPPB', 'Em dezembro realizou manobra de reposição do lado E.', NULL, '04/03/20- manobra negativa para ambos os lados. Brandt-Daroff negativo.', '2020-02-04', '5', NULL, 1, '2020-02-03 20:50:53', '2020-02-04 18:29:46', NULL);
INSERT INTO public.prontuario VALUES (173, 2, 35, 173, 'urog', NULL, NULL, NULL, '05/02/20- exercício do elevador por 4 seg + contração rápida por 5 min', '2020-02-05', '5', NULL, 1, '2020-01-30 16:34:02', '2020-02-05 13:52:45', NULL);
INSERT INTO public.prontuario VALUES (174, 2, 22, 174, 'urog', NULL, NULL, NULL, '05/02/20- teve inf de urina por E.Coli. Fez ultrassom e não há resíduo miccional.', '2020-02-05', '5', NULL, 1, '2020-01-30 16:34:32', '2020-02-05 13:54:15', NULL);
INSERT INTO public.prontuario VALUES (193, 2, 54, 193, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-02-05 13:56:53', '2020-02-05 13:56:53', NULL);
INSERT INTO public.prontuario VALUES (194, 2, 66, 194, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', NULL, NULL, 1, '2020-02-05 13:58:28', '2020-02-05 13:58:28', NULL);
INSERT INTO public.prontuario VALUES (175, 2, 5, 175, 'urog', NULL, NULL, NULL, '05/02/20- não relata dor. Laser e estimulação parassacral.', '2020-02-05', '5', NULL, 1, '2020-01-30 16:35:06', '2020-02-05 16:50:22', NULL);
INSERT INTO public.prontuario VALUES (176, 2, 16, 176, 'acup', NULL, NULL, NULL, NULL, '2020-02-05', '5', NULL, 1, '2020-01-30 16:35:58', '2020-02-05 16:59:43', NULL);
INSERT INTO public.prontuario VALUES (188, 2, 39, 188, 'RV', NULL, NULL, NULL, NULL, '2020-02-05', '5', NULL, 1, '2020-02-03 19:47:28', '2020-02-05 17:48:32', NULL);
INSERT INTO public.prontuario VALUES (177, 2, 18, 177, 'urog', NULL, NULL, NULL, '05/02/20- respiração diafragmática associada a contração perineal', '2020-02-05', '5', NULL, 1, '2020-01-30 16:36:35', '2020-02-05 17:49:46', NULL);
INSERT INTO public.prontuario VALUES (198, 2, 17, 198, 'acup', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 17:52:04', '2020-02-05 17:52:04', NULL);
INSERT INTO public.prontuario VALUES (199, 2, 61, 199, 'urog', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 17:52:56', '2020-02-05 17:52:56', NULL);
INSERT INTO public.prontuario VALUES (200, 2, 18, 200, 'urog', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 17:53:47', '2020-02-05 17:53:47', NULL);
INSERT INTO public.prontuario VALUES (201, 2, 47, 201, 'urog', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 17:54:36', '2020-02-05 17:54:36', NULL);
INSERT INTO public.prontuario VALUES (202, 2, 41, 202, 'gestante', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 17:55:23', '2020-02-05 17:55:23', NULL);
INSERT INTO public.prontuario VALUES (178, 2, 30, 178, 'urog', 'Paciente relatando perda de fezes e sensação de esvaziamento incompleto do reto. Cirurgia de prolapso retal e retite aguda erosiva em junho de 2019. Na cirurgia reparo do AP com esfincteroplastia.', 'Manometria: pressão anal basal reduzida e pressão de contração reduzida; RIA positivo; sensibilidade retal normal; hipotonia de esfíncter anal interno e de musculatura voluntaria.', 'Contração débil, não mantida a palpação.', '05/02/20- parassacral 35 hz', '2020-02-05', '5', NULL, 1, '2020-01-30 16:37:11', '2020-02-05 18:14:49', NULL);
INSERT INTO public.prontuario VALUES (179, 2, 52, 179, 'urog', NULL, NULL, NULL, NULL, '2020-02-05', '5', NULL, 1, '2020-01-30 16:37:51', '2020-02-05 18:50:31', NULL);
INSERT INTO public.prontuario VALUES (203, 2, 67, 203, 'urog', NULL, NULL, NULL, NULL, '2020-02-07', NULL, NULL, 1, '2020-02-05 19:15:53', '2020-02-05 19:15:53', NULL);
INSERT INTO public.prontuario VALUES (181, 2, 48, 181, 'rv', NULL, NULL, NULL, NULL, '2020-02-05', '5', NULL, 1, '2020-01-30 16:39:13', '2020-02-05 19:42:26', NULL);
INSERT INTO public.prontuario VALUES (197, 2, 59, 197, 'urog', NULL, NULL, NULL, 'iniciamos eletrodo com 3 seg contraçao', '2020-02-05', '5', NULL, 1, '2020-02-05 17:50:49', '2020-02-05 20:35:22', NULL);
INSERT INTO public.prontuario VALUES (196, 2, 50, 196, 'rv', 'Tontura VPPB a esq posicionada. Vecto: Disfunção vestibular periférica irritativa bilateral (2018)', NULL, NULL, '07/02/20- passei brandt daroff (deitar p esq). X1', '2020-02-07', '5', NULL, 1, '2020-02-05 13:59:52', '2020-02-07 17:01:36', NULL);
INSERT INTO public.prontuario VALUES (195, 2, 53, 195, 'urog', NULL, NULL, NULL, '07/02/20- tibial post e indiquei postergar em 1 min as idas ao banheiro', '2020-02-07', '5', NULL, 1, '2020-02-05 13:59:11', '2020-02-07 17:03:04', NULL);
INSERT INTO public.prontuario VALUES (204, 2, 23, 204, 'urog', NULL, NULL, NULL, NULL, '2020-02-06', '5', NULL, 1, '2020-02-05 19:56:05', '2020-02-07 17:03:23', NULL);
INSERT INTO public.prontuario VALUES (236, 2, 61, 236, 'urog', NULL, NULL, NULL, '14/02/20- a palpação não houve tensão e nem dor do AP. Avaliação de contração profunda 3seg e força superficial 4 seg de boa contração. Passei ex do elevador e pesinhos para casa.', '2020-02-14', '5', NULL, 1, '2020-02-10 16:41:12', '2020-02-14 20:17:10', NULL);
INSERT INTO public.prontuario VALUES (252, 2, 5, 252, 'urog', NULL, NULL, NULL, '17/02/20- agulhas em R3 Bp6', '2020-02-17', '5', NULL, 1, '2020-02-17 13:38:52', '2020-02-17 18:07:59', NULL);
INSERT INTO public.prontuario VALUES (286, 2, 68, 286, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-18 14:26:13', '2020-02-18 14:26:13', NULL);
INSERT INTO public.prontuario VALUES (292, 2, 83, 292, 'rv', 'Tontura do tipo flutuação com dor de cabeça occipital e temporal. Ouvido D tampado e zumbido.', 'Quis realizar manobra de Dix H. Negativa para ambas as orelhas
Indiquei avaliação com dentista (má oclusão)', 'Disfunção da ATM? Pontos gatilho em masseter D e E e dor em ambos pterigoideos. Má oclusao', NULL, '2020-02-20', '5', NULL, 1, '2020-02-19 17:06:00', '2020-02-20 19:34:49', NULL);
INSERT INTO public.prontuario VALUES (295, 2, 53, 295, 'urog', NULL, NULL, NULL, NULL, '2020-02-27', NULL, NULL, 1, '2020-02-20 19:51:28', '2020-02-20 19:51:28', NULL);
INSERT INTO public.prontuario VALUES (301, 2, 79, 301, 'urog', NULL, NULL, NULL, NULL, '2020-02-27', NULL, NULL, 1, '2020-02-20 19:58:30', '2020-02-20 19:58:30', NULL);
INSERT INTO public.prontuario VALUES (296, 2, 70, 296, 'acup', NULL, NULL, NULL, NULL, '2020-02-27', '5', NULL, 1, '2020-02-20 19:52:16', '2020-02-27 17:54:24', NULL);
INSERT INTO public.prontuario VALUES (302, 2, 84, 302, 'rv', NULL, NULL, NULL, NULL, '2020-02-27', NULL, NULL, 1, '2020-02-27 17:55:27', '2020-02-27 17:55:27', NULL);
INSERT INTO public.prontuario VALUES (297, 2, 64, 297, 'urog', NULL, NULL, NULL, '27/02/20- relata somente perda com urgência. Tibial post', '2020-02-27', '5', NULL, 1, '2020-02-20 19:54:09', '2020-02-27 18:26:06', NULL);
INSERT INTO public.prontuario VALUES (298, 2, 54, 298, 'urog', NULL, NULL, NULL, '27/02/20- Passou em consulta com Dra Carla. Iniciou Impere. Tibial post', '2020-02-27', '5', NULL, 1, '2020-02-20 19:56:02', '2020-02-27 19:06:44', NULL);
INSERT INTO public.prontuario VALUES (299, 2, 62, 299, 'urog', NULL, NULL, NULL, '27/02/20- contrações de 1 seg e repouso de 3seg. Intra vaginal 4hz. Relata que tem perdas sem estar com vontade e sem perceber', '2020-02-27', '5', NULL, 1, '2020-02-20 19:56:43', '2020-02-27 19:44:08', NULL);
INSERT INTO public.prontuario VALUES (300, 2, 71, 300, 'urog', NULL, 'Pelo diário miccional - síndrome da bexiga hiperativa. Medico indicou impere.', '27/02/20- tensão no corpo do períneo. à palpação, dor em obturador D. Relatou dor em relação sexual.
hipertonia de AP.', '27/02/20- urodinamica teve perda aos esforços. Não acusou hiperatividade detrusora.
diário- urgeincontinencia. Alguns episódios quando demora muito ao ir ao banheiro.', '2020-02-27', '5', NULL, 1, '2020-02-20 19:57:43', '2020-02-27 20:36:37', NULL);
INSERT INTO public.prontuario VALUES (303, 2, 4, 303, 'acup', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:18:43', '2020-03-06 20:18:43', NULL);
INSERT INTO public.prontuario VALUES (304, 2, 14, 304, 'acup', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:19:13', '2020-03-06 20:19:13', NULL);
INSERT INTO public.prontuario VALUES (322, 2, 16, 322, 'acup', NULL, NULL, NULL, NULL, '2020-03-10', '5', NULL, 1, '2020-03-09 19:13:11', '2020-03-10 20:19:43', NULL);
INSERT INTO public.prontuario VALUES (306, 2, 39, 306, 'rv', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:20:35', '2020-03-06 20:20:35', NULL);
INSERT INTO public.prontuario VALUES (307, 2, 85, 307, 'urog', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:22:21', '2020-03-06 20:22:21', NULL);
INSERT INTO public.prontuario VALUES (347, 2, 93, 347, 'RV', NULL, NULL, NULL, NULL, '2020-09-17', NULL, NULL, 1, '2020-09-17 12:51:46', '2020-09-17 12:51:46', NULL);
INSERT INTO public.prontuario VALUES (309, 2, 44, 309, 'acup', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:26:19', '2020-03-06 20:26:19', NULL);
INSERT INTO public.prontuario VALUES (348, 2, 94, 348, 'Rv', NULL, NULL, NULL, NULL, '2020-09-17', NULL, NULL, 1, '2020-09-17 12:52:47', '2020-09-17 12:52:47', NULL);
INSERT INTO public.prontuario VALUES (311, 2, 30, 311, 'urog', NULL, NULL, NULL, '09/03/20- tibial posterior', '2020-03-09', '5', NULL, 1, '2020-03-06 20:28:31', '2020-03-09 19:10:25', NULL);
INSERT INTO public.prontuario VALUES (167, 2, 3, 167, 'acup', NULL, NULL, NULL, NULL, '2020-02-03', NULL, NULL, 1, '2020-01-29 19:28:08', '2020-01-29 19:28:08', NULL);
INSERT INTO public.prontuario VALUES (169, 2, 11, 169, 'urog', NULL, NULL, NULL, NULL, '2020-02-03', NULL, NULL, 1, '2020-01-29 19:29:12', '2020-01-29 19:29:12', NULL);
INSERT INTO public.prontuario VALUES (144, 2, 51, 144, 'urog', NULL, 'prolapso bexiga e reto?', NULL, NULL, '2020-01-29', '5', NULL, 1, '2020-01-27 14:22:11', '2020-01-29 20:11:58', NULL);
INSERT INTO public.prontuario VALUES (165, 2, 5, 165, 'urog', 'dor pélvica crônica após uso de cateter duplo j para retirada de pedra do ureter', NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:26:44', '2020-02-03 17:04:22', NULL);
INSERT INTO public.prontuario VALUES (166, 2, 43, 166, 'acup', NULL, NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:27:30', '2020-02-03 17:04:39', NULL);
INSERT INTO public.prontuario VALUES (168, 2, 44, 168, 'acup', 'Dor lombar e ombros', NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:28:42', '2020-02-03 18:12:42', NULL);
INSERT INTO public.prontuario VALUES (170, 2, 60, 170, 'rv', 'VPPB', NULL, NULL, 'Realizada manobra para canal posterior D. Não teve mais tontura.
03/02/20- teste para D e E negativos', '2020-02-03', '5', NULL, 1, '2020-01-29 19:29:46', '2020-02-03 19:20:37', NULL);
INSERT INTO public.prontuario VALUES (171, 2, 46, 171, 'urog', NULL, NULL, NULL, NULL, '2020-02-03', '5', NULL, 1, '2020-01-29 19:30:22', '2020-02-04 18:26:55', NULL);
INSERT INTO public.prontuario VALUES (253, 2, 43, 253, 'acup', NULL, NULL, NULL, NULL, '2020-02-17', NULL, NULL, 1, '2020-02-17 13:39:35', '2020-02-17 13:39:35', NULL);
INSERT INTO public.prontuario VALUES (256, 2, 11, 256, 'uro', NULL, NULL, NULL, NULL, '2020-02-17', NULL, NULL, 1, '2020-02-17 13:42:01', '2020-02-17 13:42:01', NULL);
INSERT INTO public.prontuario VALUES (257, 2, 30, 257, 'urog', NULL, NULL, NULL, NULL, '2020-02-17', NULL, NULL, 1, '2020-02-17 13:42:40', '2020-02-17 13:42:40', NULL);
INSERT INTO public.prontuario VALUES (258, 2, 46, 258, 'urog', NULL, NULL, NULL, NULL, '2020-02-17', NULL, NULL, 1, '2020-02-17 13:43:08', '2020-02-17 13:43:08', NULL);
INSERT INTO public.prontuario VALUES (259, 2, 47, 259, 'uro', NULL, NULL, NULL, NULL, '2020-02-17', NULL, NULL, 1, '2020-02-17 13:43:43', '2020-02-17 13:43:43', NULL);
INSERT INTO public.prontuario VALUES (262, 2, 51, 262, 'urog', NULL, NULL, NULL, NULL, '2020-02-18', NULL, NULL, 1, '2020-02-17 13:45:14', '2020-02-17 13:45:14', NULL);
INSERT INTO public.prontuario VALUES (263, 2, 54, 263, 'urog', NULL, NULL, NULL, NULL, '2020-02-18', NULL, NULL, 1, '2020-02-17 13:45:45', '2020-02-17 13:45:45', NULL);
INSERT INTO public.prontuario VALUES (264, 2, 62, 264, 'urog', NULL, NULL, NULL, NULL, '2020-02-18', NULL, NULL, 1, '2020-02-17 13:46:27', '2020-02-17 13:46:27', NULL);
INSERT INTO public.prontuario VALUES (267, 2, 22, 267, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:28:33', '2020-02-17 14:28:33', NULL);
INSERT INTO public.prontuario VALUES (269, 2, 80, 269, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:29:52', '2020-02-17 14:29:52', NULL);
INSERT INTO public.prontuario VALUES (271, 2, 24, 271, 'gestante', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:35:54', '2020-02-17 14:35:54', NULL);
INSERT INTO public.prontuario VALUES (272, 2, 18, 272, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:37:23', '2020-02-17 14:37:23', NULL);
INSERT INTO public.prontuario VALUES (275, 2, 71, 275, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:40:23', '2020-02-17 14:40:23', NULL);
INSERT INTO public.prontuario VALUES (276, 2, 16, 276, 'acup', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 14:41:41', '2020-02-17 14:41:41', NULL);
INSERT INTO public.prontuario VALUES (277, 2, 53, 277, 'urog', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-17 14:43:55', '2020-02-17 14:43:55', NULL);
INSERT INTO public.prontuario VALUES (278, 2, 54, 278, 'urog', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-17 14:47:02', '2020-02-17 14:47:02', NULL);
INSERT INTO public.prontuario VALUES (280, 2, 71, 280, 'urog', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-17 14:49:29', '2020-02-17 14:49:29', NULL);
INSERT INTO public.prontuario VALUES (254, 2, 3, 254, 'acup', NULL, NULL, NULL, '17/02/20- coceira pelo corpo. Alergia alimentar? laser 3j em pontos de F e R. 146hz em pontos emocionais. semente auricular(shenmen, F, Vb e C)', '2020-02-17', '5', NULL, 1, '2020-02-17 13:40:09', '2020-02-17 18:05:37', NULL);
INSERT INTO public.prontuario VALUES (255, 2, 44, 255, 'acup', NULL, NULL, NULL, '17/02/20- somente agulhas. Dor lombar e ombro D.', '2020-02-17', '5', NULL, 1, '2020-02-17 13:40:41', '2020-02-17 18:06:31', NULL);
INSERT INTO public.prontuario VALUES (250, 2, 4, 250, 'acup', NULL, NULL, NULL, NULL, '2020-02-17', '5', NULL, 1, '2020-02-17 13:38:00', '2020-02-17 18:06:48', NULL);
INSERT INTO public.prontuario VALUES (281, 2, 81, 281, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 18:09:50', '2020-02-17 18:09:50', NULL);
INSERT INTO public.prontuario VALUES (282, 2, 81, 282, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-17 18:10:29', '2020-02-17 18:10:29', NULL);
INSERT INTO public.prontuario VALUES (283, 2, 81, 283, 'rz', NULL, NULL, NULL, NULL, '2020-02-18', NULL, NULL, 1, '2020-02-17 18:11:48', '2020-02-17 18:11:48', NULL);
INSERT INTO public.prontuario VALUES (284, 2, 81, 284, 'rx', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-17 18:12:27', '2020-02-17 18:12:27', NULL);
INSERT INTO public.prontuario VALUES (287, 2, 82, 287, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', NULL, NULL, 1, '2020-02-18 18:46:27', '2020-02-18 18:46:27', NULL);
INSERT INTO public.prontuario VALUES (249, 2, 7, 249, 'rv', NULL, NULL, NULL, NULL, '2020-02-18', '5', NULL, 1, '2020-02-14 19:14:03', '2020-02-18 18:48:44', NULL);
INSERT INTO public.prontuario VALUES (260, 2, 64, 260, 'urog', NULL, NULL, NULL, '18/02/20- exercícios na bola e solo', '2020-02-18', '5', NULL, 1, '2020-02-17 13:44:18', '2020-02-18 18:49:22', NULL);
INSERT INTO public.prontuario VALUES (261, 2, 77, 261, 'rv', NULL, NULL, NULL, '18/02/20- passei x2', '2020-02-18', '5', NULL, 1, '2020-02-17 13:44:46', '2020-02-18 18:49:53', NULL);
INSERT INTO public.prontuario VALUES (288, 2, 47, 288, 'urog', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-18 18:50:55', '2020-02-18 18:50:55', NULL);
INSERT INTO public.prontuario VALUES (289, 2, 81, 289, 'RX', NULL, NULL, NULL, NULL, '2020-02-21', NULL, NULL, 1, '2020-02-18 18:51:42', '2020-02-18 18:51:42', NULL);
INSERT INTO public.prontuario VALUES (265, 2, 70, 265, 'acup', NULL, NULL, NULL, NULL, '2020-02-18', '5', NULL, 1, '2020-02-17 13:46:59', '2020-02-18 20:20:47', NULL);
INSERT INTO public.prontuario VALUES (290, 2, 17, 290, 'rv', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-18 20:26:33', '2020-02-18 20:26:33', NULL);
INSERT INTO public.prontuario VALUES (266, 2, 79, 266, 'urog', '51 anos.Perdas urinárias que iniciaram há dois anos. Urgeincontinencia. Não conseguiu tomar a medicação por reações adversas. 1 cesariana. Fez urodinamica. Pedi diário miccional. Nega infecção urinaria.  Dor lombar (artrose quadris e fissura óssea).', NULL, NULL, NULL, '2020-02-18', '5', NULL, 1, '2020-02-17 13:47:57', '2020-02-18 20:34:48', NULL);
INSERT INTO public.prontuario VALUES (268, 2, 35, 268, 'urog', NULL, NULL, NULL, '19/02/20- elevador com eletrodo interno', '2020-02-19', '5', NULL, 1, '2020-02-17 14:29:02', '2020-02-19 14:20:55', NULL);
INSERT INTO public.prontuario VALUES (270, 2, 5, 270, 'urog', NULL, NULL, NULL, '19/02/20- tibial post com eletro. Pc relata que não tem mais dor, porem continua com urgência miccional em alguns dias', '2020-02-19', '5', NULL, 1, '2020-02-17 14:30:21', '2020-02-19 14:22:20', NULL);
INSERT INTO public.prontuario VALUES (293, 2, 23, 293, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', '5', NULL, 1, '2020-02-19 17:46:32', '2020-02-19 17:47:09', NULL);
INSERT INTO public.prontuario VALUES (273, 2, 48, 273, 'rv', NULL, NULL, NULL, NULL, '2020-02-19', '5', NULL, 1, '2020-02-17 14:38:37', '2020-02-19 18:04:47', NULL);
INSERT INTO public.prontuario VALUES (294, 2, 31, 294, 'rv', NULL, NULL, NULL, NULL, '2020-02-20', NULL, NULL, 1, '2020-02-19 18:29:54', '2020-02-19 18:29:54', NULL);
INSERT INTO public.prontuario VALUES (274, 2, 52, 274, 'urog', NULL, NULL, NULL, NULL, '2020-02-19', '5', NULL, 1, '2020-02-17 14:39:28', '2020-02-19 18:41:52', NULL);
INSERT INTO public.prontuario VALUES (279, 2, 62, 279, 'urog', NULL, NULL, NULL, '20/02/20- eletrodo int com 3 seg de contração e 7 seg de relaxamento. 5 min de 4hz.', '2020-02-20', '5', NULL, 1, '2020-02-17 14:48:10', '2020-02-20 19:31:18', NULL);
INSERT INTO public.prontuario VALUES (312, 2, 46, 312, 'urog', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:29:05', '2020-03-06 20:29:05', NULL);
INSERT INTO public.prontuario VALUES (313, 2, 47, 313, 'urog', NULL, NULL, NULL, NULL, '2020-03-09', NULL, NULL, 1, '2020-03-06 20:29:44', '2020-03-06 20:29:44', NULL);
INSERT INTO public.prontuario VALUES (314, 2, 23, 314, 'urog', NULL, NULL, NULL, NULL, '2020-03-10', NULL, NULL, 1, '2020-03-06 20:30:35', '2020-03-06 20:30:35', NULL);
INSERT INTO public.prontuario VALUES (349, 2, 7, 349, 'acupuntura', NULL, NULL, NULL, NULL, '2020-09-17', NULL, NULL, 1, '2020-09-17 12:53:26', '2020-09-17 12:53:26', NULL);
INSERT INTO public.prontuario VALUES (316, 2, 88, 316, 'urog', NULL, NULL, NULL, NULL, '2020-03-10', NULL, NULL, 1, '2020-03-06 20:33:37', '2020-03-06 20:33:37', NULL);
INSERT INTO public.prontuario VALUES (317, 2, 62, 317, 'urog', NULL, NULL, NULL, NULL, '2020-03-10', NULL, NULL, 1, '2020-03-06 20:34:11', '2020-03-06 20:34:11', NULL);
INSERT INTO public.prontuario VALUES (318, 2, 85, 318, 'rv', NULL, NULL, NULL, NULL, '2020-03-16', NULL, NULL, 1, '2020-03-09 12:24:22', '2020-03-09 12:24:22', NULL);
INSERT INTO public.prontuario VALUES (319, 2, 85, 319, 'rv', NULL, NULL, NULL, NULL, '2020-03-18', NULL, NULL, 1, '2020-03-09 12:25:01', '2020-03-09 12:25:01', NULL);
INSERT INTO public.prontuario VALUES (320, 2, 44, 320, 'rv', NULL, NULL, NULL, NULL, '2020-03-16', NULL, NULL, 1, '2020-03-09 13:48:41', '2020-03-09 13:48:41', NULL);
INSERT INTO public.prontuario VALUES (308, 2, 51, 308, 'urog', NULL, NULL, NULL, '09/03/20- eletro int 4hz e contrações rápidas. Teve perda associada a urgência logo ao levantar', '2020-03-09', '5', NULL, 1, '2020-03-06 20:24:19', '2020-03-09 18:06:40', NULL);
INSERT INTO public.prontuario VALUES (305, 2, 5, 305, 'urog', NULL, NULL, NULL, '09/03/20- somente sensação de urgência miccional. sem relato de dor', '2020-03-09', '5', NULL, 1, '2020-03-06 20:19:50', '2020-03-09 18:07:40', NULL);
INSERT INTO public.prontuario VALUES (310, 2, 86, 310, 'urog', 'Perdas urinarias há dois anos. Com urgência e com esforço. Troca de 3 absorventes(diário) por dia. 1 parto cesárea há 7 anos. 35 anos.', 'Relata muita cólica e menstruaçao abundante. US de útero normal, porem em RM, possível adenomiose.
Paciente cardíaca e não faz uso de anticoncepcional. Medica indicou DIU mirena.
Em diário miccional, idas frequentes ao banheiro e a maioria com urgência(mesmo em curtos períodos).', '09/03/20- contração sentida a palpação porem sem manutenção. Conseguiu realizar 4 contrações rápidas e a musc fadigou.', NULL, '2020-03-09', '5', NULL, 1, '2020-03-06 20:27:54', '2020-03-09 19:04:39', NULL);
INSERT INTO public.prontuario VALUES (323, 2, 22, 323, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:13:48', '2020-03-09 19:13:48', NULL);
INSERT INTO public.prontuario VALUES (324, 2, 35, 324, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:14:14', '2020-03-09 19:14:14', NULL);
INSERT INTO public.prontuario VALUES (325, 2, 80, 325, 'rv', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:14:41', '2020-03-09 19:14:41', NULL);
INSERT INTO public.prontuario VALUES (326, 2, 5, 326, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:15:15', '2020-03-09 19:15:15', NULL);
INSERT INTO public.prontuario VALUES (327, 2, 24, 327, 'gestante', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:15:49', '2020-03-09 19:15:49', NULL);
INSERT INTO public.prontuario VALUES (328, 2, 7, 328, 'acup', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:17:33', '2020-03-09 19:17:33', NULL);
INSERT INTO public.prontuario VALUES (329, 2, 85, 329, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:18:14', '2020-03-09 19:18:14', NULL);
INSERT INTO public.prontuario VALUES (330, 2, 18, 330, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:18:44', '2020-03-09 19:18:44', NULL);
INSERT INTO public.prontuario VALUES (331, 2, 64, 331, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:19:17', '2020-03-09 19:19:17', NULL);
INSERT INTO public.prontuario VALUES (332, 2, 52, 332, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:19:47', '2020-03-09 19:19:47', NULL);
INSERT INTO public.prontuario VALUES (333, 2, 86, 333, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:20:19', '2020-03-09 19:20:19', NULL);
INSERT INTO public.prontuario VALUES (334, 2, 71, 334, 'uro', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:20:55', '2020-03-09 19:20:55', NULL);
INSERT INTO public.prontuario VALUES (335, 2, 79, 335, 'urog', NULL, NULL, NULL, NULL, '2020-03-11', NULL, NULL, 1, '2020-03-09 19:21:35', '2020-03-09 19:21:35', NULL);
INSERT INTO public.prontuario VALUES (336, 2, 53, 336, 'urog', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:22:25', '2020-03-09 19:22:25', NULL);
INSERT INTO public.prontuario VALUES (337, 2, 87, 337, 'urog', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:23:24', '2020-03-09 19:23:24', NULL);
INSERT INTO public.prontuario VALUES (338, 2, 70, 338, 'acup', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:24:03', '2020-03-09 19:24:03', NULL);
INSERT INTO public.prontuario VALUES (339, 2, 54, 339, 'urog', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:24:44', '2020-03-09 19:24:44', NULL);
INSERT INTO public.prontuario VALUES (340, 2, 71, 340, 'urog', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:25:29', '2020-03-09 19:25:29', NULL);
INSERT INTO public.prontuario VALUES (341, 2, 92, 341, 'urog', NULL, NULL, NULL, NULL, '2020-03-12', NULL, NULL, 1, '2020-03-09 19:27:06', '2020-03-09 19:27:06', NULL);
INSERT INTO public.prontuario VALUES (342, 2, 17, 342, 'acup', NULL, NULL, NULL, NULL, '2020-03-13', NULL, NULL, 1, '2020-03-09 19:28:09', '2020-03-09 19:28:09', NULL);
INSERT INTO public.prontuario VALUES (343, 2, 61, 343, 'urog', NULL, NULL, NULL, NULL, '2020-03-13', NULL, NULL, 1, '2020-03-09 19:28:41', '2020-03-09 19:28:41', NULL);
INSERT INTO public.prontuario VALUES (344, 2, 18, 344, 'urog', NULL, NULL, NULL, NULL, '2020-03-13', NULL, NULL, 1, '2020-03-09 19:29:11', '2020-03-09 19:29:11', NULL);
INSERT INTO public.prontuario VALUES (345, 2, 3, 345, 'acup', NULL, NULL, NULL, NULL, '2020-03-13', NULL, NULL, 1, '2020-03-09 19:29:45', '2020-03-09 19:29:45', NULL);
INSERT INTO public.prontuario VALUES (346, 2, 47, 346, 'urog', NULL, NULL, NULL, NULL, '2020-03-13', NULL, NULL, 1, '2020-03-09 19:30:21', '2020-03-09 19:30:21', NULL);
INSERT INTO public.prontuario VALUES (321, 2, 89, 321, 'urog', 'Perda urinária há mais ou menos 1 ano. Perde aos esforços(gargalhada, tosse, espirro). Notou atualmente uma perda durante uma caminhada. Não estava com bexiga cheia.
Faz hidroginástica 2 x na semana.  Nega urgência miccional. 
Paciente, 49 anos, virgem.', 'Trabalhar exercícios e tibial posterior.', NULL, NULL, '2020-03-10', '5', NULL, 1, '2020-03-09 19:12:17', '2020-03-10 19:57:44', NULL);
INSERT INTO public.prontuario VALUES (315, 2, 87, 315, 'urog', 'Perda urinaria há 2 anos. Com 36 anos, cirurgia de sling e perineoplastia. Fez cirurgia de sling para perda urinaria(não teve acesso ao exame urodinamica para apresentar). Nega perda urinaria aos esforços. Tem dificuldade para urinar. Sangra e tem dor na relação sexual. Sensação de esvaziamento incompleto.', 'Perda por transbordamento?
Diário miccional intervalo de idas ao banheiro de 6 a 7 horas. Relata não ter sensaçao', 'Tensão em AP a esq. com dor referida na bexiga. Não tem coordenação entre contração e relaxamento.', NULL, '2020-03-10', '5', NULL, 1, '2020-03-06 20:32:33', '2020-03-10 20:29:53', NULL);
INSERT INTO public.prontuario VALUES (350, 2, 95, 350, 'Rv', NULL, NULL, NULL, NULL, '2020-09-17', NULL, NULL, 1, '2020-09-17 12:54:30', '2020-09-17 12:54:30', NULL);
INSERT INTO public.prontuario VALUES (351, 2, 96, 351, 'Uro', NULL, NULL, NULL, NULL, '2020-09-17', NULL, NULL, 1, '2020-09-17 12:55:20', '2020-09-17 12:55:20', NULL);


--
-- Name: prontuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.prontuario_id_seq', 351, true);


--
-- Data for Name: system_group; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_group VALUES (999, 'SuperAdmin', NULL, NULL, NULL);
INSERT INTO public.system_group VALUES (1, 'Profissional', NULL, NULL, NULL);
INSERT INTO public.system_group VALUES (2, 'Secretario', NULL, NULL, NULL);


--
-- Name: system_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_group_id_seq', 2, true);


--
-- Data for Name: system_group_program; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_group_program VALUES (1, 999, 1, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (2, 999, 2, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (3, 999, 3, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (4, 999, 4, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (5, 999, 5, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (6, 999, 6, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (7, 999, 7, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (8, 999, 8, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (9, 999, 9, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (10, 999, 10, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (11, 999, 11, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (12, 999, 12, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (13, 999, 13, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (14, 999, 14, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (15, 999, 15, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (16, 999, 16, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (17, 999, 17, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (18, 999, 18, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (19, 999, 19, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (20, 999, 20, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (21, 999, 21, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (22, 999, 22, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (23, 999, 23, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (24, 999, 24, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (25, 999, 25, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (26, 999, 26, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (27, 999, 27, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (28, 999, 28, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (29, 999, 29, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (30, 999, 30, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (31, 999, 31, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (32, 999, 32, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (33, 999, 33, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (34, 999, 34, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (35, 999, 35, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (36, 999, 36, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (37, 999, 37, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (38, 999, 38, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (39, 999, 39, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (40, 999, 40, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (41, 999, 41, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (42, 999, 42, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (43, 999, 43, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (44, 999, 44, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (45, 999, 45, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (46, 999, 46, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (47, 999, 47, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (48, 999, 48, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (49, 999, 49, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (50, 999, 51, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (51, 999, 52, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (52, 999, 53, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (53, 999, 54, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (54, 999, 55, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (55, 999, 56, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (56, 999, 57, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (57, 999, 58, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (58, 999, 59, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (59, 999, 61, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (60, 999, 62, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (61, 999, 63, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (62, 999, 64, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (63, 999, 65, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (64, 999, 66, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (65, 999, 67, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (66, 999, 68, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (67, 999, 69, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (68, 999, 71, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (69, 999, 72, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (70, 999, 73, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (71, 999, 74, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (72, 999, 75, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (73, 999, 76, NULL, NULL, NULL);
INSERT INTO public.system_group_program VALUES (96, 1, 77, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (97, 1, 79, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (98, 1, 80, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (99, 1, 84, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (100, 1, 78, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (101, 1, 83, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (102, 1, 82, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (103, 1, 81, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (104, 1, 75, '2019-09-12 18:05:56', '2019-09-12 18:05:56', NULL);
INSERT INTO public.system_group_program VALUES (105, 1, 76, '2019-09-12 18:05:57', '2019-09-12 18:05:57', NULL);
INSERT INTO public.system_group_program VALUES (106, 1, 25, '2019-09-12 18:05:57', '2019-09-12 18:05:57', NULL);
INSERT INTO public.system_group_program VALUES (107, 1, 85, '2019-09-12 18:05:57', '2019-09-12 18:05:57', NULL);
INSERT INTO public.system_group_program VALUES (108, 2, 77, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (109, 2, 79, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (110, 2, 80, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (111, 2, 84, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (112, 2, 78, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (113, 2, 83, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (114, 2, 82, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (115, 2, 81, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (116, 2, 75, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (117, 2, 76, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (118, 2, 25, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);
INSERT INTO public.system_group_program VALUES (119, 2, 85, '2019-09-12 18:06:10', '2019-09-12 18:06:10', NULL);


--
-- Name: system_group_program_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_group_program_id_seq', 119, true);


--
-- Data for Name: system_group_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_group_user VALUES (6, 1, 999, '2020-02-19 01:04:52', '2020-02-19 01:04:52', NULL);
INSERT INTO public.system_group_user VALUES (4, 2, 1, '2019-09-12 18:07:20', '2019-09-12 18:07:20', NULL);
INSERT INTO public.system_group_user VALUES (5, 3, 2, '2019-09-12 18:07:30', '2019-09-12 18:07:30', NULL);


--
-- Name: system_group_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_group_user_id_seq', 6, true);


--
-- Data for Name: system_password_resets; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: system_program; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_program VALUES (1, 'Listar banco de dados', 'system.databases', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (2, 'Editar banco de dados', 'system.databases.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (3, 'Atualizar banco de dados', 'system.databases.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (4, 'Cadastrar banco de dados', 'system.databases.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (5, 'Salvar banco de dados', 'system.databases.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (6, 'Pesquisar banco de dados', 'system.databases.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (7, 'Listar banco de dados', 'system.databases.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (8, 'Deletar banco de dados', 'system.databases.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (9, 'Listar programas', 'system.programas', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (10, 'Editar programas', 'system.programas.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (11, 'Atualizar programas', 'system.programas.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (12, 'Cadastrar programas', 'system.programas.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (13, 'Salvar programas', 'system.programas.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (14, 'Pesquisar programas', 'system.programas.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (15, 'Listar programas', 'system.programas.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (16, 'Deletar programas', 'system.programas.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (17, 'Listar grupos', 'system.grupos', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (18, 'Editar grupos', 'system.grupos.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (19, 'Atualizar grupos', 'system.grupos.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (20, 'Cadastrar grupos', 'system.grupos.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (21, 'Salvar grupos', 'system.grupos.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (22, 'Pesquisar grupos', 'system.grupos.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (23, 'Listar grupos', 'system.grupos.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (24, 'Deletar grupos', 'system.grupos.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (25, 'Exibe Dashboard SuperAdmin', 'system.dashboard', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (26, 'Listar empresas', 'system.empresas', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (27, 'Editar empresas', 'system.empresas.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (28, 'Atualizar empresas', 'system.empresas.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (29, 'Cadastrar empresas', 'system.empresas.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (30, 'Salvar empresas', 'system.empresas.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (31, 'Pesquisar empresas', 'system.empresas.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (32, 'Listar empresas', 'system.empresas.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (33, 'Deletar empresas', 'system.empresas.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (34, 'Listar usuarios', 'system.usuarios', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (35, 'Editar usuarios', 'system.usuarios.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (36, 'Atualizar usuarios', 'system.usuarios.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (37, 'Cadastrar usuarios', 'system.usuarios.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (38, 'Salvar usuarios', 'system.usuarios.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (39, 'Pesquisar usuarios', 'system.usuarios.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (40, 'Listar usuarios', 'system.usuarios.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (41, 'Deletar usuarios', 'system.usuarios.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (42, 'Listar preferencias', 'system.preferencias', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (43, 'Editar preferencias', 'system.preferencias.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (44, 'Atualizar preferencias', 'system.preferencias.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (45, 'Cadastrar preferencias', 'system.preferencias.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (46, 'Salvar preferencias', 'system.preferencias.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (47, 'Pesquisar preferencias', 'system.preferencias.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (48, 'Listar preferencias', 'system.preferencias.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (49, 'Deletar preferencias', 'system.preferencias.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (50, 'Listar estados', 'system.estados', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (51, 'Editar estados', 'system.estados.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (52, 'Atualizar estados', 'system.estados.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (53, 'Cadastrar estados', 'system.estados.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (54, 'Salvar estados', 'system.estados.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (55, 'Pesquisar estados', 'system.estados.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (56, 'Listar estados', 'system.estados.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (57, 'Deletar estados', 'system.estados.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (58, 'Listar cidades', 'system.cidades', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (59, 'Editar cidades', 'system.cidades.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (60, 'Atualizar cidades', 'system.cidades.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (61, 'Cadastrar cidades', 'system.cidades.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (62, 'Salvar cidades', 'system.cidades.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (63, 'Pesquisar cidades', 'system.cidades.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (64, 'Listar cidades', 'system.cidades.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (65, 'Deletar cidades', 'system.cidades.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (66, 'Listar enderecos', 'system.enderecos', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (67, 'Editar enderecos', 'system.enderecos.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (68, 'Atualizar enderecos', 'system.enderecos.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (69, 'Cadastrar enderecos', 'system.enderecos.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (70, 'Salvar enderecos', 'system.enderecos.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (71, 'Pesquisar enderecos', 'system.enderecos.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (72, 'Listar enderecos', 'system.enderecos.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (73, 'Deletar enderecos', 'system.enderecos.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (74, 'Tornar endereco padrão', 'system.enderecos.padrao', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (75, 'Listar meus dados', 'meus_dados', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (76, 'Atualizar meus dados', 'meus_dados.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (77, 'Listar clientes', 'cadastros.clientes', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (78, 'Editar clientes', 'cadastros.clientes.editar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (79, 'Atualizar clientes', 'cadastros.clientes.atualizar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (80, 'Cadastrar clientes', 'cadastros.clientes.cadastrar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (81, 'Salvar clientes', 'cadastros.clientes.salvar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (82, 'Pesquisar clientes', 'cadastros.clientes.procurar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (83, 'Listar clientes', 'cadastros.clientes.listar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (84, 'Deletar clientes', 'cadastros.clientes.deletar', NULL, NULL, NULL);
INSERT INTO public.system_program VALUES (85, 'agenda', 'agenda', '2019-09-12 18:05:04', '2019-09-12 18:05:04', NULL);


--
-- Name: system_program_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_program_id_seq', 85, true);


--
-- Data for Name: system_program_user; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: system_program_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_program_user_id_seq', 1, false);


--
-- Data for Name: system_unit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_unit VALUES (1, 'Prometha', '2019-09-12 18:05:28', '2020-02-19 01:04:31', NULL);


--
-- Name: system_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_unit_id_seq', 1, true);


--
-- Data for Name: system_unit_user; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: system_unit_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_unit_user_id_seq', 1, false);


--
-- Data for Name: system_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.system_user VALUES (3, 'Keila', NULL, '10143968000120', '1935617881', 'S', 'secretario@email.com', '$2y$10$.6jAwLk6E7s5ATescAVMeu12TP1RGbvyFLK42V9a7lgHSKgb4FWpW', 1, NULL, NULL, NULL, 'Y', 'yza6mgnymU8T8LLyOzu6o8qjI6TOCQ5CKOmYuicadhqlDff1tW5tn3K6GVLM', NULL, '2019-10-28 18:10:23', NULL);
INSERT INTO public.system_user VALUES (2, 'Dr. Fabiana', NULL, '0000000000', '19996754857', 'P', 'profissional@email.com', '$2y$10$j50b8ftB2f5MyLUDoXlZi.zsck.NnRriLJXOqSbdrdaqp9kPocfoq', 1, NULL, NULL, NULL, 'Y', 'GrrpKrBS3cO8g7o98cHEmchTABDjkDaanQ3ekUJWOJ6JoUz90NYj0lHX7beE', NULL, '2019-11-06 16:59:53', NULL);
INSERT INTO public.system_user VALUES (1, 'Super Administrador', NULL, NULL, NULL, NULL, 'superadmin@email.com', '$2y$10$OZyxx1E7LrV6UK0u7czyoeSO336pM9Tdv7.V/eKRHrOTViaHj3GXG', 1, NULL, NULL, NULL, 'Y', 'yoFlAGNOxy0z0hGTYXkwITjAbJL14SrdsFGR0tD3RTQOPUOu5hjEFcqZwSxR', NULL, '2020-02-19 01:04:52', NULL);


--
-- Name: system_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_user_id_seq', 3, true);


--
-- Name: agenda_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_pkey PRIMARY KEY (id);


--
-- Name: clientes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (id);


--
-- Name: enderecos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.enderecos
    ADD CONSTRAINT enderecos_pkey PRIMARY KEY (id);


--
-- Name: loc_ceps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_ceps
    ADD CONSTRAINT loc_ceps_pkey PRIMARY KEY (id);


--
-- Name: loc_cidades_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_cidades
    ADD CONSTRAINT loc_cidades_pkey PRIMARY KEY (id);


--
-- Name: loc_estados_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_estados
    ADD CONSTRAINT loc_estados_pkey PRIMARY KEY (id);


--
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: prontuario_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario
    ADD CONSTRAINT prontuario_pkey PRIMARY KEY (id);


--
-- Name: system_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group
    ADD CONSTRAINT system_group_pkey PRIMARY KEY (id);


--
-- Name: system_group_program_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_program
    ADD CONSTRAINT system_group_program_pkey PRIMARY KEY (id);


--
-- Name: system_group_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_user
    ADD CONSTRAINT system_group_user_pkey PRIMARY KEY (id);


--
-- Name: system_program_controller_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program
    ADD CONSTRAINT system_program_controller_unique UNIQUE (controller);


--
-- Name: system_program_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program
    ADD CONSTRAINT system_program_pkey PRIMARY KEY (id);


--
-- Name: system_program_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program_user
    ADD CONSTRAINT system_program_user_pkey PRIMARY KEY (id);


--
-- Name: system_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit
    ADD CONSTRAINT system_unit_pkey PRIMARY KEY (id);


--
-- Name: system_unit_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit_user
    ADD CONSTRAINT system_unit_user_pkey PRIMARY KEY (id);


--
-- Name: system_user_doc_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user
    ADD CONSTRAINT system_user_doc_unique UNIQUE (doc);


--
-- Name: system_user_email_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user
    ADD CONSTRAINT system_user_email_unique UNIQUE (email);


--
-- Name: system_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user
    ADD CONSTRAINT system_user_pkey PRIMARY KEY (id);


--
-- Name: system_password_resets_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_password_resets_email_index ON public.system_password_resets USING btree (email);


--
-- Name: agenda_clientes_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_clientes_id_foreign FOREIGN KEY (clientes_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- Name: agenda_profissional_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_profissional_id_foreign FOREIGN KEY (profissional_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: agenda_system_unit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_system_unit_id_foreign FOREIGN KEY (system_unit_id) REFERENCES public.system_unit(id) ON DELETE CASCADE;


--
-- Name: clientes_loc_ceps_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_loc_ceps_id_foreign FOREIGN KEY (loc_ceps_id) REFERENCES public.loc_ceps(id) ON DELETE CASCADE;


--
-- Name: clientes_system_unit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_system_unit_id_foreign FOREIGN KEY (system_unit_id) REFERENCES public.system_unit(id) ON DELETE CASCADE;


--
-- Name: enderecos_loc_ceps_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.enderecos
    ADD CONSTRAINT enderecos_loc_ceps_id_foreign FOREIGN KEY (loc_ceps_id) REFERENCES public.loc_ceps(id) ON DELETE CASCADE;


--
-- Name: enderecos_system_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.enderecos
    ADD CONSTRAINT enderecos_system_user_id_foreign FOREIGN KEY (system_user_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: loc_ceps_cid_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_ceps
    ADD CONSTRAINT loc_ceps_cid_id_foreign FOREIGN KEY (cid_id) REFERENCES public.loc_cidades(id);


--
-- Name: loc_cidades_est_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loc_cidades
    ADD CONSTRAINT loc_cidades_est_id_foreign FOREIGN KEY (est_id) REFERENCES public.loc_estados(id);


--
-- Name: prontuario_agenda_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario
    ADD CONSTRAINT prontuario_agenda_id_foreign FOREIGN KEY (agenda_id) REFERENCES public.agenda(id) ON DELETE CASCADE;


--
-- Name: prontuario_clientes_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario
    ADD CONSTRAINT prontuario_clientes_id_foreign FOREIGN KEY (clientes_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- Name: prontuario_profissional_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario
    ADD CONSTRAINT prontuario_profissional_id_foreign FOREIGN KEY (profissional_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: prontuario_system_unit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prontuario
    ADD CONSTRAINT prontuario_system_unit_id_foreign FOREIGN KEY (system_unit_id) REFERENCES public.system_unit(id) ON DELETE CASCADE;


--
-- Name: system_group_program_system_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_program
    ADD CONSTRAINT system_group_program_system_group_id_foreign FOREIGN KEY (system_group_id) REFERENCES public.system_group(id) ON DELETE CASCADE;


--
-- Name: system_group_program_system_program_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_program
    ADD CONSTRAINT system_group_program_system_program_id_foreign FOREIGN KEY (system_program_id) REFERENCES public.system_program(id) ON DELETE CASCADE;


--
-- Name: system_group_user_system_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_user
    ADD CONSTRAINT system_group_user_system_group_id_foreign FOREIGN KEY (system_group_id) REFERENCES public.system_group(id) ON DELETE CASCADE;


--
-- Name: system_group_user_system_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_group_user
    ADD CONSTRAINT system_group_user_system_user_id_foreign FOREIGN KEY (system_user_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: system_program_user_system_program_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program_user
    ADD CONSTRAINT system_program_user_system_program_id_foreign FOREIGN KEY (system_program_id) REFERENCES public.system_program(id) ON DELETE CASCADE;


--
-- Name: system_program_user_system_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_program_user
    ADD CONSTRAINT system_program_user_system_user_id_foreign FOREIGN KEY (system_user_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: system_unit_user_system_unit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit_user
    ADD CONSTRAINT system_unit_user_system_unit_id_foreign FOREIGN KEY (system_unit_id) REFERENCES public.system_unit(id) ON DELETE CASCADE;


--
-- Name: system_unit_user_system_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_unit_user
    ADD CONSTRAINT system_unit_user_system_user_id_foreign FOREIGN KEY (system_user_id) REFERENCES public.system_user(id) ON DELETE CASCADE;


--
-- Name: system_user_frontpage_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user
    ADD CONSTRAINT system_user_frontpage_id_foreign FOREIGN KEY (frontpage_id) REFERENCES public.system_program(id) ON DELETE CASCADE;


--
-- Name: system_user_system_unit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_user
    ADD CONSTRAINT system_user_system_unit_id_foreign FOREIGN KEY (system_unit_id) REFERENCES public.system_unit(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM navclinic;
GRANT ALL ON SCHEMA public TO navclinic;


--
-- PostgreSQL database dump complete
--

